# Quantified Self App

Siri-enabled iOS app for tracking food intake and other variables defined by user. Food quantity can be entered using a plate UI control, instead of entering ounces, grams, serving size, etc (see below)

![UIPlate](/Empirical/Screenshots/QSDemoGif.gif)