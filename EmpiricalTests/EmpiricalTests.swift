//
//  EmpiricalTests.swift
//  EmpiricalTests
//
//  Created by Angus Mitchell on 11/7/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import XCTest

class EmpiricalTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
//    func testRegex() {
//        let tvc = TableViewController()
//        let matches = tvc.matches(for: "/(?=(aa))", in: "aaa")
//        print(matches)
//    }
    
    func testStringIndicesOf() {
        var str = "aaa"
        var matches = str.indicesOf(string: "aa")
        XCTAssertTrue(matches == [0])
        
        str = "apples with with jelly"
        matches = str.indicesOf(string: " with ")
        print(matches)
        XCTAssertTrue(matches == [6])
        
        str = "with with jelly"
        matches = str.indicesOf(string: " with ")
        print(matches)
        XCTAssertTrue(matches == [4])
        
        str = "apples with  with"
        matches = str.indicesOf(string: " with ")
        print(matches)
        XCTAssertTrue(matches == [6])
    }
    
    
    func testSpeechParse(){
        let parser = SpeechParser()
        var tuple: ([Food]?, [Token]?)
        var foods: [Food]?
        var tokens: [Token]?
        var str: String
        
        // Original test: Simplified vocabulary
//        str = "Peanut butter and jelly"
//        tuple = parser.parseSpeechText(speechString: str, debug: true)
//        foods = tuple.0
//        tokens = tuple.1
//        print("ORIGINAL: " + str)
//        printTokens(tokens: tokens!)
//        printFoods2(foods: foods)
//        //        print(foods![0].countChildrenInclusive())
//        XCTAssertEqual(str, tokens!.map{ $0.string }.joined(separator: " "))
        
        // Original test: Simplified vocabulary
        str = "Organic roasted spaghetti squash ordered from Domino's topped with tomato sauce and processed mustard and covered in banana chips"
        tuple = parser.parseSpeechText(speechString: str, debug: true)
        foods = tuple.0
        tokens = tuple.1
        print("ORIGINAL: " + str)
        printTokens(tokens: tokens!)
        printFoods2(foods: foods)
//        print(foods![0].countChildrenInclusive())
        XCTAssertEqual(str, tokens!.map{ $0.string }.joined(separator: " "))
        
        // Original test: Simplified vocabulary
        str = "3 1/2 quantity method method spaghetti squash topped with method tomato sauce and quantity glaze topped with four quantity ginger and sugar cooked with a half quantity olive oil and quantity method butter restaurant"
        tuple = parser.parseSpeechText(speechString: str, debug: true)
        foods = tuple.0
        tokens = tuple.1
        print("ORIGINAL: " + str)
        printTokens(tokens: tokens!)
        printFoods(foods: foods)
//        print(foods![0].countChildrenInclusive())
        XCTAssertEqual(str, tokens!.map{ $0.string }.joined(separator: " "))
        
        // Split "with" phrases, add a few new vocab words
        str = "Bowl oatmeal topped with handful pecans and blueberries and cooked with teaspoon butter"
        tuple = parser.parseSpeechText(speechString: str, debug: true)
        foods = tuple.0
        tokens = tuple.1
        print("ORIGINAL: " + str)
        printTokens(tokens: tokens!)
        printFoods(foods: foods)
//        print(foods![0].countChildrenInclusive())
        XCTAssertEqual(str, tokens!.map{ $0.string }.joined(separator: " "))
        
        // Add in quantities that can be split across multiple words
        str = "A bowl of oatmeal topped with a handful of pecans and blueberries and cooked with a teaspoon of butter"
        tuple = parser.parseSpeechText(speechString: str, debug: true)
        foods = tuple.0
        tokens = tuple.1
        print("ORIGINAL: " + str)
        printTokens(tokens: tokens!)
        printFoods(foods: foods)
//        print(foods![0].countChildrenInclusive())
        XCTAssertEqual(str, tokens!.map{ $0.string }.joined(separator: " "))
        
        // Demo1
        str = "Half cup of steel cut oats cooked with butter and salt"
        tuple = parser.parseSpeechText(speechString: str, debug: true)
        foods = tuple.0
        tokens = tuple.1
        print("ORIGINAL: " + str)
        printTokens(tokens: tokens!)
        printFoods(foods: foods)
//        print(foods![0].countChildrenInclusive())
        XCTAssertEqual(str, tokens!.map{ $0.string }.joined(separator: " "))
        
        // Demo2
        str = "Kale salad topped with almonds and cranberries and tossed with lemon vinaigrette"
        tuple = parser.parseSpeechText(speechString: str, debug: true)
        foods = tuple.0
        tokens = tuple.1
        print("ORIGINAL: " + str)
        printTokens(tokens: tokens!)
        printFoods(foods: foods)
//        print(foods![0].countChildrenInclusive())
        XCTAssertEqual(str, tokens!.map{ $0.string }.joined(separator: " "))
        
        // Demo3
        str = "Roasted chicken\nGreen beans\nMashed potatoes"
        tuple = parser.parseSpeechText(speechString: str, debug: true)
        foods = tuple.0
        tokens = tuple.1
        print("ORIGINAL: " + str)
        printTokens(tokens: tokens!)
        printFoods(foods: foods)
//        print(foods![0].countChildrenInclusive())
        XCTAssertEqual(str, tokens!.map{ $0.string }.joined(separator: " "))
    }
    
    func printTokens(tokens: [Token]){
        for token in tokens {
            var printString = ""
//            if token is NumberToken {
//                printString += "Num"
//            } else if token is QuantityToken {
//                printString += "Qua"
//            } else if token is CookingMethodToken {
//                printString += "Mth"
//            } else if token is RestaurantToken {
//                printString += "Res"
//            } else if token is WithToken {
//                printString += "Wth"
//            }else {
//                printString += "Tok"
//            }
            printString += token.name ?? "Token"
            printString += ": " + token.string
            print(printString)
        }
    }
    
    func printFoods(foods: [Food]?){
        if let foods = foods {
            //print(stringifyFoods(foods: foods, depth: 0))
            printFoodsRec(foods: foods, depth: 0)
        } else {
            print("nil")
        }
    }
    
    func printFoods2(foods: [Food]?){
        if let foods = foods {
            //print(stringifyFoods(foods: foods, depth: 0))
            printFoodsRec2(foods: foods, depth: 0)
        } else {
            print("nil")
        }
    }
    
    func printFoodsRec(foods: [Food], depth: Int){
        var printString = ""
        for food in foods {
            var indent = ""
            for _ in 0..<depth{
                indent += "   "
            }
            printString = indent
            printString += food.name
            print(printString)
            if let cookedWith = food.cookedWith {
                //print(indent + "CW:", terminator: "")
                printFoodsRec(foods: cookedWith, depth: depth + 1)
            }
            if let toppedWith = food.toppedWith {
                //print(indent + "TW:", terminator: "")
                printFoodsRec(foods: toppedWith, depth: depth + 1)
            }
            
        }
        //return printString
    }
    
    func printFoodsRec2(foods: [Food], depth: Int){
        var printString = ""
        for food in foods {
            var indent = ""
            for _ in 0..<depth{
                indent += "   "
            }
            printString = indent
            printString += food.name
            for tag in food.tags {
                printString += "\n" + indent + " "
                printString += tag.key + " : " + tag.value
            }
            print(printString)
            
            printFoodsRec2(foods: food.children, depth: depth + 1)
//            if let toppedWith = food.toppedWith {
//                //print(indent + "TW:", terminator: "")
//                printFoodsRec(foods: toppedWith, depth: depth + 1)
//            }
            
        }
        //return printString
    }
    
    func testStringToNumber(){
        let parser = SpeechParser()
        
        // Basics
        var num = parser.stringToNumber(string: "one") // one
        XCTAssertEqual(num, 1)
        num = parser.stringToNumber(string: "three") // three
        XCTAssertEqual(num, 3)
        num = parser.stringToNumber(string: "fifteen") // 15
        XCTAssertEqual(num, 15)
        num = parser.stringToNumber(string: "fifteenish")
        XCTAssertNil(num)
        
        // Bigger numbers
        num = parser.stringToNumber(string: "sixty") // 60
        XCTAssertEqual(num, 60)
        num = parser.stringToNumber(string: "sixty-two") // 62
        XCTAssertEqual(num, 62)
        
        /* Siri cleans this up
        num = parser.stringToNumber(string: "sixtytwo") // 62
        XCTAssertEqual(num, 62)
        num = parser.stringToNumber(string: "sixty two") // 62
        XCTAssertEqual(num, 62)
        num = parser.stringToNumber(string: "hundred") // hundred *******
        XCTAssertEqual(num, 100)
         */
        num = parser.stringToNumber(string: "one hundred") // 100
        XCTAssertEqual(num, 100)
        /* Siri cleans this up
        num = parser.stringToNumber(string: "a hundred") // 100
        XCTAssertEqual(num, 100)
        num = parser.stringToNumber(string: "one hundred sixty-two") // 162
        XCTAssertEqual(num, 162)
        num = parser.stringToNumber(string: "a hundred and sixty-two") // 162
        XCTAssertEqual(num, 162)
        num = parser.stringToNumber(string: "hundred sixty-two") // 162
        XCTAssertEqual(num, 162)
        */
        
        /* SpellOut style matches this to 1505
        num = parser.stringToNumber(string: "fifteen five") // 15 five
        XCTAssertNil(num)
        */
        num = parser.stringToNumber(string: "15 five")
        XCTAssertNil(num)
        num = parser.stringToNumber(string: "hundreds")
        XCTAssertNil(num)
        num = parser.stringToNumber(string: "sixty two hundred")
        XCTAssertNil(num)
        
        // Numerics
        num = parser.stringToNumber(string: "1")
        XCTAssertEqual(num, 1)
        num = parser.stringToNumber(string: "1.0")
        XCTAssertEqual(num, 1)
        num = parser.stringToNumber(string: "0.25")
        XCTAssertEqual(num, 0.25)
        num = parser.stringToNumber(string: ".25")
        XCTAssertEqual(num, 0.25)
        num = parser.stringToNumber(string: "4.00")
        XCTAssertEqual(num, 4)
        num = parser.stringToNumber(string: "4,234")
        XCTAssertEqual(num, 4234)
        num = parser.stringToNumber(string: "1..0")
        XCTAssertNil(num)
        num = parser.stringToNumber(string: "0.1.0")
        XCTAssertNil(num)
        num = parser.stringToNumber(string: "1a")
        XCTAssertNil(num)
        num = parser.stringToNumber(string: "1 0")
        XCTAssertNil(num)
        num = parser.stringToNumber(string: "a1")
        XCTAssertNil(num)
        
        // "Ordinal" Fractions
        num = parser.stringToNumber(string: "half") // half of a
        XCTAssertEqual(num, 0.5)
        num = parser.stringToNumber(string: "third") // third of a
        XCTAssertTrue((num != nil ? num! - (0.33333333333) < 0.00001 : false))
        num = parser.stringToNumber(string: "quarter") // quarter of a
        XCTAssertEqual(num, 0.25)
        num = parser.stringToNumber(string: "fourth") // fourth of a
        XCTAssertEqual(num, 0.25)
        num = parser.stringToNumber(string: "fifth") // fifth of a
        XCTAssertEqual(num, 0.2)
        num = parser.stringToNumber(string: "eighth") // eighth of a
        XCTAssertEqual(num, 0.125)
        num = parser.stringToNumber(string: "tenth") // tenth of a
        XCTAssertTrue((num != nil ? num! - (0.1) < 0.00001 : false))
        num = parser.stringToNumber(string: "eighths")
        XCTAssertNil(num)
        num = parser.stringToNumber(string: "quarters")
        XCTAssertNil(num)
        
        // Contextualized "Ordinal" Fractions
        // Pattern: a|an {ordinalfraction}
        num = parser.stringToNumber(string: "a half") // a half of a
        XCTAssertEqual(num, 0.5)
        num = parser.stringToNumber(string: "an eighth") // an eighth of a
        XCTAssertEqual(num, 0.125)
        
        // "Ordinal" fractions with divisor
        // Pattern: {integer} {ordinalfraction}|s
        num = parser.stringToNumber(string: "one half") // 1/2
        XCTAssertEqual(num, 0.5)
        num = parser.stringToNumber(string: "one quarter") // 1/4
        XCTAssertEqual(num, 0.25)
        num = parser.stringToNumber(string: "three quarters") // 3/4
        XCTAssertEqual(num, 0.75)
        num = parser.stringToNumber(string: "one fifth") // 1/5
        XCTAssertEqual(num, 0.2)
        num = parser.stringToNumber(string: "three fifths") // 3/5
        XCTAssertEqual(num, 0.6)
        num = parser.stringToNumber(string: "three eighths") // 3/8
        XCTAssertEqual(num, 0.375)
        num = parser.stringToNumber(string: "3 eighths") // 3/8
        XCTAssertEqual(num, 0.375)
        num = parser.stringToNumber(string: "1 quarter") // 1/4
        XCTAssertEqual(num, 0.25)
        num = parser.stringToNumber(string: "one 1/4")
        XCTAssertNil(num)
        num = parser.stringToNumber(string: "three 7")
        XCTAssertNil(num)
        /* SpellOut style matches this
        num = parser.stringToNumber(string: "three seven")
        XCTAssertNil(num)
        */
        
        // Numeric fractions
        num = parser.stringToNumber(string: "1/4")
        XCTAssertEqual(num, 0.25)
        num = parser.stringToNumber(string: "1/8")
        XCTAssertEqual(num, 0.125)
        num = parser.stringToNumber(string: "3/8")
        XCTAssertEqual(num, 0.375)
        num = parser.stringToNumber(string: "8/2")
        XCTAssertEqual(num, 4)
        
        // Composite numeric fractions
        // Pattern: /d+ /d+\//d+
        num = parser.stringToNumber(string: "3 1/8")
        XCTAssertEqual(num, 3.125)
        num = parser.stringToNumber(string: "3 1/2")
        XCTAssertEqual(num, 3.5)
        num = parser.stringToNumber(string: "1 5/8")
        XCTAssertEqual(num, 1.625)
        
        // Composite written fractions
        // Pattern: {Integer} and ({integer} {ordinalfraction}|s)|(a|an {ordinalfraction})
        //          {Integer} and {Fraction}
        num = parser.stringToNumber(string: "one and a half") // one and a half ************
        XCTAssertEqual(num, 1.5)
        num = parser.stringToNumber(string: "one and one half") // 1 1/2
        XCTAssertEqual(num, 1.5)
        num = parser.stringToNumber(string: "two and a half") // 2 1/2
        XCTAssertEqual(num, 2.5)
        num = parser.stringToNumber(string: "two and one half") // 2 1/2
        XCTAssertEqual(num, 2.5)
        num = parser.stringToNumber(string: "two and three eighths") // 2 3/8
        XCTAssertEqual(num, 2.375)
        num = parser.stringToNumber(string: "1 and a half") // one and a half ************
        XCTAssertEqual(num, 1.5)
        /* I think Siri will normally clean this up
        num = parser.stringToNumber(string: "1 and a 1/2") // one and a half ************
        XCTAssertEqual(num, 1.5)
        */
        num = parser.stringToNumber(string: "two and 3/8") // 2 3/8
        XCTAssertEqual(num, 2.375)
        num = parser.stringToNumber(string: "2 and 3/8") // 2 3/8
        XCTAssertEqual(num, 2.375)
        num = parser.stringToNumber(string: "half and a one") // half and one
        XCTAssertNil(num)
        num = parser.stringToNumber(string: "a half and one") // a half and one
        XCTAssertNil(num)
        num = parser.stringToNumber(string: "2.5 and 0.5") // a half and one
        XCTAssertNil(num)
        
        // ALSO ******************
        // Cup and a half of coffee
    }
    
    func testSortedBy() {
        // Hacky comparison method but array-of-array equality looks
        // like a bitch and I don't want to deal with it
        let indicesOfAllRuns = [[1, 2, 3], [5, 6], [4]]
        
        // Descending
        var sortedIndices = indicesOfAllRuns.sorted(by: { $0[0] > $1[0] })
        XCTAssertTrue(String(describing: sortedIndices) == "[[5, 6], [4], [1, 2, 3]]")
        
        // Ascending
        sortedIndices = indicesOfAllRuns.sorted(by: { $0[0] < $1[0] })
        XCTAssertTrue(String(describing: sortedIndices) == "[[1, 2, 3], [4], [5, 6]]")
    }
    
    func testProcessNumericTokens() {
        let parser = SpeechParser()
        var string: String
        var tokens: [Token]?
        var numbers: [Float]?
        
        string = "Three hundred fifty-two animals played hopscotch"
        tokens = parser.tokenizeString(string: string)
        tokens = parser.processNumericTokens(speechTokens: tokens)!
        numbers = tokens?.filter{ $0 is NumberToken }.map{ ($0 as! NumberToken).number! }
        XCTAssertTrue(numbers?.count == 1)
        XCTAssertTrue((numbers?.contains(352))!)
        //printTokens(tokens: tokens!)
        
        string = "TEN cups of coffee were delivered to 2 1/2 robots"
        tokens = parser.tokenizeString(string: string)
        tokens = parser.processNumericTokens(speechTokens: tokens)!
        numbers = tokens?.filter{ $0 is NumberToken }.map{ ($0 as! NumberToken).number! }
        XCTAssertTrue(numbers?.count == 2)
        XCTAssertTrue((numbers?.contains(10))! && (numbers?.contains(2.5))!)
        //printTokens(tokens: tokens!)
        
        string = "I have the 3 of the 9 and two and a half"
        tokens = parser.tokenizeString(string: string)
        tokens = parser.processNumericTokens(speechTokens: tokens)!
        numbers = tokens?.filter{ $0 is NumberToken }.map{ ($0 as! NumberToken).number! }
        XCTAssertTrue(numbers?.count == 3)
        XCTAssertTrue((numbers?.contains(3))! && (numbers?.contains(9))! && (numbers?.contains(2.5))!)
        //printTokens(tokens: tokens!)
        
        // Empty token array
        string = ""
        tokens = parser.tokenizeString(string: string)
        tokens = parser.processNumericTokens(speechTokens: tokens)
        XCTAssertTrue(tokens!.isEmpty)
        
        // Nil token array
        tokens = parser.processNumericTokens(speechTokens: nil)
        XCTAssertNil(tokens)
        
    }
    
}
