//
//  ParserTimingTests.swift
//  SiriTests
//
//  Created by Angus Mitchell on 11/13/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import XCTest

class ParserTimingTests: XCTestCase {
    let parser = SpeechParser()
    var str = "Ten bowl of oatmeal topped with a handful of twenty-three pecans and blueberries and cooked with a teaspoon of butter"
    var numString = "hello hello hello hello"
    var lowercased = "hello hello hello hello"

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSpeechParseTime_Entire(){
        var tuple: ([Food]?, [Token]?)
        self.measure() {
            tuple = parser.parseSpeechText(speechString: str, debug: false)
        }
    }
    
    func testSpeechParseTime_ParseTokens() {
        
        var speechTokens = [Token]()
        speechTokens = parser.tokenizeString(string: str)
        speechTokens = parser.processNumericTokens(speechTokens: speechTokens)!
        self.measure() {
            parser.parseSpeechTokens(speechTokens: speechTokens, isWithClause: false, debug: false, depth: 0, order: "A")
        }
    }
    
    func testSpeechParseTime_TokensAndNumbers() {
        
        var speechTokens = [Token]()
        self.measure() {
            speechTokens = parser.tokenizeString(string: str)
            speechTokens = parser.processNumericTokens(speechTokens: speechTokens)!
        }
    }
    
    func testSpeechParseTime_Tokens() {
        var speechTokens = [Token]()
        self.measure() {
            speechTokens = parser.tokenizeString(string: str)
        }
    }
    
    func testSpeechParseTime_Numbers() {
        var speechTokens = [Token]()
        speechTokens = parser.tokenizeString(string: str)
        self.measure() {
            speechTokens = parser.processNumericTokens(speechTokens: speechTokens)!
        }
    }
    
    func testTokensToNumbersTime() {
        var numberTokens = [Token]()
        //var numString = "two and a half"
        numberTokens = parser.tokenizeString(string: numString)
        self.measure() {
            parser.tokensToNumber(tokens: numberTokens)
        }
    }
    
    func testStringToNumbersTime() {
        //var numString = "two and a half"
        self.measure() {
            parser.stringToNumber(string: numString)
        }
    }
    
    // Spell out
    func testSpellOutToNumberTime() {
        //var lowercased = "two and a half"
        self.measure() {
            parser.spelledOutStringToNumber(string: lowercased)
        }
    }
    
    func testSpellOutToNumberTime_NF(){
        var number: NSNumber?
        self.measure() {
            let numberFormatter: NumberFormatter = NumberFormatter()
            numberFormatter.numberStyle = NumberFormatter.Style.spellOut
            number = numberFormatter.number(from: lowercased)
        }
    
        // Check to see if ordinal, if so then invert
        if number != nil {
            if parser.doesStringHaveFractionalQualities(string: lowercased) {
                let divisor = Float(truncating: number!)
            }
        }
    }
    
    func testSpellOutToNumberTime_NFInit(){
        var number: NSNumber?
        var numberFormatter: NumberFormatter?
        numberFormatter = nil
        self.measure() {
            numberFormatter  = NumberFormatter()
            numberFormatter!.numberStyle = NumberFormatter.Style.spellOut
        }
        
        number = numberFormatter!.number(from: lowercased)
        
        // Check to see if ordinal, if so then invert
        if number != nil {
            if parser.doesStringHaveFractionalQualities(string: lowercased) {
                let divisor = Float(truncating: number!)
            }
        }
    }
    
    func testSpellOutToNumberTime_NFParse(){
        var number: NSNumber?
        let numberFormatter: NumberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.spellOut
        
        self.measure() {
            number = numberFormatter.number(from: lowercased)
        }
        
        // Check to see if ordinal, if so then invert
        if number != nil {
            if parser.doesStringHaveFractionalQualities(string: lowercased) {
                let divisor = Float(truncating: number!)
            }
        }
    }
    
    func testSpellOutToNumberTime_Check(){
        let numberFormatter: NumberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.spellOut
        let number = numberFormatter.number(from: lowercased)
        
        self.measure() {
            // Check to see if ordinal, if so then invert
            if number != nil {
                if parser.doesStringHaveFractionalQualities(string: lowercased) {
                    let divisor = Float(truncating: number!)
                }
            }
        }
    }
    
    // Decimal
    func testDecimalStringToNumberTime() {
        //var lowercased = "two and a half"
        self.measure() {
            parser.decimalStringToNumber(string: lowercased)
        }
    }
    
    // Numeric fractions
    func testNumericFractionToNumberTime() {
        //var lowercased = "two and a half"
        self.measure() {
            parser.numericFractionStringToNumber(string: lowercased)
        }
    }
    
    // Half, quarter, etc.
    func testSpecialCaseFractionToNumberTime() {
        //var lowercased = "two and a half"
        self.measure() {
            parser.specialCaseFractionalStringToNumber(string: lowercased)
        }
    }
    
//    // Contextualized Fraction
//    func testContextualizedFractionToNumberTime() {
//        //var lowercased = "two and a half"
//        self.measure() {
//            parser.contextualizedFractionStringToNumber(string: lowercased)
//        }
//    }
//    
//    // Fraction with Divisor
//    func testFractionWithDivisorToNumberTime() {
//        //var lowercased = "two and a half"
//        self.measure() {
//            parser.fractionWithDivisorStringToNumber(string: lowercased)
//        }
//    }
//    
//    // Composite Text Fraction
//    func testCompositeFractionToNumberTime() {
//        //var lowercased = "two and a half"
//        self.measure() {
//            parser.compositeFractionStringToNumber(string: lowercased)
//        }
//    }
}
