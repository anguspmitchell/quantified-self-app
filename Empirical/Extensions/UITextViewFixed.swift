//
//  UITextViewFixed.swift
//  Siri
//
//  Created by Angus Mitchell on 12/12/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

@IBDesignable class UITextViewFixed: UITextView {
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    func setup() {
        textContainerInset = UIEdgeInsets.zero
        textContainer.lineFragmentPadding = 0
    }
}
