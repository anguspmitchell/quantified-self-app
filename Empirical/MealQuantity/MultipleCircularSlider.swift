//
//  RangeCircularSlider.swift
//  Pods
//
//  Created by Hamza Ghazouani on 25/10/2016.
//
//

import UIKit

/**
 A visual control used to select a range of values (between start point and the end point) from a continuous range of values.
 RangeCircularSlider use the target-action mechanism to report changes made during the course of editing:
 ValueChanged, EditingDidBegin and EditingDidEnd
 */
open class MultipleCircularSlider: CircularSlider {

    //var selectedThumb: Int?
    //selectedThumb = nil
//    public enum SelectedThumb {
//        case startThumb
//        case endThumb
//        case none
//
//        var isStart: Bool {
//            return  self == SelectedThumb.startThumb
//        }
//        var isEnd: Bool {
//            return  self == SelectedThumb.endThumb
//        }
//    }

    // MARK: Changing the Slider’s Appearance
    
    /**
     * The color used to tint start thumb
     * Ignored if the startThumbImage != nil
     *
     * The default value of this property is the groupTableViewBackgroundColor.
     */
    @IBInspectable
    open var startThumbTintColor: UIColor = UIColor.groupTableViewBackground
    open var middleThumbTintColor: UIColor = UIColor.groupTableViewBackground
    
    /**
     * The color used to tint the stroke of the start thumb
     * Ignored if the startThumbImage != nil
     *
     * The default value of this property is the green color.
     */
    @IBInspectable
    open var startThumbStrokeColor: UIColor = UIColor.green
    open var middleThumbStrokeColor: UIColor = UIColor.cyan
    
    /**
     * The stroke highlighted color of start thumb
     * The default value of this property is blue color
     */
    @IBInspectable
    open var startThumbStrokeHighlightedColor: UIColor = UIColor.purple
    open var middleThumbStrokeHighlightedColor: UIColor = UIColor.gray
    
    
    /**
     * The image of the end thumb
     * Clears any custom color you may have provided for end thumb.
     *
     * The default value of this property is nil
     */
    //open var startThumbImage: UIImage?
    //open var middleThumbImage: UIImage?
    open var thumbImages = [UIImage?]()
    open var foodLabels = [UILabel?]()
    // MARK: Accessing the Slider’s Value Limits
    
    /**
     * The minimum value of the receiver.
     *
     * If you change the value of this property, and the start value of the receiver is below the new minimum, the start value is adjusted to match the new minimum value automatically.
     * The end value is also adjusted to match (startPointValue + distance) automatically if the distance is different to -1 (SeeAlso: startPointValue, distance)
     * The default value of this property is 0.0.
     */
    override open var minimumValue: CGFloat {
        didSet {
            if pointValues[0] < minimumValue {
                pointValues[0] = minimumValue
            }
        }
    }
    
    /**
     * The maximum value of the receiver.
     *
     * If you change the value of this property, and the end value of the receiver is above the new maximum, the end value is adjusted to match the new maximum value automatically.
     * The start value is also adjusted to match (endPointValue - distance) automatically  if the distance is different to -1 (see endPointValue, distance)
     * The default value of this property is 1.0.
     */
    @IBInspectable
    override open var maximumValue: CGFloat {
        didSet {
            if endPointValue > maximumValue {
                endPointValue = maximumValue
            }
        }
    }
    
    open var maximumRatio: CGFloat = 24
    
    /**
    * The fixed distance between the start value and the end value
    *
    * If you change the value of this property, the end value is adjusted to match (startPointValue + distance)
    * If the end value is above the maximum value, the end value is adjusted to match the maximum value and the start value is adjusted to match (endPointValue - distance)
    * To disable distance use -1 (by default)
    *
    * The default value of this property is -1
    */
    @IBInspectable
    open var distance: CGFloat = -1 {
        didSet {
            assert(distance <= maximumValue - minimumValue, "The distance value is greater than distance between max and min value")
            pointValues[2] = pointValues[0] + distance
        }
    }
    
    
    /**
     * The value in the start thumb.
     *
     * If you try to set a value that is below the minimum value, the minimum value is set instead.
     * If you try to set a value that is above the (endPointValue - distance), the (endPointValue - distance) is set instead.
     *
     * The default value of this property is 0.0.
     */
    
    open var pointValues = [CGFloat]()
    open var pointValueDifferences = [CGFloat]()
    open var labelValues = [CGFloat]()
    
    open var startPointValue: CGFloat = 0.0 {
        didSet {
            guard oldValue != startPointValue else { return }

            if startPointValue < minimumValue {
                startPointValue = minimumValue
            }

            if distance > 0 {
                endPointValue = startPointValue + distance
            }

            setNeedsDisplay()
        }
    }
//
//    open var middlePointValue: CGFloat = 0.0 {
//        didSet {
//            guard oldValue != middlePointValue else { return }
//
////            if middlePointValue < minimumValue {
////                middlePointValue = minimumValue
////            }
////
////            if distance > 0 {
////                middlePointValue = middlePointValue + distance
////            }
//
////            setNeedsDisplay()
//        }
//    }
    
    /**
     * The value in the end thumb.
     *
     * If you try to set a value that is above the maximum value, the maximum value is set instead.
     * If you try to set a value that is below the (startPointValue + distance), the (startPointValue + distance) is set instead.
     *
     * The default value of this property is 0.5
     */
    override open var endPointValue: CGFloat {
        didSet {
            if oldValue == endPointValue && distance <= 0 {
                return
            }
            
            if endPointValue > maximumValue {
                endPointValue = maximumValue
            }
            
            if distance > 0 {
                pointValues[0] = endPointValue - distance
            }
            
            setNeedsDisplay()
        }
    }
    
    // MARK: private properties / methods
    
    /**
     * The center of the start thumb
     * Used to know in which thumb is the user gesture
     */
    fileprivate var startThumbCenter: CGPoint = CGPoint.zero
    fileprivate var middleThumbCenter: CGPoint = CGPoint.zero
    
    open var thumbCenters = [CGPoint]()
    
    /**
     * The center of the end thumb
     * Used to know in which thumb is the user gesture
     */
    fileprivate var endThumbCenter: CGPoint = CGPoint.zero
    
    /**
     * The last touched thumb
     * By default the value is none
     */
    //fileprivate var selectedThumb: SelectedThumb = .none
    fileprivate var selectedThumb: Int? = nil
    
    /**
     Checks if the touched point affect the thumb
     
     The point affect the thumb if :
     The thumb rect contains this point
     Or the angle between the touched point and the center of the thumb less than 15°
     
     - parameter thumbCenter: the center of the thumb
     - parameter touchPoint:  the touched point
     
     - returns: true if the touched point affect the thumb, false if not.
     */
    internal func isThumb(withCenter thumbCenter: CGPoint, containsPoint touchPoint: CGPoint) -> Bool {
        // the coordinates of thumb from its center
        //print("isThumb")
        let rect = CGRect(x: thumbCenter.x - thumbRadius, y: thumbCenter.y - thumbRadius, width: thumbRadius * 2, height: thumbRadius * 2)
        if rect.contains(touchPoint) {
            return true
        }
        
        let angle = CircularSliderHelper.angle(betweenFirstPoint: thumbCenter, secondPoint: touchPoint, inCircleWithCenter: bounds.center)
        let degree =  CircularSliderHelper.degrees(fromRadians: angle)
        
        // tolerance 15°
        let isInside = degree < 15 || degree > 345
        return isInside
    }
    
    // MARK: Drawing
    
    /**
     See superclass documentation
     */
    let colorArray : [UIColor] = [UIColor.cyan, UIColor.orange, UIColor.green, UIColor.red, UIColor.yellow, UIColor.blue, UIColor.magenta, UIColor.lightGray]
    
    override open func draw(_ rect: CGRect) {
        //print("Draw")
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        drawCircularSlider(inContext: context)
        
        let interval = Interval(min: minimumValue, max: maximumValue, rounds: numberOfRounds)
        
        
        for i in 0..<pointValues.count {
            let startAngle2 = CircularSliderHelper.scaleToAngle(value: pointValues[i], inInterval: interval) + CircularSliderHelper.circleInitialAngle
            let endIndex = i + 1 < pointValues.count ? i + 1 : 0
            let endAngle2 = CircularSliderHelper.scaleToAngle(value: pointValues[endIndex], inInterval: interval) + CircularSliderHelper.circleInitialAngle
            
            let baseColor : UIColor = colorArray[i % colorArray.count]
            let trackColor = baseColor.withAlphaComponent(0.15)
            drawFilledArc(fromAngle: startAngle2, toAngle: endAngle2, inContext: context, trackColor: trackColor)
        }
        
        for i in 0..<pointValues.count {
            let startAngle2 = CircularSliderHelper.scaleToAngle(value: pointValues[i], inInterval: interval) + CircularSliderHelper.circleInitialAngle
            
            let trackColor : UIColor = colorArray[i % colorArray.count]
            
            endThumbTintColor.setFill()
            (isHighlighted == true && selectedThumb == i) ? trackColor.setStroke() : trackColor.setStroke()

//            thumbCenters[i] = drawThumb(withAngle: startAngle2, inContext: context)
            //if let image = endThumbImage {
            if let image = thumbImages[i] {
                thumbCenters[i] = drawThumb(withImage: image, angle: startAngle2, inContext: context)
            } else {
                thumbCenters[i] = drawThumb(withAngle: startAngle2, inContext: context)
            }
        }
        
        for i in 0..<labelValues.count {
            let startAngle2 = CircularSliderHelper.scaleToAngle(value: labelValues[i], inInterval: interval) + CircularSliderHelper.circleInitialAngle
            
            let trackColor : UIColor = colorArray[i % colorArray.count]
            
            endThumbTintColor.setFill()
            (isHighlighted == true && selectedThumb == i) ? trackColor.setStroke() : trackColor.setStroke()
            
//            thumbCenters[i] = drawThumb(withAngle: startAngle2, inContext: context)
            //if let image = endThumbImage {
            if let label = foodLabels[i] {
                let point = drawLabel(withLabel: label, angle: startAngle2, inContext: context)
            }
        }
    }
    
    // MARK: User interaction methods
    
    /**
     See superclass documentation
     */
    override open func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
//        print("Being Tracking")
        sendActions(for: .editingDidBegin)
        // the position of the pan gesture
        let touchPosition = touch.location(in: self)
        selectedThumb = thumb(for: touchPosition)

        return selectedThumb != .none
    }
    
    /**
     See superclass documentation
     */
    override open func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
//        print("Continue Tracking")
//        guard selectedThumb != .none else {
        guard selectedThumb != nil else {
            return false
        }

        // the position of the pan gesture
        let touchPosition = touch.location(in: self)
        let startPoint = CGPoint(x: bounds.center.x, y: 0)
        
//        let oldValue: CGFloat = selectedThumb.isStart ? startPointValue : endPointValue
//        var oldValue: CGFloat = 0.0
//        if selectedThumb == 0 {
//            oldValue = pointValues[0]//startPointValue
//        }
//        else if selectedThumb == 1{
//            oldValue = pointValues[1]//middlePointValue
//        }
//        else if selectedThumb == 2 {
//            oldValue = pointValues[2]//endPointValue
//        }
        let oldValue = pointValues[selectedThumb!]
        let value =  newValue(from: oldValue, touch: touchPosition, start: startPoint)
        //print(value)
        //if selectedThumb.isStart {
        let gap : CGFloat = maximumValue / maximumRatio //3600.0
//        print(maximumRatio)
//        print(gap)
        
        let leftIndex = selectedThumb! - 1 >= 0 ? selectedThumb! - 1 : pointValues.count - 1
        let leftEdge = (pointValues[leftIndex] + gap).truncatingRemainder(dividingBy: maximumValue)
        let rightIndex = selectedThumb! + 1 < pointValues.count ? selectedThumb! + 1 : 0
        let rightEdge = (pointValues[rightIndex] + maximumValue - gap).truncatingRemainder(dividingBy: maximumValue)
        if leftEdge < rightEdge {
            if value > leftEdge && value < rightEdge {
                pointValues[selectedThumb!] = value
            }
        }
        else {
            if (value > leftEdge && value <= maximumValue) || (value >= 0 && value < rightEdge) {
                pointValues[selectedThumb!] = value
            }
        }
        
        updatePointValueDiffereces()
        setLabelValues()
        
//        print(pointValues)
//        print(getPointDifferentials(pointValues: pointValues))
        
        
//        if selectedThumb == 0 {
////            if abs(value - endPointValue) < 3600 || abs(value - middlePointValue) < 3600 {
////                print("CROSSING")
////            }
////            else {
////                startPointValue = value
////            }
//            let leftEdge = (pointValues[2] + gap).truncatingRemainder(dividingBy: maximumValue)
//            let rightEdge = (pointValues[1] + maximumValue - gap).truncatingRemainder(dividingBy: maximumValue)
//            if leftEdge < rightEdge {
//                if value > leftEdge && value < rightEdge {
//                    pointValues[0] = value
//                }
//            }
//            else {
//                if (value > leftEdge && value <= maximumValue) || (value >= 0 && value < rightEdge) {
//                    pointValues[0] = value
//                }
//            }
//        } else if selectedThumb == 1 {
//            let leftEdge = (pointValues[0] + gap).truncatingRemainder(dividingBy: maximumValue)
//            let rightEdge = (pointValues[2] + maximumValue - gap).truncatingRemainder(dividingBy: maximumValue)
//            if leftEdge < rightEdge {
//                if value > leftEdge && value < rightEdge {
//                    pointValues[1] = value
//                }
//            }
//            else {
//                if (value > leftEdge && value <= maximumValue) || (value >= 0 && value < rightEdge) {
//                    pointValues[1] = value
//                }
//            }
//        } else if selectedThumb == 2 {
////            if abs(value - startPointValue) < 3600 || abs(value - middlePointValue) < 3600 {
////                print("CROSSING")
////            }
////            else {
////                endPointValue = value
////            }
//            let leftEdge = (pointValues[1] + gap).truncatingRemainder(dividingBy: maximumValue)
//            let rightEdge = (pointValues[0] + maximumValue - gap).truncatingRemainder(dividingBy: maximumValue)
//            if leftEdge < rightEdge {
//                if value > leftEdge && value < rightEdge {
//                    pointValues[2] = value
//                }
//            }
//            else {
//                if (value > leftEdge && value <= maximumValue) || (value >= 0 && value < rightEdge) {
//                    pointValues[2] = value
//                }
//            }
//        }

        sendActions(for: .valueChanged)
        
        return true
    }
    
//    func updatePointValues(differentials: [Float], updateIndex: Int) {
//        for offset in 0..<(pointValues.count - 1) {
//            let currentIndex = (updateIndex + offset) % pointValues.count
//            let nextIndex = (updateIndex + offset + 1) % pointValues.count
//            let currentDifferential = differentials[currentIndex]
//            let currentPointValue = pointValues[currentIndex]
//            pointValues[nextIndex] = (currentPointValue + CGFloat(currentDifferential)).truncatingRemainder(dividingBy: maximumValue)
//        }
//    }
    
    func getPointDifferentials() -> [CGFloat] {
        return getPointDifferentials(pointValues: pointValues)
    }
    
    func getPointDifferentials(pointValues: [CGFloat]) -> [CGFloat] {
        var differentials = [CGFloat]()
        for (i, value) in pointValues.enumerated() {
            var nextIndex = i + 1
            if nextIndex == pointValues.count {
                nextIndex = 0
            }
            let nextValue = pointValues[nextIndex]
            
            var differential: CGFloat
            if nextValue >= value {
                differential = nextValue - value
            } else {
                differential = nextValue + (maximumValue - value)
            }
            differentials.append(differential)
        }
        
        return differentials
    }
    
    func updatePointValueDiffereces() {
        var differences = [CGFloat]()
        for (i, value) in pointValues.enumerated() {
            var nextIndex = i + 1
            if nextIndex == pointValues.count {
                nextIndex = 0
            }
            let nextValue = pointValues[nextIndex]
            
            var difference: CGFloat
            if nextValue >= value {
                difference = nextValue - value
            } else {
                difference = nextValue + (maximumValue - value)
            }
            differences.append(difference)
        }
        
        self.pointValueDifferences = differences
    }
    
    func setLabelValues() {
        var values = [CGFloat]()
        for (i, pointValue) in pointValues.enumerated() {
            let value = (pointValue + (pointValueDifferences[i] / 2)).truncatingRemainder(dividingBy: maximumValue)
            values.append(value)
        }
        
        self.labelValues = values
    }

    override open func endTracking(_ touch: UITouch?, with event: UIEvent?) {
//        print("End Tracking")
        super.endTracking(touch, with: event)
    }


    // MARK: - Helpers
//    open func thumb(for touchPosition: CGPoint) -> SelectedThumb {
//        print("Thumb")
//        if isThumb(withCenter: startThumbCenter, containsPoint: touchPosition) {
//            return .startThumb
//        }
//        else if isThumb(withCenter: endThumbCenter, containsPoint: touchPosition) {
//            return .endThumb
//        } else {
//            return .none
//        }
//    }
    
    open func thumb(for touchPosition: CGPoint) -> Int? {
        //print("Thumb")
        for (i, thumbCenter) in thumbCenters.enumerated() {
            if isThumb(withCenter: thumbCenter, containsPoint: touchPosition) {
                return i
            }
        }
        return nil
    }

}
