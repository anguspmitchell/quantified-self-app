//
//  ViewController.swift
//  HGCircularSlider
//
//  Created by Hamza Ghazouani on 10/19/2016.
//  Copyright (c) 2016 Hamza Ghazouani. All rights reserved.
//

import UIKit
//import HGCircularSlider

extension Date {
    
}

class MealQuantityViewController: UIViewController {
    

//    @IBOutlet weak var durationLabel: UILabel!
//    @IBOutlet weak var bedtimeLabel: UILabel!
//    @IBOutlet weak var wakeLabel: UILabel!

    @IBOutlet weak var rangeCircularSlider: MultipleCircularSlider!
    @IBOutlet weak var sliderStackView: UIStackView!
    
    @IBOutlet weak var portionSlider: UISlider!
    @IBOutlet weak var circleSliderBackgroundImageView: UIImageView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var circularSliderContainer: UIView!
    @IBOutlet weak var rangeCircularSliderHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var rangeCircularSliderWidthConstraint: NSLayoutConstraint!
//    var sliders = [UISlider]()
    var sliderQuantities = [UILabel]()
    var sliderComparisons = [UILabel]()
    var sliderVolumes = [UILabel]()
    
    var foods: [Food]?
    let step: Float = 1.0
//    var totalCurrentFood: Float = 0
//    var minimumPossibleFood: Float = 100
//    var maximumPossibleFood: Float = 250
//    var rangePossibleFood: Float = 0
//    var individualMaxValue: Float = 0
//    var individualMinValue: Float = 0
//    let totalMaxValue: Float = 250
//    let totalMinValue: Float = 100
    let maxPortion: Float = 500
    let minPortion: Float = 2
    var currentPortion: Float = 100
    var portionPercentages = [Float]()
    let portionImages = [UIImage(named: "oneHandDesat"), UIImage(named: "handfulDesat"), UIImage(named: "smallPlateDesat"), UIImage(named: "bigPlateDesat")]
    
    
    //let numberThumbs = 3
    
    var maximumRatio: Float = 0
    var fullWidth: CGFloat = 0
    var fullHeight: CGFloat = 0
    var maxHeight: CGFloat = 0
    var maxWidth: CGFloat = 0
    var minHeight: CGFloat = 0
    var minWidth: CGFloat = 0
    
//    lazy var dateFormatter: DateFormatter = {
//        let dateFormatter = DateFormatter()
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
//        dateFormatter.dateFormat = "hh:mm a"
//        return dateFormatter
//    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup O'clock
//        rangeCircularSlider.startThumbImage = UIImage(named: "Bedtime")
//        rangeCircularSlider.middleThumbImage = UIImage(named: "Bedtime")
//        rangeCircularSlider.endThumbImage = UIImage(named: "Wake")
//        rangeCircularSlider.thumbImages[0] = UIImage(named: "Bedtime")
//        rangeCircularSlider.thumbImages[1] = UIImage(named: "Bedtime")
//        rangeCircularSlider.thumbImages[2] = UIImage(named: "Wake")
        
        //let dayInSeconds = 24 * 60 * 60
        
        let totalPercentage = 1.0 // 24 * 60 * 60.0 // 1.0
        rangeCircularSlider.maximumValue = CGFloat(totalPercentage)
        rangeCircularSlider.numberOfRounds = 1
        
//        rangeCircularSlider.addTarget(self, action: #selector(ClockViewController.circleSliderBeginEdit(_:)), for: .editingDidBegin)
        rangeCircularSlider.addTarget(self, action: #selector(MealQuantityViewController.circleSliderValueChanged(_:)), for: .valueChanged)
//        rangeCircularSlider.addTarget(self, action: #selector(ClockViewController.circleSliderEndEdit(_:)), for: .editingDidEnd)
        // circularSliderContainer.backgroundColor = UIColor(patternImage: UIImage(named: "handfulDesat")!)
        updatePortionBackgroundImage()
        
        portionSlider.value = currentPortion
        
        view.layoutIfNeeded()
        fullWidth = circularSliderContainer.frame.size.width // Float(rangeCircularSlider.frame.size.width)
        fullHeight = circularSliderContainer.frame.size.height // Float(rangeCircularSlider.frame.size.height)
        minHeight = fullHeight * 0.3
        minWidth = fullWidth * 0.3
        maxHeight = fullHeight * 1
        maxWidth = fullWidth * 1
        
        
        //let images : [UIImage?] = [UIImage(named: "Bedtime"), UIImage(named: "Wake")!]
//        sliders = [UISlider]()
        let sliderImage = UIImage(named: "sliderApple")
        let numFoods = foods?.count ?? 0
        
        let nameColumn = UIStackView()
        nameColumn.axis = .vertical
        nameColumn.spacing = 3
        nameColumn.widthAnchor.constraint(equalToConstant: fullWidth * 0.23).isActive = true

        let massColumn = UIStackView()
        massColumn.axis = .vertical
        massColumn.spacing = 3
        massColumn.widthAnchor.constraint(equalToConstant: fullWidth * 0.12).isActive = true
        
        let volumeColumn = UIStackView()
        volumeColumn.axis = .vertical
        volumeColumn.spacing = 3
        volumeColumn.widthAnchor.constraint(equalToConstant: fullWidth * 0.2).isActive = true
        
        let comparisonColumn = UIStackView()
        comparisonColumn.axis = .vertical
        comparisonColumn.spacing = 3
        comparisonColumn.widthAnchor.constraint(equalToConstant: fullWidth * 0.45).isActive = true
        
        if numFoods > 0 {
//            individualMinValue = maximumPossibleFood / 25
//            individualMaxValue = maximumPossibleFood - (Float(numFoods - 1) * individualMinValue)
            let headerFood = UILabel()
            headerFood.text = "Food"
            headerFood.font = headerFood.font.withSize(14)
            headerFood.textColor = .lightGray
            nameColumn.addArrangedSubview(headerFood)
            
            let headerMass = UILabel()
            headerMass.text = "Mass"
            headerMass.font = headerMass.font.withSize(14)
            headerMass.textColor = .lightGray
            massColumn.addArrangedSubview(headerMass)
            
            let headerVolume = UILabel()
            headerVolume.text = "Vol"
            headerVolume.font = headerVolume.font.withSize(14)
            headerVolume.textColor = .lightGray
            volumeColumn.addArrangedSubview(headerVolume)
            
            let headerComparison = UILabel()
            headerComparison.text = "Comparison"
            headerComparison.font = headerFood.font.withSize(14)
            headerComparison.textColor = .lightGray
            comparisonColumn.addArrangedSubview(headerComparison)
            
            var shift: Float = 0
            if numFoods == 2 {
                shift = 0.125
            }
            for i in 0..<numFoods {
                rangeCircularSlider.pointValues.append(CGFloat((Float(i) / Float(numFoods)) + shift))
                rangeCircularSlider.thumbImages.append(sliderImage)
                rangeCircularSlider.thumbCenters.append(CGPoint.zero)
                let foodLabel = UILabel()
                foodLabel.frame.size = CGSize(width: 75, height: 15)
                let fixedHeight = foodLabel.frame.size.height
                foodLabel.text = foods![i].name
                foodLabel.font = foodLabel.font.withSize(11)
                foodLabel.frame.size = foodLabel.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: fixedHeight))
//                foodLabel.clipsToBounds = true
//                rangeCircularSlider.clipsToBounds = true
                
                
                rangeCircularSlider.foodLabels.append(foodLabel)
                
                portionPercentages.append(1.0 / Float(numFoods))
//                let mySlider = UISlider() //frame:CGRect(x: 10, y: 100, width: 300, height: 20))
//                mySlider.minimumValue = individualMinValue
//                mySlider.maximumValue = individualMaxValue
                
    //            maximumPossibleFood += individualMaxValue
    //            minimumPossibleFood += individualMinValue
                
//                let initialValue = (individualMinValue + individualMaxValue) / 2
                
//                totalCurrentFood += initialValue
                
//                mySlider.value = initialValue
//                mySlider.isContinuous = true
//                //        mySlider.tintColor = UIColor.green
//                mySlider.addTarget(self, action: #selector(ClockViewController.sliderValueDidChange(_:)), for: .valueChanged)
//                mySlider.addTarget(self, action: #selector(ClockViewController.sliderTouchDown(_:)), for: .touchDown)
//                mySlider.addTarget(self, action: #selector(ClockViewController.sliderTouchUpInside(_:)), for: .touchUpInside)
//                mySlider.addTarget(self, action: #selector(ClockViewController.sliderTouchUpOutside(_:)), for: .touchUpOutside)
//                mySlider.tag = i
//                sliders.append(mySlider)
    //            self.sliderStackView.addArrangedSubview(mySlider)
                
                let sliderLabel = UILabel()
                sliderLabel.text = foods![i].name
                sliderLabel.font = sliderLabel.font.withSize(14)
                //sliderLabel.frame.size.width = fullWidth / 3
//                sliderLabel.widthAnchor.constraint(equalToConstant: min(120, fullWidth * 0.3)).isActive = true
                nameColumn.addArrangedSubview(sliderLabel)
                
                let sliderQuantityLabel = UILabel()
                sliderQuantityLabel.font = sliderQuantityLabel.font.withSize(14)
                //sliderQuantityLabel.text = String(currentPortion * portionPercentages[i]) + " g"
                sliderQuantities.append(sliderQuantityLabel)
//                updateSliderText(index: i)
                massColumn.addArrangedSubview(sliderQuantityLabel)
                
                let sliderVolumeLabel = UILabel()
                sliderVolumeLabel.font = sliderVolumeLabel.font.withSize(14)
                sliderVolumes.append(sliderVolumeLabel)
                volumeColumn.addArrangedSubview(sliderVolumeLabel)
                
                let sliderComparisonLabel = UILabel()
                sliderComparisonLabel.font = sliderComparisonLabel.font.withSize(14)
                sliderComparisons.append(sliderComparisonLabel)
                comparisonColumn.addArrangedSubview(sliderComparisonLabel)
                
//                let sliderLabelStackView = UIStackView()
//                sliderLabelStackView.axis = .vertical
//                sliderLabelStackView.spacing = 2
//                sliderLabelStackView.addArrangedSubview(sliderLabel)
//                sliderLabelStackView.addArrangedSubview(sliderQuantityLabel)
                
//                let sliderLabelView = UIStackView()
//                sliderLabelView.axis = .horizontal
//                sliderLabelView.spacing = 10
//                sliderLabelView.addArrangedSubview(sliderLabel)
//                sliderLabelView.addArrangedSubview(sliderQuantityLabel)
//                sliderLabelView.addArrangedSubview(sliderVolumeLabel)
//                sliderLabelView.addArrangedSubview(sliderComparisonLabel)
//                sliderLabelView.addArrangedSubview(mySlider)
//                self.sliderStackView.addArrangedSubview(sliderLabelView)
            }
            
            self.sliderStackView.addArrangedSubview(nameColumn)
            self.sliderStackView.addArrangedSubview(massColumn)
            self.sliderStackView.addArrangedSubview(volumeColumn)
            self.sliderStackView.addArrangedSubview(comparisonColumn)
        }
//        rangePossibleFood = maximumPossibleFood - minimumPossibleFood
        maximumRatio = 25 //(individualMaxValue * (Float(numFoods) - 1)) / individualMinValue
        rangeCircularSlider.maximumRatio = CGFloat(maximumRatio)
        
        let sizeRatio = CGFloat((currentPortion - minPortion) / (maxPortion - minPortion)) //CGFloat((totalCurrentFood - minimumPossibleFood) / rangePossibleFood)
        rangeCircularSliderHeightConstraint.constant = minHeight + ((maxHeight - minHeight) * sizeRatio)
        rangeCircularSliderWidthConstraint.constant = minWidth + ((maxWidth - minWidth) * sizeRatio)
        
        rangeCircularSlider.updatePointValueDiffereces()
        rangeCircularSlider.setLabelValues()
        
        updateAllFoodAmounts()
        

//        rangeCircularSliderWidthConstraint.constant = fullWidth
//        rangeCircularSliderHeightConstraint.constant = fullHeight
//        updateTexts(rangeCircularSlider)
        
//        print(fullHeight)
//        print(fullWidth)
//        print(circularSliderContainer.frame.size.width)
//        print(UIScreen.main.bounds.width)
    }
    
    func updatePortionBackgroundImage() {
        if currentPortion < 25 {
            circleSliderBackgroundImageView.image = portionImages[0]
        } else if currentPortion < 75 {
            circleSliderBackgroundImageView.image = portionImages[1]
        } else if currentPortion < 250 {
            circleSliderBackgroundImageView.image = portionImages[2]
        } else {
            circleSliderBackgroundImageView.image = portionImages[3]
        }
    }
    
//    @objc func circleSliderBeginEdit(_ sender:RangeCircularSlider!){
////        print("Begin edit!")
////        for slider in sliders {
////            slider.isUserInteractionEnabled = false
////        }
//    }
    
    @objc func circleSliderValueChanged(_ sender:MultipleCircularSlider!){
        // Total amount of food remains constant
        let differences = rangeCircularSlider.pointValueDifferences //getPointDifferentials()
        portionPercentages = differences.map{ Float($0) }
        updateAllFoodAmounts()
//        if differentials.reduce(0, +) != 1.0 {
//            print("Differential fuckup")
//            print(differentials.reduce(0, +))
//        }
//        for (i, differential) in differentials.enumerated() {
////            sliders[i].value = Float(differential) * (totalCurrentFood - minimumPossibleFood)
////            updateSliderText(index: i) // sliderQuantities[i].text = String(sliders[i].value) + " g"
//        }
    }
    
    func updateAllFoodAmounts() {
        for (i, sliderQuantity) in sliderQuantities.enumerated() {
            let mass = currentPortion * portionPercentages[i]
            sliderQuantity.text = String(format: "%.0f", mass) + "g"
            sliderVolumes[i].text = convertMassToVolume(mass: mass)
            sliderComparisons[i].text = convertMassToComparison(mass: mass)
        }
    }
    
    func convertMassToVolume(mass: Float) -> String {
        if mass < 6 {
            return "1 tsp"
        } else if mass < 25 { // 4 table
            return String(format: "%.0f", mass / 4) + " tbsp"
        } else if mass < 100 { // cup
            return String(format: "%.1f", round(mass / 100 * 2) / 2) + " cups"
        } else {
            return String(format: "%.1f", round(mass / 100 * 2) / 2) + " cups"
        }
    }
    
    func convertMassToComparison(mass: Float) -> String {
        if mass < 4 {
            return "Pecan"
        } else if mass < 13 {
            return "Strawberry"
        } else if mass < 40 {
            return "Plum"
        } else if mass < 80 {
            return "Egg"
        } else if mass < 160 {
            return "Chicken Breast"
        } else if mass < 200 {
            return "Apple"
        } else {
            return "T-Bone Steak"
        }
    }
    
//    func updateSliderText(index: Int){
////        let roundedValue = round(sliders[index].value * 2) / 2
////        sliderQuantities[index].text = String(roundedValue) + " g"
//    }
//
//    func updateAllSliderText(){
////        for i in 0..<sliders.count {
////            updateSliderText(index: i)
////        }
//    }
    
//    @objc func circleSliderEndEdit(_ sender:RangeCircularSlider!){
////        print("End edit!")
////        for slider in sliders {
////            slider.isUserInteractionEnabled = true
////        }
//    }
    
    @IBAction func portionSliderValueChanged(_ sender: UISlider) {
        
        currentPortion = sender.value
        let sizeRatio = CGFloat((currentPortion - minPortion) / (maxPortion - minPortion)) //CGFloat((totalCurrentFood - minimumPossibleFood) / rangePossibleFood)
//        print(String(sender.value) + "/" + String(describing: sizeRatio))
        rangeCircularSliderHeightConstraint.constant = minHeight + ((maxHeight - minHeight) * sizeRatio)
        rangeCircularSliderWidthConstraint.constant = minWidth + ((maxWidth - minWidth) * sizeRatio)
        updatePortionBackgroundImage()
        rangeCircularSlider.setNeedsDisplay()
        updateAllFoodAmounts()
    }
    
    
//    @objc func sliderValueDidChange(_ sender:UISlider!)
//    {
//
////        print("Slider value changed")
//
//        // Use this code below only if you want UISlider to snap to values step by step
//        let roundedStepValue = round(sender.value / step) * step
////        sender.value = roundedStepValue
//
////        rangeCircularSlider.sendActions(for: .editingDidBegin)
////        rangeCircularSlider.sendActions(for: .valueChanged)
//
//
////        rangeCircularSlider.pointValues[0] = CGFloat(roundedStepValue) / 100.0
//
//        let sliderIndex = sender.tag
//        var newTotalCurrentFood: Float = 0
////        var totalRangeFood: Float = 0
////        for slider in sliders {
////            newTotalCurrentFood += slider.value
//////            totalRangeFood += (slider.value - minValue)
////        }
//        totalCurrentFood = newTotalCurrentFood
//        print(totalCurrentFood)
//        let sizeRatio = CGFloat((totalCurrentFood - minimumPossibleFood) / (rangePossibleFood))
//
//        var newDifferentials = [Float]()
////        for slider in sliders {
////            newDifferentials.append(slider.value / totalCurrentFood)
////        }
//
//        rangeCircularSlider.updatePointValues(differentials: newDifferentials, updateIndex: sliderIndex)
//
////        let size = rangeCircularSlider.frame.size
//        //rangeCircularSlider.frame.size = CGSize(width: CGFloat(fullWidth * currentToTotal), height: CGFloat(fullHeight * currentToTotal))
//        rangeCircularSliderHeightConstraint.constant = minHeight + ((maxHeight - minHeight) * sizeRatio)
//        rangeCircularSliderWidthConstraint.constant = minWidth + ((maxWidth - minWidth) * sizeRatio)
//        rangeCircularSlider.setNeedsDisplay()
//
//        updateAllSliderText()
////        print("Slider \(Int(sender.tag)) step value \(Int(roundedStepValue)) point value \(Float(rangeCircularSlider.pointValues[0]))")
//    }
    
//    @objc func sliderTouchDown(_ sender:UISlider!)
//    {
////        print("Slider touch down")
//        rangeCircularSlider.isUserInteractionEnabled = false
////        print("Circle Slider Locked")
//    }

//    @objc func sliderTouchUpInside(_ sender:UISlider!)
//    {
//        sliderTouchUp()
//    }
    
//    @objc func sliderTouchUpOutside(_ sender:UISlider!)
//    {
//        sliderTouchUp()
//    }
    
//    func sliderTouchUp()
//    {
////        print("Slider touch up")
////        print(rangeCircularSlider.radius)
////        let size = rangeCircularSlider.frame.size
////        rangeCircularSlider.frame.width *= 0.8
////        rangeCircularSlider.frame.height *= 0.8
//            //= CGRectMake(f.minX, f.minY, f.width * 0.8, f.height * 0.8)
////        rangeCircularSlider.frame.size = CGSize(width: size.width * 0.8, height: size.height * 0.8)
////        rangeCircularSlider.setNeedsDisplay()
//        rangeCircularSlider.isUserInteractionEnabled = true
////        print("Circle Slider Unlocked")
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickCancelButton(_ sender: UIBarButtonItem) {
        print("Cancel!")
//        dismiss(animated: true, completion: nil)
        if let owningNavigationController = navigationController{ // Push
            owningNavigationController.popViewController(animated: true)
        }
        else {
            fatalError("The ViewController is not inside a navigation controller.")
        }
    }
    
    // MARK: - Navigation
    // This method lets you configure a view controller before it's presented.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for:segue, sender:sender)
        
        // Configure the destination view controller only when the save button is pressed.
        guard let button = sender as? UIBarButtonItem, button === saveButton else {
            return
        }
        
        if let foods = self.foods {
            for (i, food) in foods.enumerated() {
                let mass = round(currentPortion * portionPercentages[i])
                print(mass)
                food.quantity = Quantity(number: mass, measurementType: "g")
            }
        }
    }
    //    @IBAction func updateTexts(_ sender: AnyObject) {
//
//        adjustValue(value: &rangeCircularSlider.startPointValue)
//        adjustValue(value: &rangeCircularSlider.endPointValue)
//
//
////        let bedtime = TimeInterval(rangeCircularSlider.startPointValue)
////        let bedtimeDate = Date(timeIntervalSinceReferenceDate: bedtime)
////        bedtimeLabel.text = dateFormatter.string(from: bedtimeDate)
////
////        let wake = TimeInterval(rangeCircularSlider.endPointValue)
////        let wakeDate = Date(timeIntervalSinceReferenceDate: wake)
////        wakeLabel.text = dateFormatter.string(from: wakeDate)
////
////        let duration = wake - bedtime
////        let durationDate = Date(timeIntervalSinceReferenceDate: duration)
////        dateFormatter.dateFormat = "HH:mm"
////        durationLabel.text = dateFormatter.string(from: durationDate)
////        dateFormatter.dateFormat = "hh:mm a"
//    }
    
//    func adjustValue(value: inout CGFloat) {
//        let minutes = value / 60
//        let adjustedMinutes =  ceil(minutes / 5.0) * 5
//        value = adjustedMinutes * 60
//    }
}

