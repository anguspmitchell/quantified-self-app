//
//  Keywords.swift
//  Siri
//
//  Created by Angus Mitchell on 11/8/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class Keywords {
    var ambiguousWithKeywords = [
        "with"
    ]
    var cookedWithKeywords = [
        "cookedwith"
        ,"cooked with"
        ,"sauteed with"
        ,"fried with"
        ,"pan-fried with"
        ,"pan fried with"
        ,"braised with"
        ,"marinated in"
        ,"roasted with"
        ,"grilled with"
        ,"shallow-fried with"
        ,"shallow fried with"
        ,"deep-fried with"
        ,"deep fried with"
        ,"roasted with"
    ]
    
    var quantityKeywords = [
        "bowl"
        ,"handful"
        ,"plate"
    ]
    
    var measurementTypes = [
        MeasurementType(name: "Unit", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Teaspoon", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Tablespoon", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Fluid Ounce", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Gill", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Cup", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Pint", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Quart", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Gallon", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Milliliter", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Liter", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Deciliter", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Pound", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Ounce", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Milligram", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Gram", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Kilogram", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Millimeter", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Centimeter", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Meter", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Inch", aliases: nil, abbreviation: nil)
        , MeasurementType(name: "Foot", aliases: nil, abbreviation: nil)
    ]
    
    init() {}
}

public struct MeasurementType {
    var name: String
    var aliases: [String]?
    var abbreviation: String?
}
