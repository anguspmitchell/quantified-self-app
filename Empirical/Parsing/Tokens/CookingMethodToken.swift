//
//  CookingMethodToken.swift
//  Siri
//
//  Created by Angus Mitchell on 11/9/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class CookingMethodToken: Token {
    var cookingMethod: CookingMethod?
//    var children: [Token]?
    
    init?(children: [Token], cookingMethod: CookingMethod) {
        guard !children.isEmpty else {
            return nil
        }
        
        let arrayOfStringValues = children.map{ $0.string }
        let joinedString = arrayOfStringValues.joined(separator: " ")
        super.init(string: joinedString, type: TokenType.cookingMethod)
        
        self.children = children
        self.cookingMethod = cookingMethod
    }
}
