//
//  RestaurantToken.swift
//  Siri
//
//  Created by Angus Mitchell on 11/9/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class RestaurantToken: Token {
    var restaurant: String?
//    var children: [Token]?
    
    init?(children: [Token], restaurant: String) {
        guard !children.isEmpty else {
            return nil
        }
        
        let arrayOfStringValues = children.map{ $0.string }
        let joinedString = arrayOfStringValues.joined(separator: " ")
        super.init(string: joinedString, type: TokenType.restaurant)
        
        self.children = children
        self.restaurant = restaurant
    }
}
