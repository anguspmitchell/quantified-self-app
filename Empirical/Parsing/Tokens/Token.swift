//
//  Token.swift
//  Siri
//
//  Created by Angus Mitchell on 11/8/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class Token {
    var string: String
    var type: TokenType // Can this be deleted?
    var flags = [TokenFlag]()
//    var number: Float?
    var children: [Token]?
    var name: String?
    var phraseType: PhraseType?
    
    init?(children: [Token], name: String, phraseType: PhraseType) {
        guard !children.isEmpty else {
            return nil
        }
        
        let arrayOfStringValues = children.map{ $0.string }
        let joinedString = arrayOfStringValues.joined(separator: " ")
        
        self.string = joinedString
        self.children = children
        self.name = name
        self.phraseType = phraseType
        
        self.type = .parent
    }
    
    init?(string: String) {
        guard !string.isEmpty else {
            return nil
        }
        
        self.string = string
        type = TokenType.unclassified
    }
    
    init?(string: String, type: TokenType) {
        guard !string.isEmpty else {
            return nil
        }
        
        self.string = string
        self.type = type
    }
    
    func getLowercase() -> String {
        return string.lowercased()
    }
    
//    init?(type: TokenType, children: [Token], number: Float) {
//        guard !children.isEmpty else {
//            return nil
//        }
//        
//        let arrayOfStringValues = children.map{ $0.string }
//        let joinedString = arrayOfStringValues.joined(separator: " ")
//        self.string = joinedString
//        self.type = type
//        self.children = children
//        self.number = number
//    }
}

enum TokenType {
    //case with
    case toppedWith
    case cookedWith
    case and
    case quantity
    case cookingMethod
    case restaurant
    case number
    case unclassified
    case parent
    case food
}

enum TokenFlag {
    case isTextNumber
    case isNewLine
}

enum PhraseType {
    case tag
    case child
    case separator
}
