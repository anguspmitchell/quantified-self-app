//
//  QuantityToken.swift
//  Siri
//
//  Created by Angus Mitchell on 11/8/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class QuantityToken: Token {
    var quantity: Quantity?
//    var children: [Token]?
    
    init?(children: [Token], quantity: Quantity) {
        guard !children.isEmpty else {
            return nil
        }
        
        let arrayOfStringValues = children.map{ $0.string }
        let joinedString = arrayOfStringValues.joined(separator: " ")
        super.init(string: joinedString, type: TokenType.quantity)

        self.children = children
        self.quantity = quantity
    }
}
