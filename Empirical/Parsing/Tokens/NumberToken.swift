//
//  NumberToken.swift
//  Siri
//
//  Created by Angus Mitchell on 11/8/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class NumberToken: Token {
    var number: Float?
//    var children: [Token]?
    
    init?(children: [Token], number: Float) {
        guard !children.isEmpty else {
            return nil
        }
        
        let arrayOfStringValues = children.map{ $0.string }
        let joinedString = arrayOfStringValues.joined(separator: " ")
        super.init(string: joinedString, type: TokenType.number)
        
        self.children = children
        self.number = number
    }
}
