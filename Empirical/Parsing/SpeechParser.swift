//
//  SpeechParser.swift
//  Siri
//
//  Created by Angus Mitchell on 11/9/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class SpeechParser {
    init(){
        
    }
    let MAX_RESTAURANT: Int = 3
    let MAX_QUANTITY: Int = 3
    let MAX_METHOD: Int = 3
    let MAX_COOKEDWITH: Int = 3
    let MAX_TOPPEDWITH: Int = 3
    let MAX_NUMBER: Int = 5
    
    //MARK: - High Level Runner
    func parseSpeechText(speechString: String?, debug: Bool) -> ([Food]?, [Token]?) {
        if speechString == nil || speechString?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            return (nil, nil)
        }
        
        // 1) Split string by new lines
        let splitByLines = speechString!.split(separator: "\n")
        var returnFoods = [Food]()
        var returnTokens = [Token]()
        
        for (i, lineString) in splitByLines.enumerated() {
            if lineString.count > 0 {
                var speechTokens = tokenizeString(string: String(lineString))
                
                // 2) Process numeric tokens
                speechTokens = processNumericTokens(speechTokens: speechTokens)!
                
                // 3) Parse other tokens recursively
                let foods_tokens = parseSpeechTokens2(tokens: speechTokens, isWithClause: false, debug: debug, depth: 0, order: "A")
               
                // 4) Flag first token as new line
                if let foods = foods_tokens.0, let tokens = foods_tokens.1 {
                    if i > 0 && tokens.count > 0 {
                        tokens[0].flags.append(TokenFlag.isNewLine)
                    }
                    returnFoods += foods
                    returnTokens += tokens
                }
            }
        }
        
        return (returnFoods, returnTokens)
    }
    
    func tokenizeString(string: String) -> [Token] {
        let components = string.components(separatedBy: " ")
        var tokens = [Token]()
        for word in components {
            let token = Token(string: word)
            if let token = token {
                tokens.append(token)
            }
        }
        return tokens
    }
    
    //MARK: - Parse Tokens
    func printRecursionDown(tokens: [Token], depth: Int, order: String){
        var indent = ""
        var charsAllowed = 45
        for i in 0..<depth {
            indent += "   "
        }
        charsAllowed -= indent.count
        var printString = indent + "D" + String(depth) + order + " "
        var charsLeft = charsAllowed
        for token in tokens {
            if token.string.count <= charsLeft && charsLeft > 0 {
                printString += token.string + " "
                charsLeft -= (token.string.count + 1)
            } else {
                printString += "\n" + indent + "D" + String(depth) + order + " "
                printString += token.string + " "
                charsLeft = charsAllowed - token.string.count
            }
        }
        print(printString)
    }
    
    let phraseKeywords = [
        Phrase(name: "Topped With", phraseType: PhraseType.child, values: ["topped with", "covered in"])
        , Phrase(name: "Cooked With", phraseType: PhraseType.tag, values: ["cooked with", "cooked in", "tossed with"])
        , Phrase(name: "Restaurant/Brand", phraseType: PhraseType.tag, values: ["ordered from", "made by"])]
    
    class Phrase {
        var name: String
        var phraseType: PhraseType
        var values: [String]
        var maxLength: Int
        
        init(name: String, phraseType: PhraseType, values: [String]){
            self.name = name
            self.phraseType = phraseType
            self.values = values.map{ $0.lowercased() }
            self.maxLength = 0
            for value in values {
                if value.count > maxLength {
                    maxLength = value.count
                }
            }
        }
    }
    
    class Separator {
        var name: String
        var values: [String]
        var maxLength = 0
        var canToggle: Bool = true
        
        init(name: String, values: [String], canToggle: Bool){
            self.name = name
            self.values = values
            self.canToggle = canToggle
            self.maxLength = 0
            for value in values {
                if value.count > maxLength {
                    maxLength = value.count
                }
            }
        }
    }
    
    class Description {
        var name: String
        var values: [String]
        var maxLength = 0
        
        init(name: String, values: [String]){
            self.name = name
            self.values = values
            self.maxLength = 0
            for value in values {
                if value.count > maxLength {
                    maxLength = value.count
                }
            }
        }
    }
    
    let separatorKeywords = [Separator(name: "And", values: ["and"], canToggle: true)]
    
    let descriptionKeywords = [
        Description(name: "Cooking Method", values: [
            "sauteed"
            ,"fried"
            ,"pan-fried"
            ,"pan fried"
            ,"braised"
            ,"marinated"
            ,"roasted"
            ,"grilled"
            ,"shallow-fried"
            ,"shallow fried"
            ,"deep-fried"
            ,"deep fried"
            ,"roasted"])
        , Description(name: "Processed", values: ["processed"])
        , Description(name: "Organic", values: ["organic"])]

    
    func doTokensMatchPhrase(tokens: [Token], phrase: Phrase) -> Bool {
        let lowercased = tokensToLowercase(tokens: tokens)
        if phrase.values.contains(lowercased) {
            return true
        }
        return false
    }
    
    func doTokensMatchDescription(tokens: [Token], description: Description) -> Bool {
        let lowercased = tokensToLowercase(tokens: tokens)
        if description.values.contains(lowercased) {
            return true
        }
        return false
    }
    
    func doTokensMatchSeparator(tokens: [Token], separator: Separator) -> Bool {
        let lowercased = tokensToLowercase(tokens: tokens)
        if separator.values.contains(lowercased) {
            return true
        }
        return false
    }
    
    func getAllSeparatorIndices(tokens: [Token]?, separator: Separator) -> [[Int]]? {
        if tokens == nil {
            return nil
        } else if tokens!.isEmpty {
            return nil
        }
        
        var indicesOfRuns = [[Int]]()
        
        // 1 ----------
        // 2 ---------
        // 3  ---------
        // 4 --------
        // 5  --------
        // 6   --------
        // .....
        var windowSize = min(tokens!.count, separator.maxLength)
        while windowSize > 0 {
            for windowStartInx in 0..<tokens!.count - windowSize + 1 {
                let coveredIndices = indicesOfRuns.flatMap{ $0 }
                var noRunsCoverWindow = true
                if coveredIndices.contains(windowStartInx) || coveredIndices.contains(windowStartInx + windowSize - 1) {
                    noRunsCoverWindow = false
                }
                
                if noRunsCoverWindow {
                    let tokenSlice = tokens![windowStartInx..<(windowStartInx + windowSize)]
                    let runIndices = Array(windowStartInx...(windowStartInx + windowSize - 1))
                    
                    let isMatch = doTokensMatchSeparator(tokens: Array(tokenSlice), separator: separator)
                    if isMatch {
                        indicesOfRuns.append(runIndices)
                    }
                }
            }
            windowSize -= 1
        }
        
        return indicesOfRuns
    }
    
    func getAllDescriptionIndices(tokens: [Token]?, description: Description) -> [[Int]]? {
        if tokens == nil {
            return nil
        } else if tokens!.isEmpty {
            return nil
        }
        
        var indicesOfRuns = [[Int]]()
        
        // 1 ----------
        // 2 ---------
        // 3  ---------
        // 4 --------
        // 5  --------
        // 6   --------
        // .....
        var windowSize = min(tokens!.count, description.maxLength)
        while windowSize > 0 {
            for windowStartInx in 0..<tokens!.count - windowSize + 1 {
                let coveredIndices = indicesOfRuns.flatMap{ $0 }
                var noRunsCoverWindow = true
                if coveredIndices.contains(windowStartInx) || coveredIndices.contains(windowStartInx + windowSize - 1) {
                    noRunsCoverWindow = false
                }
                
                if noRunsCoverWindow {
                    let tokenSlice = tokens![windowStartInx..<(windowStartInx + windowSize)]
                    let runIndices = Array(windowStartInx...(windowStartInx + windowSize - 1))
                    
                    let isMatch = doTokensMatchDescription(tokens: Array(tokenSlice), description: description)
                    if isMatch {
                        indicesOfRuns.append(runIndices)
                    }
                }
            }
            windowSize -= 1
        }
        
        return indicesOfRuns
    }
    
    func getAllPhraseIndices(tokens: [Token]?, phrase: Phrase) -> [[Int]]? {
        if tokens == nil {
            return nil
        } else if tokens!.isEmpty {
            return nil
        }
        
        var indicesOfRuns = [[Int]]()
        
        // 1 ----------
        // 2 ---------
        // 3  ---------
        // 4 --------
        // 5  --------
        // 6   --------
        // .....
        var windowSize = min(tokens!.count, phrase.maxLength)
        while windowSize > 0 {
            for windowStartInx in 0..<tokens!.count - windowSize + 1 {
                let coveredIndices = indicesOfRuns.flatMap{ $0 }
                var noRunsCoverWindow = true
                if coveredIndices.contains(windowStartInx) || coveredIndices.contains(windowStartInx + windowSize - 1) {
                    noRunsCoverWindow = false
                }
                
                if noRunsCoverWindow {
                    let tokenSlice = tokens![windowStartInx..<(windowStartInx + windowSize)]
                    let runIndices = Array(windowStartInx...(windowStartInx + windowSize - 1))
                    
                    let isMatch = doTokensMatchPhrase(tokens: Array(tokenSlice), phrase: phrase)
                    if isMatch {
                        indicesOfRuns.append(runIndices)
                    }
                    
//                    switch(tokenType){
//                    case .number:
//                        let number = tokensToNumber(tokens: Array(tokenSlice))
//                        if let number = number {
//                            indicesAndValuesOfRuns.append((runIndices, number))
//                        }
//                    case .restaurant:
//                        let restaurant = tokensToRestaurant(tokens: Array(tokenSlice))
//                        if restaurant != nil {
//                            indicesAndValuesOfRuns.append((runIndices, nilNumber))
//                        }
//                    case .cookedWith:
//                        let cookedWith = tokensToCookedWith(tokens: Array(tokenSlice))
//                        if cookedWith != nil {
//                            indicesAndValuesOfRuns.append((runIndices, nilNumber))
//                        }
//                    case .quantity:
//                        let quantity = tokensToQuantity(tokens: Array(tokenSlice))
//                        if quantity != nil {
//                            indicesAndValuesOfRuns.append((runIndices, nilNumber))
//                        }
//                    case .cookingMethod:
//                        let method = tokensToCookingMethod(tokens: Array(tokenSlice))
//                        if method != nil {
//                            indicesAndValuesOfRuns.append((runIndices, nilNumber))
//                        }
//                    case .toppedWith:
//                        let toppedWith = tokensToToppedWith(tokens: Array(tokenSlice))
//                        if toppedWith != nil {
//                            indicesAndValuesOfRuns.append((runIndices, nilNumber))
//                        }
//                    default:
//                        break
//                    }
                }
            }
            windowSize -= 1
        }
        
        return indicesOfRuns
    }
    
    func consolidateDescriptionTokens(tokens: [Token], descriptions_indices: [(Description, [Int])]) -> [Token] {
//        guard let tokens = tokens, !tokens.isEmpty else {
//            return nil
//        }
        
        if tokens.isEmpty || descriptions_indices.count == 0 { // No numbers
            return Array(tokens)
        }
        
        var returnTokens = [Token]()
        let descriptions_indices_ascending = descriptions_indices.sorted(by: { $0.1[0] < $1.1[0] })
//        let indices_ascending = descriptions_indices_ascending.map{ $0.1 }
        
        var tokenItr = 0
        var dscrItr = 0

        while tokenItr < tokens.count {
            if dscrItr < descriptions_indices_ascending.count { // Runs of descriptions exist
                let next_description_indices = descriptions_indices_ascending[dscrItr]
                let next_description = next_description_indices.0
                let next_indices = next_description_indices.1
                
                if tokenItr == next_indices[0] { // If hit beginning of run, add all in run
                    var childrenTokens = [Token]()
                    for i in tokenItr...next_indices.last!{
                        childrenTokens.append(tokens[i])
                    }
                    
                    let newParentToken = Token(children: childrenTokens, name: next_description.name, phraseType: PhraseType.tag)
                    if let newParentToken = newParentToken {
                        returnTokens.append(newParentToken)
                    } else {
                        returnTokens += childrenTokens
                    }
                    
                    // Advance token itr, run itr
                    tokenItr = next_indices.last! + 1
                    dscrItr += 1
                    
                    //let toppedWith = tokensToToppedWith(tokens: childrenTokens)
//                    if toppedWith != nil {
//                        let newParentToken = WithToken(children: childrenTokens, type: TokenType.toppedWith)
//                        returnTokens.append(newParentToken!)
//                    } else {
//                        returnTokens += childrenTokens
//                    }
                    
//                    // Advance token itr, run itr
//                    tokenItr = nextDesc.last! + 1
//                    descItr += 1
                } else { // Otherwise just add
                    returnTokens.append(tokens[tokenItr])
                    tokenItr += 1
                }
            } else { // If we're out of descriptions, just append
                returnTokens.append(tokens[tokenItr])
                tokenItr += 1
            }
        }
        
        return returnTokens
    }
    
    func consolidateSeparatorTokens(tokens: [Token], separators_indices: [(Separator, [Int])]) -> [Token] {
        
        if tokens.isEmpty || separators_indices.count == 0 { // No numbers
            return Array(tokens)
        }
        
        var returnTokens = [Token]()
        let separators_indices_ascending = separators_indices.sorted(by: { $0.1[0] < $1.1[0] })
        
        var tokenItr = 0
        var sepItr = 0
        
        while tokenItr < tokens.count {
            if sepItr < separators_indices_ascending.count { // Runs of descriptions exist
                let next_separator_indices = separators_indices_ascending[sepItr]
                let next_separator = next_separator_indices.0
                let next_indices = next_separator_indices.1
                
                if tokenItr == next_indices[0] { // If hit beginning of run, add all in run
                    var childrenTokens = [Token]()
                    for i in tokenItr...next_indices.last!{
                        childrenTokens.append(tokens[i])
                    }
                    
                    let newParentToken = Token(children: childrenTokens, name: next_separator.name, phraseType: PhraseType.separator)
                    if let newParentToken = newParentToken {
                        returnTokens.append(newParentToken)
                    } else {
                        returnTokens += childrenTokens
                    }
                    
                    // Advance token itr, run itr
                    tokenItr = next_indices.last! + 1
                    sepItr += 1
                } else { // Otherwise just add
                    returnTokens.append(tokens[tokenItr])
                    tokenItr += 1
                }
            } else { // If we're out of descriptions, just append
                returnTokens.append(tokens[tokenItr])
                tokenItr += 1
            }
        }
        
        return returnTokens
    }
    
    func consolidatePhraseTokens(tokens: [Token], phrase_indices: (Phrase, [Int]), startIndex: Int) -> [Token] {
//        guard let tokens = tokens, !tokens.isEmpty else {
//            return nil
//        }
        
        let indices = phrase_indices.1
        if tokens.isEmpty || indices.count == 0 {
            return Array(tokens)
        }
        let clause = phrase_indices.0
        let indices_offset = indices.map { $0 - startIndex }
        var returnTokens = [Token]()
        
        var tokenItr = 0
        
        while tokenItr < tokens.count {
//            if dscrItr < descriptions_indices_ascending.count { // Runs of descriptions exist
//                let next_description_indices = descriptions_indices_ascending[dscrItr]
//                let next_description = next_description_indices.0
//                let next_indices = next_description_indices.1
            
            if tokenItr == indices_offset[0] { // If hit beginning of run, add all in run
                var childrenTokens = [Token]()
                for i in tokenItr...indices_offset.last!{
                    childrenTokens.append(tokens[i])
                }
                
                let newParentToken = Token(children: childrenTokens, name: clause.name, phraseType: PhraseType.child)
                if let newParentToken = newParentToken {
                    returnTokens.append(newParentToken)
                } else {
                    returnTokens += childrenTokens
                }
                
                // Advance token itr, run itr
                tokenItr = indices_offset.last! + 1
//                    dscrItr += 1
                
            } else { // Otherwise just add
                returnTokens.append(tokens[tokenItr])
                tokenItr += 1
            }
//            } else { // If we're out of descriptions, just append
//                returnTokens.append(tokens[tokenItr])
//                tokenItr += 1
//            }
        }
        
        return returnTokens
    }
    
    func parseSpeechTokens2(tokens: [Token]?, isWithClause: Bool, debug: Bool, depth: Int, order: String) -> ([Food]?, [Token]?) {
        guard let tokens = tokens,  tokens.count > 0  else {
            return (nil, nil)
        }
        
        // 1. Separate into Main Clause / Tag Clauses / Child Clauses
        // 1a. Split main clause by any separators
        // 2. Identify tags in main clause
        // 3. Create main clause food item
        // 4. Recurse on tag clauses (1 level deep)
        // 5. Recurse on child clauses (as many as needed)
        
        var foods = [Food]()
        var returnTokens = [Token]()
        
        // Identify indices of phrases
        var tokens_phrases_indices = [(Phrase, [Int])]()
        for phraseKeyword in self.phraseKeywords {
            let arrayOfIndices = getAllPhraseIndices(tokens: tokens, phrase: phraseKeyword)
            if let arrayOfIndices = arrayOfIndices {
                let sortedArrayOfIndices = arrayOfIndices.sorted(by: { $0[0] < $1[0] })
                for indices in sortedArrayOfIndices {
                    tokens_phrases_indices += [(phraseKeyword, indices)]
                }
            }
        }
        let tokens_phrases_indices_ascending = tokens_phrases_indices.sorted(by: { $0.1[0] < $1.1[0] })
        
        // Get first instance of clause
        var firstClauseIdx = tokens.count
        if tokens_phrases_indices_ascending.count > 0 {
            if tokens_phrases_indices_ascending[0].1.count > 0 {
                firstClauseIdx = tokens_phrases_indices_ascending[0].1[0]
            }
        }
        var mainClause: [Token] = Array(tokens[0..<firstClauseIdx])
        
        // Split up entire sentence by main clause separators
        var separators_indices = [(Separator, [Int])]()
        for separator in self.separatorKeywords {
            let arrayOfIndices = getAllSeparatorIndices(tokens: mainClause, separator: separator)
            if let arrayOfIndices = arrayOfIndices {
                for indices in arrayOfIndices {
                    separators_indices += [(separator, indices)]
                }
            }
        }
        
        var tokens_sep = consolidateSeparatorTokens(tokens: tokens, separators_indices: separators_indices)
        
        var separatorTokens_subjectTokens = [(Token?, [Token])]()
        var priorSeparator: Token? = nil
        var currentSubjectTokens = [Token]()
        var i = 0
        while i < tokens_sep.count {
            if tokens_sep[i].phraseType == .separator { // add previous
                separatorTokens_subjectTokens.append((priorSeparator, currentSubjectTokens))
                
                currentSubjectTokens.removeAll()
                priorSeparator = tokens_sep[i]
            } else {
                currentSubjectTokens.append(tokens_sep[i])
            }
            i += 1
        }
        separatorTokens_subjectTokens.append((priorSeparator, currentSubjectTokens))
        
        for separatorToken_subjectToken in separatorTokens_subjectTokens {
            var subjectTokens = separatorToken_subjectToken.1
            let separatorToken = separatorToken_subjectToken.0
            
            // Identify indices of phrases
            var phrases_indices = [(Phrase, [Int])]()
            for phraseKeyword in self.phraseKeywords {
                let arrayOfIndices = getAllPhraseIndices(tokens: subjectTokens, phrase: phraseKeyword)
                if let arrayOfIndices = arrayOfIndices {
                    let sortedArrayOfIndices = arrayOfIndices.sorted(by: { $0[0] < $1[0] })
                    for indices in sortedArrayOfIndices {
                        phrases_indices += [(phraseKeyword, indices)]
                    }
                }
            }
            let phrases_indices_ascending = phrases_indices.sorted(by: { $0.1[0] < $1.1[0] })
            
            // Get first instance of clause
            var firstSubjectClauseIdx = subjectTokens.count
            if tokens_phrases_indices_ascending.count > 0 {
                if tokens_phrases_indices_ascending[0].1.count > 0 {
                    firstSubjectClauseIdx = tokens_phrases_indices_ascending[0].1[0]
                }
            }
            var mainSubjectClause: [Token] = Array(subjectTokens[0..<firstSubjectClauseIdx])

            
            // Split up phrases, store in array of token arrays
            var phrases_tokens = [(Phrase, [Token])]()
            var priorEndIndex = 0
            
            for (i, phrase_indices) in phrases_indices_ascending.enumerated() {
                let phraseKeyword = phrase_indices.0
                let indices = phrase_indices.1
                
                if indices.count > 0 {
                    let startIndex = indices[0]
                    var endIndexExclusive = subjectTokens.count // Stop at end of array...
                    
                    // ... unless there is another phrase after this
                    if i < phrases_indices_ascending.count - 1 && startIndex >= priorEndIndex {
                        var nextFound = false
                        var j = i + 1
                        while j < phrases_indices_ascending.count && !nextFound {
                            var nextIndices = phrases_indices_ascending[j].1
                            if nextIndices.count > 0 {
                                if nextIndices[0] > indices.last! { // Cannot overlap
                                    endIndexExclusive = nextIndices[0]
                                    nextFound = true
                                }
                            }
                            j += 1
                        }
                    }
                    
                    var tokensInPhrase = Array(subjectTokens[startIndex..<endIndexExclusive])
                    tokensInPhrase = consolidatePhraseTokens(tokens: tokensInPhrase, phrase_indices: phrase_indices, startIndex: startIndex)
                    phrases_tokens += [(phraseKeyword, tokensInPhrase)]
                    
                    priorEndIndex = endIndexExclusive
                }
            }
            
            // Identify indices of descriptions in mainSubjectClause
            var descriptions_indices = [(Description, [Int])]()
            for description in self.descriptionKeywords {
                let arrayOfIndices = getAllDescriptionIndices(tokens: mainSubjectClause, description: description)
                if let arrayOfIndices = arrayOfIndices {
                    for indices in arrayOfIndices {
                        descriptions_indices += [(description, indices)]
                    }
                }
            }
            
            mainSubjectClause = consolidateDescriptionTokens(tokens: mainSubjectClause, descriptions_indices: descriptions_indices)
            
    //        print(subjectTokens.count)
    //        print(phrases_indices.count)
    //        print(descriptions_indices.count)
            

            
            for token in mainSubjectClause {
                if token.phraseType == nil {
                    token.type = .food
                }
            }
            let foodName = mainSubjectClause.filter{ $0.type == .food }.map{ $0.string.capitalizeFirstLetter() }.joined(separator: " ")
            let food = Food(name: foodName)
            
            if let food = food {
                // Add tags to food object
                let tagTokens = mainSubjectClause.filter { $0.phraseType == .tag }
                for tagToken in tagTokens {
                    let tagName = tagToken.name
                    if let tagName = tagName {
                        food.tags[tagName] = tagToken.string
                    }
                }
                
                // For phrases...
                var new_phrases_tokens = [(Phrase, [Token])]()
                for phrase_tokens in phrases_tokens {
                    let phraseKeyword = phrase_tokens.0
                    let tokensInPhrase = phrase_tokens.1
                    
                    // ... if child clause, then recurse
                    if phraseKeyword.phraseType == .child && tokensInPhrase.count > 1 {
                        let phraseKeywordTokens = [tokensInPhrase[0]]
                        let phraseSubstanceTokens = Array(tokensInPhrase[1..<tokensInPhrase.count])
                        let phrase_food_tokens = parseSpeechTokens2(tokens: phraseSubstanceTokens, isWithClause: false, debug: false, depth: 0, order: "A")
                        let returnedPhraseFood = phrase_food_tokens.0
                        let returnedPhraseTokens = phrase_food_tokens.1
                        if let returnedPhraseFood = returnedPhraseFood, let returnedPhraseTokens = returnedPhraseTokens {
                            food.children += returnedPhraseFood
                            let stitchedTogetherTokens = phraseKeywordTokens + returnedPhraseTokens
                            new_phrases_tokens.append((phraseKeyword, stitchedTogetherTokens))
                        } else {
                            new_phrases_tokens.append(phrase_tokens)
                        }
                    }
                    // ... if tag clause, then add tag to food object
                    else if phraseKeyword.phraseType == .tag {
                        let valueTokens = tokensInPhrase.filter{ $0.name == nil }
                        let valueString = valueTokens.map{ $0.string.capitalizeFirstLetter() }.joined(separator: " ")
                        if valueString.count > 0 {
                            food.tags[phraseKeyword.name] = valueString
                        }
                        new_phrases_tokens.append(phrase_tokens)
                    } else {
                        new_phrases_tokens.append(phrase_tokens)
                    }
                }
                
                // Add food to list of foods to return
                foods.append(food)
                
                // Add modified tokens to list of tokens to return
                if let separatorToken = separatorToken {
                    returnTokens += [separatorToken]
                }
                returnTokens += mainSubjectClause
                for new_clause_token in new_phrases_tokens {
                    returnTokens += new_clause_token.1
                }
            } else {
                if let separatorToken = separatorToken {
                    returnTokens += [separatorToken]
                }
                returnTokens += mainSubjectClause
                for clause_token in phrases_tokens {
                    returnTokens += clause_token.1
                }
            }
        }
        return (foods, returnTokens)
    }
    
    func parseSpeechTokens(speechTokens: [Token]?, isWithClause: Bool, debug: Bool, depth: Int, order: String) -> ([Food]?, [Token]?) {
        if speechTokens == nil || speechTokens?.count == 0 {
            return (nil, nil)
        }
        
        // DEBUG
        if debug {
            printRecursionDown(tokens: speechTokens!, depth: depth, order: order)
        }
        
        var foods = [Food]()
        var returnTokens = [Token]()
        
        var mutableSpeechTokens = [Token](speechTokens!)
        //        var withIndices = [Int]()
//        var cookedWithIndices = [Int]()
//        var toppedWithIndices = [Int]()
        var andIndices = [Int]()
        var restaurantIndices = [Int]()
        var restaurant: String?
        restaurant = nil
        //var quantityIndices = [Int]()
        
        //for var (i, token) in mutableSpeechTokens.enumerated() {
        for i in 0..<mutableSpeechTokens.count {
            let token = mutableSpeechTokens[i]
            var tokenType = TokenType.unclassified
            if isCookedWith(string: token.string) {
                tokenType = TokenType.cookedWith
//                cookedWithIndices.append(i)
            } else if isToppedWith(string: token.string) {
                tokenType = TokenType.toppedWith
//                toppedWithIndices.append(i)
            } else if isAnd(string: token.string){
                tokenType = TokenType.and
                andIndices.append(i)
            } else if isQuantity(string: token.string) || mutableSpeechTokens[i].type == TokenType.quantity { // HAck
                tokenType = TokenType.quantity
                //quantityIndices.append(i)
            } else if isMethod(string: token.string) || mutableSpeechTokens[i].type == TokenType.cookingMethod {
                tokenType = TokenType.cookingMethod
                //methodIndices.append(i)
            } else if isRestaurant(string: token.string) || mutableSpeechTokens[i].type == TokenType.restaurant {
                tokenType = TokenType.restaurant
                restaurantIndices.append(i)
            }
            mutableSpeechTokens[i].type = tokenType
        }
        
//        for (i, revIndex) in restaurantIndices.reversed().enumerated() {
//            if i == restaurantIndices.count - 1 {
//                restaurant = mutableSpeechTokens[revIndex].string
//            }
//            mutableSpeechTokens.remove(at: revIndex)
//        }
        

//        if toppedWithIndices.count > 0 && toppedWithIndices[0] > 0 {
//            firstToppedWith = toppedWithIndices[0]
//        }
        
//        if cookedWithIndices.count > 0 && cookedWithIndices[0] > 0 {
//            firstCookedWith = cookedWithIndices[0]
//        }
        
        let toppedWithIndicesAndValues = getIndicesOfTokenRuns(tokens: mutableSpeechTokens, tokenType: .toppedWith, maxRunLength: MAX_TOPPEDWITH)
        var firstToppedWith = -1
        if toppedWithIndicesAndValues != nil && toppedWithIndicesAndValues!.count > 0 {
            let toppedWithIndices = toppedWithIndicesAndValues!.map{ $0.0 }
            if toppedWithIndices[0].count > 0 && toppedWithIndices[0][0] > 0 {
                firstToppedWith = toppedWithIndices[0][0]
                mutableSpeechTokens = consolidateToppedWithTokens(tokens: mutableSpeechTokens, indices: [toppedWithIndices[0]])!
            }
        }
        
        let cookedWithIndicesAndValues = getIndicesOfTokenRuns(tokens: mutableSpeechTokens, tokenType: .cookedWith, maxRunLength: MAX_COOKEDWITH)
        var firstCookedWith = -1
        if cookedWithIndicesAndValues != nil && cookedWithIndicesAndValues!.count > 0 {
            let cookedWithIndices = cookedWithIndicesAndValues!.map{ $0.0 }
            if cookedWithIndices[0].count > 0 && cookedWithIndices[0][0] > 0 {
                firstCookedWith = cookedWithIndices[0][0]
                mutableSpeechTokens = consolidateCookedWithTokens(tokens: mutableSpeechTokens, indices: [cookedWithIndices[0]])!
            }
        }
        
        // Here for now
        mutableSpeechTokens = processQuantityTokens(tokens: mutableSpeechTokens, startIndex: 0, endIndex: 0)!
        mutableSpeechTokens = processMethodTokens(tokens: mutableSpeechTokens, startIndex: 0, endIndex: 0)!
        mutableSpeechTokens = processRestaurantTokens(tokens: mutableSpeechTokens)!
        
        let restaurantTokens = mutableSpeechTokens.filter{ $0 is RestaurantToken }
        if restaurantTokens.count > 0 {
            restaurant = mutableSpeechTokens.filter{ $0 is RestaurantToken }.map{ $0 as! RestaurantToken }.first?.restaurant
        }
        
        var distinctFoods = [[Token]]()
        var distinctFoodsAndTokens: [[Token]?]
        distinctFoodsAndTokens = [nil]
        // "corn and beans" --> ["corn", "beans"] & ["and", nil]
        
        //        var validQuantityIndices = [Int]()
        //        for quantityIndex in quantityIndices {
        //            if (quantityIndex < firstToppedWith || firstToppedWith < 0) && (quantityIndex < firstCookedWith || firstCookedWith < 0) {
        //                validQuantityIndices.append(quantityIndex)
        //            }
        //        }
        
        if isWithClause {
            var validAndIndices = [Int]()
            for andIndex in andIndices {
                if (andIndex < firstToppedWith || firstToppedWith < 0) && (andIndex < firstCookedWith || firstCookedWith < 0) {
                    validAndIndices.append(andIndex)
                }
            }
            
            if validAndIndices.count > 0 {
                // First and last (0 to idx + idx to end)
                // Last (> 0) (prior to idx + idx to end)
                // First (0 to idx)
                // Middle (prior to idx)
                distinctFoodsAndTokens = [[Token]?]()
                for (i, currentIndex) in validAndIndices.enumerated() {
                    var priorIndex = 0
                    if i > 0 {
                        priorIndex = validAndIndices[i - 1] + 1 // skip the "and" itself
                    }
                    
                    distinctFoodsAndTokens.append(Array(mutableSpeechTokens[currentIndex...currentIndex]))
                    distinctFoods.append(Array(mutableSpeechTokens[priorIndex..<currentIndex]))
                    if i == validAndIndices.count - 1  && currentIndex < mutableSpeechTokens.count - 1 {
                        distinctFoods.append(Array(mutableSpeechTokens[(currentIndex + 1)..<mutableSpeechTokens.count]))
                        distinctFoodsAndTokens.append(nil)
                    }
                }
            } else {
                distinctFoods.append(Array(mutableSpeechTokens[0..<mutableSpeechTokens.count]))
            }
        } else {
            distinctFoods.append(Array(mutableSpeechTokens[0..<mutableSpeechTokens.count]))
        }
        // Go through each distinct food
        // pick out quantity
        // keep track of that quantity and how many distinct foods
        // it applies to. when you hit the next quantity (or end of list)
        // go back and associate the quantity with everything before it, as well
        // as the ratio
        var distinctFoodsWithQuantitiesAndMethods = [([Token], Quantity?, [String])]()
        for distinctFood in distinctFoods {
            distinctFoodsWithQuantitiesAndMethods.append((distinctFood, nil, []))
        }
        
        var methodTokenIndicesToDelete = [[Int]]()
        var currentQuantity: Quantity?
        var currentQuantityIndex = -1
        var currentQuantitySpan = 0
        var quantityActive = false
        
        var methodActive = false
        var currentMethods = [String]()
        
        for i in 0..<distinctFoods.count {
            var distinctFood = distinctFoods[i]
            
            var validQuantityIndices = [Int]()
            var validMethodIndices = [Int]()
            //var validQuantityOrMethodIndices = [Int]()
            var withClauseEncountered = false
            var j = 0
            while !withClauseEncountered && j < distinctFood.count { //{for token in distinctFood {
                let token = distinctFood[j]
                if token.type == TokenType.cookedWith || token.type == TokenType.toppedWith {
                    withClauseEncountered = true
                } else if token.type == TokenType.quantity || token.type == TokenType.cookingMethod {
                    //validQuantityOrMethodIndices.append(j)
                    if token.type == TokenType.quantity {
                        validQuantityIndices.append(j)
                    }
                    if token.type == TokenType.cookingMethod {
                        validMethodIndices.append(j)
                    }
                }
                j += 1
            }
            
            if validQuantityIndices.count > 0 { // i.e. there exists quantities
                // associated with this distinct food
                if quantityActive { // wrap up prior
                    for k in currentQuantityIndex..<i {
                        if var currentQuantity = currentQuantity {
                            currentQuantity.divisor = currentQuantitySpan
                            distinctFoodsWithQuantitiesAndMethods[k].1 = currentQuantity // + String(currentQuantitySpan)
                        }
                    }
                }
                //                else {
                //                    quantityActive = true
                //                }
                
                if distinctFood[validQuantityIndices[0]] is QuantityToken {
                    if !quantityActive {
                        quantityActive = true
                    }
                    var quantityToken = distinctFood[validQuantityIndices[0]] as! QuantityToken
                    currentQuantity = quantityToken.quantity//distinctFood[validQuantityIndices[0]]//.string
                    currentQuantitySpan = 1
                    currentQuantityIndex = i
                }
            } else {
                if quantityActive {
                    currentQuantitySpan += 1
                }
            }
            
            // inactive - dont hit
            // inactive - hit quantity
            // active   - dont hit (increment counter)
            // active   - hit quantity (wrap up prior)
            // active   - list expires (wrap up prior)
            
            if validMethodIndices.count > 0 { // Hit new method
                currentMethods = []
                for index in validMethodIndices {
                    currentMethods += [distinctFood[index].string]
                }
                distinctFoodsWithQuantitiesAndMethods[i].2 = currentMethods
                methodActive = true
            } else {
                if methodActive {
                    distinctFoodsWithQuantitiesAndMethods[i].2 = currentMethods
                }
            }
            
            // inactive - dont hit
            // inactive - hit new method (add & store)
            // active   - dont hit (add stored)
            // active   - hit new method (add & store)
            
            methodTokenIndicesToDelete.append(validMethodIndices)//(validQuantityOrMethodIndices)
        }
        
        if quantityActive { // wrap up prior
            for k in currentQuantityIndex..<distinctFoods.count {
                if var currentQuantity = currentQuantity {
                    currentQuantity.divisor = currentQuantitySpan
                    distinctFoodsWithQuantitiesAndMethods[k].1 = currentQuantity // + String(currentQuantitySpan)
                }
                //                distinctFoodsWithQuantitiesAndMethods[k].1 = currentQuantity + String(currentQuantitySpan)
            }
        }
        
        //        // Delete quantity and method tokens
        //        for i in 0..<methodTokenIndicesToDelete.count {
        //            for index in methodTokenIndicesToDelete[i].reversed() {
        //                distinctFoodsWithQuantitiesAndMethods[i].0.remove(at: index)
        //            }
        //        }
        
        for (distinctFoodWithQuantityIndex, distinctFoodWithQuantity) in distinctFoodsWithQuantitiesAndMethods.enumerated() {
            
            var distinctFood = distinctFoodWithQuantity.0
            var distinctCookedWithIndices = [Int]()
            var distinctToppedWithIndices = [Int]()
            
            for var (i, token) in distinctFood.enumerated() {
                if token.type == TokenType.cookedWith {
                    distinctCookedWithIndices.append(i)
                } else if token.type == TokenType.toppedWith {
                    distinctToppedWithIndices.append(i)
                }
            }
            
            
            var distinctFirstToppedWith = -1
            if distinctToppedWithIndices.count > 0 && distinctToppedWithIndices[0] > 0 {
                distinctFirstToppedWith = distinctToppedWithIndices[0]
            }
            var distinctFirstCookedWith = -1
            if distinctCookedWithIndices.count > 0 && distinctCookedWithIndices[0] > 0 {
                distinctFirstCookedWith = distinctCookedWithIndices[0]
            }
            
            
            var foodTokens = ArraySlice<Token>()
            //var withClauseTokens = ArraySlice<Token>()
            var cookedWithTokens = ArraySlice<Token>()
            var toppedWithTokens = ArraySlice<Token>()
            var withKeywords = [ArraySlice<Token>]()
            //var toppedWithKeywords = ArraySlice<Token>()
            
            // Neither clause exists
            // cooked with exists
            // topped with exists
            // both exist
            // cookedwith is first
            // toppedwith is first
            
            // Default: Neither clause exists
            foodTokens = distinctFood[0..<distinctFood.count]
            
            if distinctFirstCookedWith > 0 || distinctFirstToppedWith > 0 {
                if distinctFirstCookedWith > 0 && distinctFirstToppedWith > 0 { // Both exist
                    if distinctFirstCookedWith > distinctFirstToppedWith { // Topped with first
                        if distinctFirstToppedWith < distinctFood.count - 2 {
                            foodTokens = distinctFood[0..<distinctFirstToppedWith]
                            
                            withKeywords += [distinctFood[distinctFirstToppedWith...distinctFirstToppedWith]]
                            toppedWithTokens = distinctFood[(distinctFirstToppedWith + 1)..<distinctFirstCookedWith]
                            withKeywords += [distinctFood[distinctFirstCookedWith...distinctFirstCookedWith]]
                            cookedWithTokens = distinctFood[(distinctFirstCookedWith + 1)..<distinctFood.count]
                        }
//                        if distinctFirstCookedWith < distinctFood.count - 1 {
//                            cookedWithTokens = distinctFood[(distinctFirstCookedWith + 1)..<distinctFood.count]
//                        }
                    } else { // Cooked with first
                        if distinctFirstCookedWith < distinctFood.count - 2 && distinctFirstToppedWith < distinctFood.count - 1{
                            foodTokens = distinctFood[0..<distinctFirstCookedWith]
                            
                            withKeywords += [distinctFood[distinctFirstCookedWith...distinctFirstCookedWith]]
                            cookedWithTokens = distinctFood[(distinctFirstCookedWith + 1)..<distinctFirstToppedWith]
                            
                            withKeywords += [distinctFood[distinctFirstToppedWith...distinctFirstToppedWith]]
                            toppedWithTokens = distinctFood[(distinctFirstToppedWith + 1)..<distinctFood.count]
                        }
//                        if distinctFirstToppedWith < distinctFood.count - 1 {
//                            toppedWithTokens = distinctFood[(distinctFirstToppedWith + 1)..<distinctFood.count]
//                        }
                    }
                } else if distinctFirstCookedWith > 0 { // Only cooked with
                    if distinctFirstCookedWith < distinctFood.count - 1 {
                        foodTokens = distinctFood[0..<distinctFirstCookedWith]
                        
                        withKeywords += [distinctFood[distinctFirstCookedWith...distinctFirstCookedWith]]
                        cookedWithTokens = distinctFood[(distinctFirstCookedWith + 1)..<distinctFood.count]
                    }
                } else if distinctFirstToppedWith > 0 { // Only topped with
                    if distinctFirstToppedWith < distinctFood.count - 1 {
                        foodTokens = distinctFood[0..<distinctFirstToppedWith]
                        
                        withKeywords += [distinctFood[distinctFirstToppedWith...distinctFirstToppedWith]]
                        toppedWithTokens = distinctFood[(distinctFirstToppedWith + 1)..<distinctFood.count]
                    }
                }
            }
//            else {
//                foodTokens = distinctFood[0..<distinctFood.count]
//            }
            //        if firstWith > -1 && firstWith < speechTokens!.count - 1 {
            //            foodTokens = speechTokens![0..<firstWith]
            //            withClauseTokens = speechTokens![(firstWith + 1)..<speechTokens!.count]
            //        } else {
            //            foodTokens = speechTokens![0..<speechTokens!.count]
            //        }
            let food = Food(name: foodTokens.filter{!($0 is NumberToken) && !($0 is QuantityToken) && !($0 is CookingMethodToken) && !($0 is RestaurantToken) && !($0 is WithToken)}.map{$0.string.capitalizeFirstLetter()}.joined(separator: " "))
            if let food = food {
                let toppedWithTuple: ([Food]?, [Token]?) = parseSpeechTokens(speechTokens: Array(toppedWithTokens), isWithClause: true, debug: debug, depth: depth + 1, order: "A")
                let toppedWith = toppedWithTuple.0
                let processedToppedWithTokens = toppedWithTuple.1
                food.toppedWith = toppedWith
                
                let cookedWithTuple: ([Food]?, [Token]?) = parseSpeechTokens(speechTokens: Array(cookedWithTokens), isWithClause: true, debug: debug, depth: depth + 1, order: "B")
                let cookedWith = cookedWithTuple.0
                let processedCookedWithTokens = cookedWithTuple.1
                food.cookedWith = cookedWith
                
                food.quantity = distinctFoodWithQuantity.1
                
                food.cookingMethods = distinctFoodWithQuantity.2
                
                if let restaurant = restaurant {
                    food.restaurant = restaurant
                }
                
                foods.append(food)
                
                // All this needs to get brought out in case food == nil right?
                returnTokens += Array(foodTokens)
                var withClauses = [([Token], Int)]()
                if let processedCookedWithTokens = processedCookedWithTokens, distinctFirstCookedWith > 0 {
                    withClauses += [(processedCookedWithTokens, distinctFirstCookedWith)]
                }
                if let processedToppedWithTokens = processedToppedWithTokens, distinctFirstToppedWith > 0 {
                    withClauses += [(processedToppedWithTokens, distinctFirstToppedWith)]
                }
                let sortedWithClauses = withClauses.sorted { $0.1 < $1.1 }
                
                for (i, withClause) in sortedWithClauses.enumerated() {
                    returnTokens += Array(withKeywords[i])
                    returnTokens += withClause.0
                }
                if distinctFoodsAndTokens[distinctFoodWithQuantityIndex] != nil {
                    returnTokens += distinctFoodsAndTokens[distinctFoodWithQuantityIndex]!
                }
                
                if debug {
                    printRecursionUp(tokenRuns: [Array(foodTokens)] + sortedWithClauses.map{ $0.0 }, depth: depth, order: order)
                }
            }
        }
        return (foods, returnTokens)
    }
    
    func printRecursionUp(tokenRuns: [[Token]?], depth: Int, order: String){
        var indent = ""
        var charsAllowed = 45
        for _ in 0..<depth {
            indent += "   "
        }
        charsAllowed -= indent.count
        var printString = indent + "U" + String(depth) + order + " "
        var charsLeft = charsAllowed
        for (tokenRunIdx, tokenRun) in tokenRuns.enumerated() {
            if tokenRunIdx > 0 {
                printString += " + "
                charsLeft -= 3
            }
            printString += "("
            charsLeft -= 1
            if tokenRun == nil {
                printString += "nil"
                charsLeft -= 3
            } else {
                for (tokenIdx, token) in tokenRun!.enumerated() {
                    if token.string.count <= charsLeft && charsLeft > 0 {
                        if tokenIdx > 0 {
                            printString += " "
                            charsLeft -= 1
                        }
                        printString += token.string
                        charsLeft -= token.string.count
                    } else {
                        printString += "\n" + indent + "U" + String(depth) + order + " "
                        printString += token.string
                        charsLeft = charsAllowed - token.string.count
                    }
                }
            }
            printString += ")"
            charsLeft -= 1
        }
        print(printString)
    }
    
    //MARK: - Process Tokens
    //MARK: Token search and consolidation
    func getIndicesOfTokenRuns(tokens: [Token]?, tokenType: TokenType, maxRunLength: Int) -> [([Int], Float?)]? {
        if tokens == nil {
            return nil
        } else if tokens!.isEmpty {
            return nil
        }
        
        var indicesAndValuesOfRuns = [([Int], Float?)]()
        
        // 1 ----------
        // 2 ---------
        // 3  ---------
        // 4 --------
        // 5  --------
        // 6   --------
        // .....
        var windowSize = min(tokens!.count, maxRunLength)
        while windowSize > 0 {
            for windowStartInx in 0..<tokens!.count - windowSize + 1 {
                let coveredIndices = indicesAndValuesOfRuns.map{ $0.0 }.flatMap{ $0 }
                var noRunsCoverWindow = true
                if coveredIndices.contains(windowStartInx) || coveredIndices.contains(windowStartInx + windowSize - 1) {
                    noRunsCoverWindow = false
                }
                
                if noRunsCoverWindow {
                    let tokenSlice = tokens![windowStartInx..<(windowStartInx + windowSize)]
                    let indicesOfRun = Array(windowStartInx...(windowStartInx + windowSize - 1))
                    let nilNumber: Float? = nil
                    
                    switch(tokenType){
                    case .number:
                        let number = tokensToNumber(tokens: Array(tokenSlice))
                        if let number = number {
                            indicesAndValuesOfRuns.append((indicesOfRun, number))
                        }
                    case .restaurant:
                        let restaurant = tokensToRestaurant(tokens: Array(tokenSlice))
                        if restaurant != nil {
                            indicesAndValuesOfRuns.append((indicesOfRun, nilNumber))
                        }
                    case .cookedWith:
                        let cookedWith = tokensToCookedWith(tokens: Array(tokenSlice))
                        if cookedWith != nil {
                            indicesAndValuesOfRuns.append((indicesOfRun, nilNumber))
                        }
                    case .quantity:
                        let quantity = tokensToQuantity(tokens: Array(tokenSlice))
                        if quantity != nil {
                            indicesAndValuesOfRuns.append((indicesOfRun, nilNumber))
                        }
                    case .cookingMethod:
                        let method = tokensToCookingMethod(tokens: Array(tokenSlice))
                        if method != nil {
                            indicesAndValuesOfRuns.append((indicesOfRun, nilNumber))
                        }
                    case .toppedWith:
                        let toppedWith = tokensToToppedWith(tokens: Array(tokenSlice))
                        if toppedWith != nil {
                            indicesAndValuesOfRuns.append((indicesOfRun, nilNumber))
                        }
                    default:
                        break
                    }
                }
            }
            windowSize -= 1
        }
        
        return indicesAndValuesOfRuns
    }
    
    func processRestaurantTokens(tokens: [Token]?) -> [Token]? {
        if tokens == nil {
            return nil
        }
        var returnTokens = [Token]()
        
        let indicesAndValuesOfAllRuns = getIndicesOfTokenRuns(tokens: tokens!, tokenType: .restaurant, maxRunLength: MAX_RESTAURANT)
        
        
        if indicesAndValuesOfAllRuns == nil || indicesAndValuesOfAllRuns!.count == 0 {
            return Array(tokens!)
        }
        
        // Order array of indices by reverse start position
        let indicesOfAllRuns = indicesAndValuesOfAllRuns!.map{ $0.0 }
        var sortedRuns = indicesOfAllRuns.sorted(by: { $0[0] < $1[0] })
        var tokenItr = 0
        var runItr = 0
        
        while tokenItr < tokens!.count {
            if runItr < sortedRuns.count { // Runs of #s exist
                var nextRun = sortedRuns[runItr]
                if tokenItr == nextRun[0] { // If hit beginning of run, add all in run
                    var combinedTokens = [Token]()
                    for i in tokenItr...nextRun.last!{
                        combinedTokens.append(tokens![i])
                    }
                    let restaurant = tokensToRestaurant(tokens: combinedTokens)
                    let newParentToken = RestaurantToken(children: combinedTokens, restaurant: restaurant!)
                    returnTokens.append(newParentToken!)
                    
                    // Advance token itr, run itr
                    tokenItr = nextRun.last! + 1
                    runItr += 1
                } else { // Otherwise just add
                    returnTokens.append(tokens![tokenItr])
                    tokenItr += 1
                }
            } else { // If we're out of runs, just append
                returnTokens.append(tokens![tokenItr])
                tokenItr += 1
            }
        }
        return returnTokens
    }
    
    func consolidateToppedWithTokens(tokens: [Token]?, indices: [[Int]]) -> [Token]? {
        if tokens == nil {
            return nil
        } else if tokens!.isEmpty {
            return nil
        }
        
        var returnTokens = [Token]()
        
        if indices.count == 0 { // No numbers
            returnTokens = Array(tokens!)
        } else {
            // Order array of indices by reverse start position
            var sortedRuns = indices.sorted(by: { $0[0] < $1[0] })
            var tokenItr = 0
            var runItr = 0
            
            while tokenItr < tokens!.count {
                if runItr < sortedRuns.count { // Runs of #s exist
                    var nextRun = sortedRuns[runItr]
                    if tokenItr == nextRun[0] { // If hit beginning of run, add all in run
                        var combinedTokens = [Token]()
                        for i in tokenItr...nextRun.last!{
                            combinedTokens.append(tokens![i])
                        }
                        let toppedWith = tokensToToppedWith(tokens: combinedTokens)
                        if toppedWith != nil {
                            let newParentToken = WithToken(children: combinedTokens, type: TokenType.toppedWith)
                            returnTokens.append(newParentToken!)
                        } else {
                            returnTokens += combinedTokens
                        }
                        
                        // Advance token itr, run itr
                        tokenItr = nextRun.last! + 1
                        runItr += 1
                    } else { // Otherwise just add
                        returnTokens.append(tokens![tokenItr])
                        tokenItr += 1
                    }
                } else { // If we're out of runs, just append
                    returnTokens.append(tokens![tokenItr])
                    tokenItr += 1
                }
            }
        }
        
        return returnTokens
    }
    
    func consolidateCookedWithTokens(tokens: [Token]?, indices: [[Int]]) -> [Token]? {
        if tokens == nil {
            return nil
        } else if tokens!.isEmpty {
            return nil
        }
        
        var returnTokens = [Token]()
        
        if indices.count == 0 { // No numbers
            returnTokens = Array(tokens!)
        } else {
            // Order array of indices by reverse start position
            var sortedRuns = indices.sorted(by: { $0[0] < $1[0] })
            var tokenItr = 0
            var runItr = 0
            
            while tokenItr < tokens!.count {
                if runItr < sortedRuns.count { // Runs of #s exist
                    var nextRun = sortedRuns[runItr]
                    if tokenItr == nextRun[0] { // If hit beginning of run, add all in run
                        var combinedTokens = [Token]()
                        for i in tokenItr...nextRun.last!{
                            combinedTokens.append(tokens![i])
                        }
                        let cookedWith = tokensToCookedWith(tokens: combinedTokens)
                        if cookedWith != nil {
                            let newParentToken = WithToken(children: combinedTokens, type: TokenType.cookedWith)
                            returnTokens.append(newParentToken!)
                        } else {
                            returnTokens += combinedTokens
                        }
                        
                        // Advance token itr, run itr
                        tokenItr = nextRun.last! + 1
                        runItr += 1
                    } else { // Otherwise just add
                        returnTokens.append(tokens![tokenItr])
                        tokenItr += 1
                    }
                } else { // If we're out of runs, just append
                    returnTokens.append(tokens![tokenItr])
                    tokenItr += 1
                }
            }
        }
        
        return returnTokens
    }
    
    func processQuantityTokens(tokens: [Token]?, startIndex: Int, endIndex: Int) -> [Token]? {
        if tokens == nil {
            return nil
        } else if tokens!.isEmpty {
            return nil
        }
        
        let indicesAndValuesOfQuantityRuns = getIndicesOfTokenRuns(tokens: tokens, tokenType: .quantity, maxRunLength: MAX_QUANTITY)
        
        if indicesAndValuesOfQuantityRuns == nil || indicesAndValuesOfQuantityRuns!.count == 0 {
            return Array(tokens!)
        }
        
        var returnTokens = [Token]()
        let indicesOfQuantityRuns = indicesAndValuesOfQuantityRuns!.map{ $0.0 }
        
        // Order array of indices by reverse start position
        var sortedRuns = indicesOfQuantityRuns.sorted(by: { $0[0] < $1[0] })
        var tokenItr = 0
        var runItr = 0
        
        while tokenItr < tokens!.count {
            if runItr < sortedRuns.count { // Runs of #s exist
                var nextRun = sortedRuns[runItr]
                if tokenItr == nextRun[0] { // If hit beginning of run, add all in run
                    var combinedTokens = [Token]()
                    for i in tokenItr...nextRun.last!{
                        combinedTokens.append(tokens![i])
                    }
                    let quantity = tokensToQuantity(tokens: combinedTokens)
                    if let quantity = quantity {
                        let newParentToken = QuantityToken(children: combinedTokens, quantity: quantity)
                        returnTokens.append(newParentToken!)
                    } else {
                        returnTokens += combinedTokens
                    }
                    
                    // Advance token itr, run itr
                    tokenItr = nextRun.last! + 1
                    runItr += 1
                } else { // Otherwise just add
                    returnTokens.append(tokens![tokenItr])
                    tokenItr += 1
                }
            } else { // If we're out of runs, just append
                returnTokens.append(tokens![tokenItr])
                tokenItr += 1
            }
        }
        
        return returnTokens
    }
    
    func processMethodTokens(tokens: [Token]?, startIndex: Int, endIndex: Int) -> [Token]? {
        if tokens == nil {
            return nil
        } else if tokens!.isEmpty {
            return nil
        }
        
        let indicesAndValuesOfRuns = getIndicesOfTokenRuns(tokens: tokens, tokenType: .cookingMethod, maxRunLength: MAX_METHOD)
        
        if indicesAndValuesOfRuns == nil || indicesAndValuesOfRuns!.count == 0 {
            return Array(tokens!)
        }
        
        var returnTokens = [Token]()
        let indicesOfRuns = indicesAndValuesOfRuns!.map{ $0.0 }
        
        // Order array of indices by reverse start position
        var sortedRuns = indicesOfRuns.sorted(by: { $0[0] < $1[0] })
        var tokenItr = 0
        var runItr = 0
        
        while tokenItr < tokens!.count {
            if runItr < sortedRuns.count { // Runs of #s exist
                var nextRun = sortedRuns[runItr]
                if tokenItr == nextRun[0] { // If hit beginning of run, add all in run
                    var combinedTokens = [Token]()
                    for i in tokenItr...nextRun.last!{
                        combinedTokens.append(tokens![i])
                    }
                    let method = tokensToCookingMethod(tokens: combinedTokens)
                    if let method = method {
                        let newParentToken = CookingMethodToken(children: combinedTokens, cookingMethod: method)
                        returnTokens.append(newParentToken!)
                    } else {
                        returnTokens += combinedTokens
                    }
                    
                    // Advance token itr, run itr
                    tokenItr = nextRun.last! + 1
                    runItr += 1
                } else { // Otherwise just add
                    returnTokens.append(tokens![tokenItr])
                    tokenItr += 1
                }
            } else { // If we're out of runs, just append
                returnTokens.append(tokens![tokenItr])
                tokenItr += 1
            }
        }
        
        return returnTokens
    }

    //MARK: Generic Tokens to Specific Tokens
    func tokensToWith(tokens: [Token]?) -> String? {
        if tokens == nil {
            return nil
        } else if tokens!.isEmpty {
            return nil
        }
        
        if tokens!.count == 1 {
            if isWith(string: tokens![0].string) && !(tokens![0] is WithToken) { // hacky
                return tokens![0].string
            }
        }
        return nil
    }
    
    func tokensToToppedWith(tokens: [Token]?) -> String? {
        if tokens == nil {
            return nil
        } else if tokens!.isEmpty {
            return nil
        }
        
        if tokens!.count == 2 {
            let joinedString = tokensToLowercase(tokens: tokens!)
            if joinedString == "topped with" {
                return joinedString
            }
        } else if tokens!.count == 1 {
            if isToppedWith(string: tokens![0].string) && !(tokens![0] is WithToken) { // hacky
                return tokens![0].string
            }
        }
        return nil
    }
    
    func tokensToLowercase(tokens: [Token]) -> String {
        return tokens.map{ $0.string }.joined(separator: " ").lowercased()
    }
    
    func tokensToCookedWith(tokens: [Token]?) -> String? {
        if tokens == nil {
            return nil
        } else if tokens!.isEmpty {
            return nil
        }
        
        if tokens!.count == 2 {
            let joinedString = tokens!.map{ $0.string }.joined(separator: " ")
            if joinedString == "cooked with" || joinedString == "tossed with" {
                return joinedString
            }
        } else if tokens!.count == 1 {
            if isCookedWith(string: tokens![0].string) && !(tokens![0] is WithToken) { // hacky
                return tokens![0].string
            }
        }
        return nil
    }
    
    func tokensToRestaurant(tokens: [Token]?) -> String? {
        if tokens == nil {
            return nil
        } else if tokens!.isEmpty {
            return nil
        }
        
        if tokens!.count == 1 {
            if isRestaurant(string: tokens![0].string) && !(tokens![0] is RestaurantToken) { // hacky
                return tokens![0].string
            }
        }
        return nil
    }
    
    func tokensToQuantity(tokens: [Token]?) -> Quantity? {
        if tokens == nil {
            return nil
        } else if tokens!.isEmpty {
            return nil
        }
        
        if tokens!.count == 3 {
            if tokens![0].string.lowercased() == "a" && isQuantity(string: tokens![1].string) && tokens![2].string.lowercased()  == "of" {
                return Quantity(number: 1.0, measurementType: tokens![1].string)
            } else if tokens![0] is NumberToken && isQuantity(string: tokens![1].string) && tokens![2].string.lowercased()  == "of" {
                let numberToken = tokens![0] as! NumberToken
                return Quantity(number: numberToken.number, measurementType: tokens![1].string)
            }
        } else if tokens!.count == 2 {
            if tokens![0] is NumberToken {
                let numberToken = tokens![0] as! NumberToken
                if isQuantity(string: tokens![1].string) {
                    return Quantity(number: numberToken.number, measurementType: tokens![1].string)
                }
            } else if isQuantity(string: tokens![0].string) && tokens![1].string.lowercased()  == "of" {
                return Quantity(number: nil, measurementType: tokens![0].string)
            }
        } else if tokens!.count == 1 {
            if isQuantity(string: tokens![0].string) && !(tokens![0] is QuantityToken) { // hacky
                return Quantity(number: nil, measurementType: tokens![0].string)
            } else if tokens![0] is NumberToken {
                let numberToken = tokens![0] as! NumberToken
                return Quantity(number: numberToken.number, measurementType: "")
            }
        }
        return nil
    }
    
    func tokensToCookingMethod(tokens: [Token]?) -> CookingMethod? {
        if tokens == nil {
            return nil
        } else if tokens!.isEmpty {
            return nil
        }
        
        if tokens!.count == 2 {
            if isMethod(string: tokens![0].string) && !(tokens![0] is CookingMethodToken) && isMethod(string: tokens![1].string) && !(tokens![1] is CookingMethodToken) { // hacky
                return CookingMethod(method: tokens![0].string + " " + tokens![1].string)
            }
        } else if tokens!.count == 1 {
            if isMethod(string: tokens![0].string) && !(tokens![0] is CookingMethodToken) { // hacky
                return CookingMethod(method: tokens![0].string)
            }
        }
        return nil
    }
    
    //MARK: Vocab Check
    func isWith(string: String) -> Bool {
        if string.lowercased().trimmingCharacters(in: .whitespacesAndNewlines) == "with" {
            return true
        }
        return false
    }
    
    func isAnd(string: String) -> Bool {
        if string.lowercased().trimmingCharacters(in: .whitespacesAndNewlines) == "and" {
            return true
        }
        return false
    }
    
    func isCookedWith(string: String) -> Bool {
        if string.lowercased().trimmingCharacters(in: .whitespacesAndNewlines) == "cookedwith" {
            return true
        }
        return false
    }
    
    func isToppedWith(string: String) -> Bool {
        var lowercased = string.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        if lowercased == "toppedwith" || lowercased == "with" {
            return true
        }
        return false
    }
    
    func isQuantity(string: String) -> Bool {
        var lowercased = string.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        if lowercased == "quantity" || lowercased == "bowl" || lowercased == "handful" || lowercased == "teaspoon" || lowercased == "cup" {
            return true
        }
        return false
    }
    
    func isMethod(string: String) -> Bool {
        let lowercased = string.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        if lowercased == "method" || lowercased == "roasted" || lowercased == "mashed" || lowercased == "dried" {
            return true
        }
        return false
    }
    
    func isRestaurant(string: String) -> Bool {
        if string.lowercased().trimmingCharacters(in: .whitespacesAndNewlines) == "restaurant" {
            return true
        }
        return false
    }

    //MARK: - Process Numeric Tokens
    func processNumericTokens(speechTokens: [Token]?) -> [Token]? {
        if speechTokens == nil {
            return nil
        }
        var returnTokens = [Token]()
        
        // Preprocess by flagging spelled-out numbers,
        // which cuts down the number of number checks made later
        flagSpelledOutNumberTokens(tokens: speechTokens!)
        
        let indicesAndValuesOfAllRuns = getIndicesOfTokenRuns(tokens: speechTokens!, tokenType: .number, maxRunLength: 5)
        
        if indicesAndValuesOfAllRuns == nil || indicesAndValuesOfAllRuns!.count == 0 {
            return Array(speechTokens!)
        }
        
        // Order array of indices by reverse start position
        var sortedRuns = indicesAndValuesOfAllRuns!.sorted(by: { $0.0[0] < $1.0[0] })
        var tokenItr = 0
        var runItr = 0
        
        while tokenItr < speechTokens!.count {
            if runItr < sortedRuns.count { // Runs of #s exist
                var nextRun = sortedRuns[runItr].0
                if tokenItr == nextRun[0] { // If hit beginning of run, add all in run
                    var childrenTokens = [Token]()
                    for i in tokenItr...nextRun.last!{
                        childrenTokens.append(speechTokens![i])
                    }
                    if let number = sortedRuns[runItr].1 {
                        let newParentToken = NumberToken(children: childrenTokens, number: number)
                        returnTokens.append(newParentToken!)
                    }
                    
                    // Advance token itr, run itr
                    tokenItr = nextRun.last! + 1
                    runItr += 1
                } else { // Otherwise just add
                    returnTokens.append(speechTokens![tokenItr])
                    tokenItr += 1
                }
            } else { // If we're out of runs, just append
                returnTokens.append(speechTokens![tokenItr])
                tokenItr += 1
            }
        }
        
        return returnTokens
    }
    
    func flagSpelledOutNumberTokens(tokens: [Token]){
        let numberFormatter: NumberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.spellOut
        for token in tokens {
            let number = numberFormatter.number(from: token.string.lowercased())
            if number != nil {
                token.flags.append(TokenFlag.isTextNumber)
            }
        }
    }
    func tokensContainTextNumberFlag(tokens: [Token]) -> Bool{
        var flags = tokens.map{ $0.flags }.flatMap { $0 }
        return flags.contains(TokenFlag.isTextNumber)
    }
    
    func tokensToNumber(tokens: [Token]?) -> Float? {
        if tokens == nil {
            return nil
        } else if tokens!.isEmpty {
            return nil
        }
        
        let lowercased = tokensToLowercase(tokens: tokens!)
        let wordCount = tokens!.count
        var returnFloat: Float?
        returnFloat = nil
        
        // Spell out
        // Any number of words (cut if off at, say, 4)
        if wordCount <= 4 && tokensContainTextNumberFlag(tokens: tokens!) {
            returnFloat = spelledOutStringToNumber(string: lowercased)
        }
        
        // Decimal
        // 1 word
        if returnFloat == nil && wordCount == 1 {
            returnFloat = decimalStringToNumber(string: lowercased)
        }
        
        // Numeric fractions
        // 2 or fewer words
        if returnFloat == nil && wordCount <= 2 {
            returnFloat = numericFractionStringToNumber(string: lowercased)
        }
        
        // Half, quarter, etc.
        // 1 word
        if returnFloat == nil && wordCount == 1 {
            returnFloat = specialCaseFractionalStringToNumber(string: lowercased)
        }
        
        // Contextualized Fraction
        // 2 words
        if returnFloat == nil && wordCount == 2 {
            returnFloat = contextualizedFractionTokensToNumber(tokens: tokens!)
        }
        
        // Fraction with Divisor
        // 2 words
        if returnFloat == nil && wordCount == 2 {
            returnFloat = fractionWithDivisorTokensToNumber(tokens: tokens!)
        }
        
        // Composite Text Fraction
        // 3 words
        if returnFloat == nil && (wordCount == 3 || wordCount == 4) {
            returnFloat = compositeFractionTokensToNumber(tokens: tokens!)
        }
        
        return returnFloat
    }
    
    func contextualizedFractionTokensToNumber (tokens: [Token]) -> Float? {
        //var splitString = string.split(separator: " ")
        if tokens.count == 2 { //splitString.count == 2 {
            if tokens[0].getLowercase() == "a" || tokens[0].getLowercase() == "an" {
                let float = oneWordTextFractionTokenToNumber(tokens: [tokens[1]])
//                if stringIsOneWordTextFraction(string: tokens[1].getLowercase()) {
//                    let float = tokensToNumber(tokens: [tokens[1]])
                    if float != nil {
                        if float! < Float(1) {
                            return float
                        }
                    }
//                }
            }
        }
        return nil
    }
    
    func fractionWithDivisorTokensToNumber (tokens: [Token]) -> Float? {
        //var splitString = string.split(separator: " ")
        if tokens.count == 2 {
            let multiplier = tokensToNumber(tokens: [tokens[0]])
            let divisorString = tokens[1].getLowercase() //String(splitString[1])

            if let multiplier = multiplier {
                let fraction = oneWordTextFractionTokenToNumber(tokens: [tokens[1]])
                if fraction != nil {
                    return multiplier * fraction!
                }
//                if stringIsOneWordTextFraction(string: divisorString) {
//                    let fraction = tokensToNumber(tokens: [tokens[1]]) //stringToNumber(string: divisorString)
//                    if fraction != nil { //}|| fractionSingular != nil) {
//                        return multiplier * fraction!
//                    }
//                }
                else {
                    var singularAlternative: String?
                    let indexLastChar = divisorString.index(divisorString.startIndex, offsetBy: divisorString.count - 1)
                    if divisorString[indexLastChar] == "s" {
                        let substring = divisorString[..<indexLastChar]
                        singularAlternative = String(substring)
                    }
                    let fractionSingular = oneWordTextFractionStringToNumber(string: singularAlternative)
                    if fractionSingular != nil {
                        return multiplier * fractionSingular!
                    }
//                    if stringIsOneWordTextFraction(string: singularAlternative) {
//                        let fractionSingular = stringToNumber(string: singularAlternative)
//                        if fractionSingular != nil {
//                            return multiplier * fractionSingular!
//                        }
//                    }
                }
            }
        }
        return nil
    }
    
    func compositeFractionTokensToNumber(tokens: [Token]) -> Float? {
        if tokens[1].getLowercase() == "and" && (tokens.count == 3 || tokens.count == 4)  {
            let wholeNumber = tokensToNumber(tokens: [tokens[0]])
            let fraction = tokensToNumber(tokens: Array(tokens[2...]))
            if wholeNumber != nil && fraction != nil {
                if wholeNumber! - floor(wholeNumber!) == 0 && fraction! > Float(0) && fraction! < Float(1) {
                    return wholeNumber! + fraction!
                }
            }
        }
        return nil
    }
    
    func oneWordTextFractionTokenToNumber (tokens: [Token]) -> Float? {
        if tokens.count == 1 {
            let specialCase = specialCaseFractionalStringToNumber(string: tokens[0].getLowercase())
            
            if specialCase != nil {
                return specialCase!
            } else if doesStringHaveFractionalQualities(string: tokens[0].getLowercase()) {
                let spelledOut = spelledOutStringToNumber(string: tokens[0].getLowercase())
                if spelledOut != nil {
                    if spelledOut! > Float(0) && spelledOut! < Float(1) {
                        return spelledOut!
                    }
                }
            }
        }
        
        return nil
    }
    
    func stringToNumber(string: String?) -> Float? {
        if string == nil || string?.count == 0 {
            return nil
        }
        
        let tokens = tokenizeString(string: string!)
        flagSpelledOutNumberTokens(tokens: tokens)
        return tokensToNumber(tokens: tokens)
    }
    
    // Input: "Twenty-three"  Return: 23
    func spelledOutStringToNumber (string: String) -> Float? {
        let numberFormatter: NumberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.spellOut
        let number = numberFormatter.number(from: string)
        
        // Check to see if ordinal, if so then invert
        if number != nil {
            if doesStringHaveFractionalQualities(string: string) {
                let divisor = Float(truncating: number!)
                if divisor != 0 {
                    return 1.0 / divisor
                } else {
                    return nil
                }
            }
            
            return Float(truncating: number!)
        }
        return nil
    }
    
    func doesStringHaveFractionalQualities(string: String) -> Bool {
        if string == "third" || specialCaseFractionalStringToNumber(string: string) != nil {
            return true
        }
        if string.range(of: "th") != nil {
            if string.distance(from: string.startIndex, to: string.range(of: "th")!.lowerBound) == string.count - 2 {
                return true
            }
        }
        return false
    }
    
    func decimalStringToNumber (string: String) -> Float? {
        let numberFormatter: NumberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        let number = numberFormatter.number(from: string)
        if (number != nil){
            return Float(truncating: number!)
        }
        return nil
    }
    
    func specialCaseFractionalStringToNumber (string: String) -> Float? {
        switch(string.lowercased()){
        case "half":
            return 0.5
        case "halve":
            return 0.5
        case "quarter":
            return 0.25
        default:
            return nil
        }
    }
    
    func numericFractionStringToNumber (string: String) -> Float? {
        do {
            let regex = try NSRegularExpression(pattern: "^([1-9][0-9]* )?[1-9][0-9]*\\/[1-9][0-9]*$")
            let results = regex.matches(in: string,
                                        range: NSRange(string.startIndex..., in: string))
            if results.count == 1 {
                var splitFraction = string.split(separator: "/")
                if splitFraction.count == 2 {
                    var wholeNumber = 0
                    var divisor = 0
                    let dividend = Int(splitFraction[1])
                    
                    var splitDivisor = splitFraction[0].split(separator: " ")
                    if splitDivisor.count == 1 {
                        divisor = Int(splitFraction[0])!
                    } else {
                        wholeNumber = Int(splitDivisor[0])!
                        divisor = Int(splitDivisor[1])!
                    }
                    if dividend != nil && dividend != 0 {
                        return Float(wholeNumber) + (Float(divisor) / Float(dividend!))
                    }
                }
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return nil
        }
        return nil
    }
    
    func oneWordTextFractionStringToNumber (string: String?) -> Float? {
        if string == nil || string?.count == 0 { return nil }
        let specialCase = specialCaseFractionalStringToNumber(string: string!)
        
        if specialCase != nil {
            return specialCase!
        } else if doesStringHaveFractionalQualities(string: string!) {
            let spelledOut = spelledOutStringToNumber(string: string!)
            if spelledOut != nil {
                if spelledOut! > Float(0) && spelledOut! < Float(1) {
                    return spelledOut!
                }
            }
        }
        
        return nil
    }
}
