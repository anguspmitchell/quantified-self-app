//
//  MealItemTableViewCell.swift
//  Siri
//
//  Created by Angus Mitchell on 11/6/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class MealItemTableViewCell: UITableViewCell {
//    @IBOutlet weak var mealItemDetailStackView: UIStackView!
    
    @IBOutlet weak var cookedWithLabel: UILabel!
    @IBOutlet weak var methodLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var mealItemLabel: UILabel!
//    @IBOutlet weak var mealItemDetailLabel: UILabel!
    @IBOutlet weak var mealItemImage: UIImageView!
    var defaultMargin: CGFloat = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.defaultMargin = self.layoutMargins.left
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
