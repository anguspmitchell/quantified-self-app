//
//  TestViewController2.swift
//  Siri
//
//  Created by Angus Mitchell on 11/13/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit
import Speech
import Alamofire

class AddMealViewController: UIViewController, UITextViewDelegate, UITableViewDataSource, SFSpeechRecognizerDelegate {
    
    // MARK: - Variables
    // MARK: Model
    var foods = [Food]()
    var mealNutritionalInfo = NutritionInfo()
    
    // MARK: Speech Variables
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    var isMicrophoneEnabled = false

    // MARK: Outlets
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var mikeButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textViewContainer: UIView!
    
    @IBOutlet weak var caloriesValueLabel: UILabel!
    @IBOutlet weak var fatValueLabel: UILabel!
    @IBOutlet weak var proteinValueLabel: UILabel!
    @IBOutlet weak var carbsValueLabel: UILabel!
    @IBOutlet weak var sugarValueLabel: UILabel!
    
    
    //    @IBOutlet weak var scrollView: UIScrollView!
//    var fakeData = [Int]()
    
//    var isTypingActive = false
    weak var timer: Timer?

//    @IBOutlet weak var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    
    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.tableView.rowHeight = UITableViewAutomaticDimension
        //tableView.estimatedRowHeight = 352

        // Do any additional setup after loading the view.
//        for i in 1...6 {
//            fakeData.append(i)
//        }
        tableView.dataSource = self
        
        textView.delegate = self
        registerForKeyboardNotifications()
        
        //textView.textContainer.heightTracksTextView = true
        //textView.isScrollEnabled = false
        textView.layer.cornerRadius = 16.5
        textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0).cgColor
        //print("Default:")
        //print(textView.textContainerInset)
        textView.textContainerInset.left = 10.0// = UIEdgeInsetsMake(5, 10, 5, 10);
        textView.textContainerInset.right = 10.0
        
        
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            switch authStatus {
            case .authorized:
                self.isMicrophoneEnabled = true
                
            case .denied:
                self.isMicrophoneEnabled = false
                print("User denied access to speech recognition")
                
            case .restricted:
                self.isMicrophoneEnabled = false
                print("Speech recognition restricted on this device")
                
            case .notDetermined:
                self.isMicrophoneEnabled = false
                print("Speech recognition not yet authorized")
            }
        }
        
        print(self.isMicrophoneEnabled)
        if self.isMicrophoneEnabled {
            initiateAudioSesssion()
            spoofBufferRecognitionRequest()
            stopRecording()
        }
        
        self.getSnippets()
        self.postSnippets()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Speech Recognition
    func stopRecording() {
        // cell.backgroundColor = UIColor.clear
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
        }
    }
    
    func spoofBufferRecognitionRequest(){
        print("3 Buffer Recognition request")
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()  //3
        let inputNode = audioEngine.inputNode
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        } //5
        
        recognitionRequest.shouldReportPartialResults = true  //6
    }
    
    func initiateAudioSesssion() {
        // cell.backgroundColor = UIColor.green
        
        if recognitionTask != nil {  //1
            print("1 Recognition Task")
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()  //2
        do {
            print("2 Audio Session")
            try audioSession.setCategory(AVAudioSessionCategoryRecord)
            print("2a")
            try audioSession.setMode(AVAudioSessionModeMeasurement)
            print("2b")
            try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
    }
    
    func startRecording() {
        initiateAudioSesssion()
        
        print("3 Buffer Recognition request")
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()  //3
        let inputNode = audioEngine.inputNode
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        } //5
        
        recognitionRequest.shouldReportPartialResults = true  //6
        
        var resultString = String()
        textView.text = ""
        self.textView.typingAttributes = [NSAttributedStringKey.font.rawValue: UIFont.systemFont(ofSize: 14.0)]
//        var resultStringAttributed = NSMutableAttributedString()
//        var parsedFoods: [Food]?
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in  //7
            
            var isFinal = false  //8
            
            if result != nil {
                resultString = result!.bestTranscription.formattedString
                
//                let speechParser = SpeechParser()
//                let foods_tokens = speechParser.parseSpeechText(speechString: resultString, debug: false)
                
//                parsedFoods = foods_tokens.0
                
//                resultStringAttributed = self.tokensToAttributedString(tokens: foods_tokens.1)
                
                //cell.addItemTextField.attributedText = resultStringAttributed
                //self.textView.attributedText = resultStringAttributed
                self.textView.text = resultString
                // Resize text view
                //self.textViewDidChange(self.textView)
                self.updateTextViewSize()
                self.updateAddButtonStatus()
                
                isFinal = (result?.isFinal)!
            }
            
            if error != nil || isFinal {  //10
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.isMicrophoneEnabled = true
                
                self.formatParsedText()
//                // Empty out the writing cell
//                // cell.addItemTextField.text = nil
//                self.textView.attributedText = nil
//                //self.textViewDidChange(self.textView)
//                self.updateTextViewSize()
//
//                if let parsedFoods = parsedFoods, parsedFoods.count > 0 {
//                    var transcriptions = [(String, Float)]()
//                    for transcription in (result?.transcriptions)! {
//                        var totalConfidence: Float = 1.0
//                        for transcriptionSegment in transcription.segments {
//                            totalConfidence *= transcriptionSegment.confidence
//                        }
//                        transcriptions.append((transcription.formattedString, totalConfidence))
//                    }
//
//                    for parsedFood in parsedFoods {
//                        parsedFood.transcriptions = transcriptions
//
//                    }
//                    self.foods += parsedFoods
//                    self.tableView.reloadData()
//                }
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)  //11
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()  //12
        
        do {
            print("4 Audio enginer start")
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
    }
    
    func tokensToAttributedString(tokens: [Token]?) -> (NSMutableAttributedString, Bool) {
        var isFormatted = false
        let font = UIFont.systemFont(ofSize: 14.0)
        let attributedString = NSMutableAttributedString()
        attributedString.addAttribute(NSAttributedStringKey.font, value: font, range: NSMakeRange(0, attributedString.length))
        
        if let tokens = tokens {
            //let boldFont = UIFont(name: "Avenir-Heavy", size: 17.0)!
            
            for (i, token) in tokens.enumerated() {
                let tokenString = token.string

                let tokenAttrString = NSMutableAttributedString(string: tokenString)
                let range = NSMakeRange(0, tokenString.count)
                tokenAttrString.addAttribute(NSAttributedStringKey.font, value: font, range: range)
                
                if token is QuantityToken || token is WithToken || token is CookingMethodToken {
                    tokenAttrString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
                    isFormatted = true
                }
//                else if token is WithToken {
//                    //tokenAttrString.addAttribute(NSAttributedStringKey.font, value: boldFont, range: range)
//                    tokenAttrString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
//                    isFormatted = true
//                }
                
                if i > 0 {
                    if token.flags.contains(TokenFlag.isNewLine){
                        let newLineString = NSMutableAttributedString(string: "\n")
                        newLineString.addAttribute(NSAttributedStringKey.font, value: font, range: NSMakeRange(0, newLineString.length))
                        attributedString.append(newLineString)
                    } else {
                        let spaceString = NSMutableAttributedString(string: " ")
                        spaceString.addAttribute(NSAttributedStringKey.font, value: font, range: NSMakeRange(0, spaceString.length))
                        attributedString.append(spaceString)
                    }
                }
                attributedString.append(tokenAttrString)
            }
            
//            let emptyString = NSMutableAttributedString(string: "")
//            emptyString.addAttribute(NSAttributedStringKey.font, value: font, range: NSMakeRange(0, emptyString.length))
//            attributedString.append(emptyString)
        }
        return (attributedString, isFormatted)
    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            isMicrophoneEnabled = true
        } else {
            isMicrophoneEnabled = false
        }
    }
    
    // MARK: - Keyboard Methods
    
    func registerForKeyboardNotifications(){
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(_:)), name: .UIKeyboardDidShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(_:)), name: .UIKeyboardWillHide, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)

    }
    
//    @objc func keyboardWasShown(_ notification: NSNotification) {
//        guard let info = notification.userInfo, let keyboardFrameValue = info[UIKeyboardFrameBeginUserInfoKey] as? NSValue else { return }
//
//        let keyboardFrame = keyboardFrameValue.cgRectValue
//        let keyboardSize = keyboardFrame.size
//
//        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
//        scrollView.contentInset = contentInsets
//        scrollView.scrollIndicatorInsets = contentInsets
//
//        // If active text field is hidden by keyboard, scroll it so it's visible
//        // Your app might not need or want this behavior.
//        var aRect: CGRect = self.view.frame
//        aRect.size.height -= keyboardSize.height
//        if (!aRect.contains(textViewContainer.frame.origin) ) {
//            self.scrollView.scrollRectToVisible(textViewContainer.frame, animated: true)
//        }
//    }
//
//    @objc func keyboardWillBeHidden(_ notification: NSNotification) {
//        let contentInsets = UIEdgeInsets.zero
//        scrollView.contentInset = contentInsets
//        scrollView.scrollIndicatorInsets = contentInsets
//    }
    
    @objc func keyboardNotification(_ notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            var duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue //.CurveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            let tabBarHeight: CGFloat = tabBarController?.tabBar.frame.size.height ?? 0.0
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = (endFrame?.size.height)! - tabBarHeight ?? 0.0
            }
            print("animate")
            UIView.animate(withDuration: duration,
                                       delay: TimeInterval(0),
                                       options: [],
                                       animations: { self.view.layoutIfNeeded() },
                                       completion: nil)
        }
    }
    
    // MARK: - Text Field Delegate
//    var trigger = false
    func textViewDidChange(_ textView: UITextView){
        // Add Button
        updateAddButtonStatus()
        
//        if trigger {
//            print(textView.attributedText)
//        }
//        print(textView.font?.pointSize)
        
        // Parse text
//        var lastLetter = textView.attributedText.attributedSubstring(from: NSMakeRange(0, (textView.attributedText.length - 1)))
//        let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
//        DispatchQueue.main.asyncAfter(deadline: when) {
//            print("Delay!")
//        }
        
        if textView.text.last == "\n" {
            restartTimer(delay: 5.0)
        } else {
            restartTimer(delay: 1.2)
        }
//        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.triggerTypedTextParse), userInfo: nil, repeats: false)
        
        // TextView Size
        updateTextViewSize()
        
        self.textView.typingAttributes = [NSAttributedStringKey.font.rawValue: UIFont.systemFont(ofSize: 14.0)]
    }
    
    func restartTimer(delay: Double) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: delay, target: self, selector: #selector(self.triggerTypedTextParse), userInfo: nil, repeats: false)
    }
    
    func stopTimer() {
        timer?.invalidate()
    }
    
    @objc func triggerTypedTextParse()
    {
        //print("hello World")
        
        formatParsedText()
//        print(typedTextAttributed)
//        trigger = true
        //self.textView.typingAttributes = [NSAttributedStringKey.font.rawValue: UIFont.systemFont(ofSize: 14.0)]
        stopTimer()
    }
    
    func formatParsedText() {
        let text = textView.attributedText.string
        let speechParser = SpeechParser()
        let foods_tokens = speechParser.parseSpeechText(speechString: text, debug: false)
        let typedTextAttributed_isFormatted = self.tokensToAttributedString(tokens: foods_tokens.1)
        let typedTextAttributed = typedTextAttributed_isFormatted.0
        self.textView.attributedText = typedTextAttributed
        
//        let isFormatted = typedTextAttributed_isFormatted.1
        
//        if isFormatted {
        UIView.transition(
            with: self.textView,
            duration: 0.15,
            options: [.curveEaseInOut, .transitionCrossDissolve],
            animations: {
            self.textView.textColor = .gray
            },
            completion: {
                finished in
                if finished {
                    UIView.transition(
                        with: self.textView,
                        duration: 0.15,
                        options: [.curveEaseInOut, .transitionCrossDissolve],
                        animations: {
                            self.textView.textColor = .black
                            },
                        completion: nil)
                }
        })
//        }
//        UIView.transition(
//            with: self.textView,
//            duration: 0.1,
//            options: [.curveEaseInOut, .transitionCrossDissolve],
//            animations: {
//                self.textView.textColor = .black
//        },
//            completion: nil)
    }
    
    func updateTextViewSize(){
        let fixedWidth = textView.frame.size.width
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
//        print(newSize.height)
        if newSize.height > 100.0 {
            textViewHeightConstraint.constant = min(newSize.height, 100.0)
            textView.isScrollEnabled = true
        } else {
            textViewHeightConstraint.constant = min(newSize.height, 100.0)
            textView.isScrollEnabled = false
        }
    }
    
    func updateAddButtonStatus(){
        if textView.attributedText.length > 0 {
            addButton.isEnabled = true
        } else {
            addButton.isEnabled = false
        }
    }
//    // MARK: - Table View
//
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return fakeData.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifier")! //1.
//
//        let text = fakeData[indexPath.row] //2.
//
//        cell.textLabel?.text = String(text) //3.
//
//        return cell //4.    }
//    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.foods.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.foods[section].countChildrenInclusive()
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        
        let cellIdentifier = "MealItemTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? MealItemTableViewCell else {
            fatalError("Dequeued cell is not an instance of MealItemTableViewCell")
        }
        
        // Fetches the appropriate meal for the data source layout.
        let parentFood = foods[indexPath.section] //foods[indexPath.row]
        let tuple = parentFood.getNthChild(n: indexPath.row)
        let food = tuple.0
        let depth = tuple.1
//        print("Depth: " + String(depth))
        if let food = food {
            cell.mealItemLabel.text = food.name.capitalizeFirstLetter() //capitalizeFirstLetter(string: food.name)
            
//            let quantityLabel = UILabel()
//            quantityLabel.font = quantityLabel.font.withSize(10)
            
            if let quantity = food.quantity {
                var quantityText = ""
//                quantityText += "Quantity:"
                print(quantity.number)
                print(quantity.divisor)
                print(quantity.measurementType)
                cell.quantityLabel.textColor = .black
                
                if var number = quantity.number {
                    print("Number v")
                    print(number)
                    if let divisor = quantity.divisor {
                        number = number / Float(divisor)
                    }
                    var numberString = String(number)
                    if round(number) == number {
                        numberString = String(format: "%.0f", number)
                    }
                    quantityText += numberString
                } else if let divisor = quantity.divisor {
                    let assumedQuantity = Float(1) / Float(divisor)
                    var assumedQuantityString = String(assumedQuantity)
                    if round(assumedQuantity) == assumedQuantity {
                        assumedQuantityString = String(format: "%.0f", assumedQuantity)
                    }
                    quantityText += assumedQuantityString
                }
                if let measurementType = quantity.measurementType {
                    quantityText += " " + measurementType //.abbv
                }
                cell.quantityLabel.text = quantityText
            } else if depth > 0 {
                cell.quantityLabel.textColor = .red
                cell.quantityLabel.text = "1 tbsp"
            }
            
            
//            let cookedWithLabel = UILabel()
//            cookedWithLabel.font = cookedWithLabel.font.withSize(10)
            
            if let cookedWith = food.cookedWith, cookedWith.count > 0 {
//                if food.quantity != nil {
//                    cookedWithText += "      "
//                }
//                quantityText += "Cooked With:"
                var cookedWithText = ""
                for (i, foodCookedWith) in cookedWith.enumerated() {
                    if i > 0 {
                        cookedWithText += ","
                    }
                    cookedWithText += " " + foodCookedWith.name.capitalizeFirstLetter() // capitalizeFirstLetter(string: foodCookedWith.name)
                }
                cell.cookedWithLabel.text = cookedWithText
                cell.cookedWithLabel.textColor = .black
            }
            
            if let methods = food.cookingMethods, methods.count > 0 {
                var methodText = ""
                for (i, method) in methods.enumerated() {
                    if i > 0 {
                        methodText += ","
                    }
                    methodText += " " + method.capitalizeFirstLetter() // capitalizeFirstLetter(string: foodCookedWith.name)
                }
                cell.methodLabel.text = methodText
                cell.methodLabel.textColor = .black
            }
            
//            cell.mealItemDetailLabel.text = quantityText
            
//            cell.mealItemDetailStackView.addArrangedSubview(quantityLabel)
//            cell.mealItemDetailStackView.addArrangedSubview(cookedWithLabel)
            
            if let image = food.image {
                cell.mealItemImage.image = image
            }
            cell.indentationWidth = 20
            cell.indentationLevel = depth
            cell.contentView.layoutMargins.left = cell.defaultMargin + CGFloat(cell.indentationLevel) * cell.indentationWidth
            cell.contentView.layoutIfNeeded()
        }
        
        return cell
    }
    
//    func capitalizeFirstLetter(string: String) -> String {
//        let first = string.prefix(1).capitalized
//        let other = string.dropFirst()
//        return first + other
//    }
    

    // MARK: - Buttons
    @IBAction func addButtonClick(_ sender: Any) {
        print("Add food!")
        let typedText = textView.attributedText.string
        let speechParser = SpeechParser()
        let foods_tokens = speechParser.parseSpeechText(speechString: typedText, debug: false)
        let parsedFoods = foods_tokens.0
        //let typedTextAttributed = self.tokensToAttributedString(tokens: foods_tokens.1)
        //self.textView.attributedText = typedTextAttributed
        
        self.textView.attributedText = nil
        //self.textViewDidChange(self.textView)
        self.updateTextViewSize()
        
        if let parsedFoods = parsedFoods, parsedFoods.count > 0 {
            var transcriptions = [(String, Float)]()
//            for transcription in (result?.transcriptions)! {
//                var totalConfidence: Float = 1.0
//                for transcriptionSegment in transcription.segments {
//                    totalConfidence *= transcriptionSegment.confidence
//                }
//                transcriptions.append((transcription.formattedString, totalConfidence))
//            }
            
            for parsedFood in parsedFoods {
                //parsedFood.transcriptions = transcriptions
                
            }
            self.foods += parsedFoods
            updateMealNutritionalInfo()
            self.tableView.reloadData()
        }
        
        textView.resignFirstResponder()
        addButton.isEnabled = false
    }
    
    func updateMealNutritionalInfo(){
        let newNutritionalInfo = NutritionInfo()
        for food in foods {
            newNutritionalInfo.add(term: food.calcCumulativeNutritionalInfo())
        }
        self.mealNutritionalInfo = newNutritionalInfo
        
        caloriesValueLabel.text = formatNutritionalInfo(value: self.mealNutritionalInfo.calories, suffix: "")
        proteinValueLabel.text = formatNutritionalInfo(value: self.mealNutritionalInfo.protein, suffix: "g")
        fatValueLabel.text = formatNutritionalInfo(value: self.mealNutritionalInfo.fat, suffix: "g")
        carbsValueLabel.text = formatNutritionalInfo(value: self.mealNutritionalInfo.carbohydrates, suffix: "g")
        sugarValueLabel.text = formatNutritionalInfo(value: self.mealNutritionalInfo.sugar, suffix: "g")
    }
    
    func formatNutritionalInfo(value: Float?, suffix: String) -> String {
        if let value = value {
            return String(format: "%.0f", value) + suffix
        }
        return "N/A"
    }
    
    @IBAction func mikeButtonTouchDown(_ sender: Any) {
        print("Btn Down")
        textView.isEditable = false
//        textViewContainer.backgroundColor = UIColor.green
//        textViewContainer.setNeedsDisplay()
        UIView.animate(
            withDuration: 0.75,
            delay: 0,
            usingSpringWithDamping: 0.5,
            initialSpringVelocity: 0,
            options: .curveEaseInOut,
            animations: {
                self.textViewContainer.backgroundColor = UIColor(red:1.00, green:0.60, blue:0.60, alpha:1.0) //UIColor(red:0.78, green:1.00, blue:0.70, alpha:1.0)
            }
        )
        UIView.animate(
            withDuration: 0.75,
            delay: 0,
            usingSpringWithDamping: 0.5,
            initialSpringVelocity: 0,
            options: .curveEaseInOut,
            animations: {
                self.textView.backgroundColor = UIColor(red:1.00, green:0.96, blue:0.96, alpha:1.0) //UIColor(red:0.78, green:1.00, blue:0.70, alpha:1.0)
        }
        )
        
//        UIView.animate(
//            withDuration: 0.2,
//            animations: {
//                self.textViewContainer.backgroundColor = UIColor(red:0.74, green:1.00, blue:0.65, alpha:1.0)
//                },
//            completion: {
//                finished in
//                if finished {
//                    UIView.animate(
//                        withDuration: 0.25,
//                        animations: {
//                            self.textViewContainer.backgroundColor = UIColor(red:0.81, green:1.00, blue:0.73, alpha:1.0)
//                            },
//                        completion: {
//                            finished in
//                            if finished {
//                                UIView.animate(
//                                    withDuration: 0.25,
//                                    animations: {
//                                        self.textViewContainer.backgroundColor = UIColor(red:0.76, green:1.00, blue:0.68, alpha:1.0)
//                                    },
//                                    completion: {
//                                        finished in
//                                        if finished {
//                                            UIView.animate(
//                                                withDuration: 0.25,
//                                                animations: {
//                                                    self.textViewContainer.backgroundColor = UIColor(red:0.78, green:1.00, blue:0.70, alpha:1.0)
//                                                }
//                                            )
//                                        }
//                                    }
//                                )
//                            }
//                            }
//                    )
//                }
//                }
//        )
//        UIView.animate(withDuration: 0.25) {
//            self.textViewContainer.backgroundColor = UIColor(red:1.00, green:0.80, blue:0.80, alpha:1.0)
//        }
//        UIView.animate(withDuration: 0.25) {
//            self.textViewContainer.backgroundColor = UIColor(red:1.00, green:0.60, blue:0.60, alpha:1.0) //.red
//        }
        print("Update")
        startRecording()
    }
    
    @IBAction func mikeButtonTouchUpInside(_ sender: Any) {
        mikeButtonUp()
    }
    
    
    @IBAction func mikeButtonTouchUpOutside(_ sender: Any) {
        mikeButtonUp()
    }
    
    func mikeButtonUp(){
        print("Btn Up")
        textView.isEditable = true
//        textViewContainer.backgroundColor = UIColor.white
//        textViewContainer.setNeedsDisplay()
        UIView.animate(
            withDuration: 0.25,
            delay: 0,
            options: .curveEaseInOut,
            animations: {
                self.textViewContainer.backgroundColor = UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.0)
            }
        )
        UIView.animate(
            withDuration: 0.25,
            delay: 0,
            options: .curveEaseInOut,
            animations: {
                self.textView.backgroundColor = .white
        }
        )
        
        stopRecording()
    }
//    @IBAction func addButtonClick(_ sender: UIButton) {
//        print("Click!")
//
//        // Both work
//        //self.view.endEditing(true)
//        //textView.resignFirstResponder()
//    }
    //    func textViewShouldReturn(_ textView: UITextView) -> Bool {
//        self.view.endEditing(true)
//        return true
//    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        print("segue")
        switch(segue.identifier ?? ""){
        case "AdjustQuantitySegue":
            guard let adjustQuantityViewController = segue.destination as? MealQuantityViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            adjustQuantityViewController.foods = self.foods
        case "SaveMealSegue":
            return
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String?, sender: Any?) -> Bool {
        print("segue")
        switch(identifier ?? ""){
        case "AdjustQuantitySegue":
            if self.foods.count > 0 {
                return true
            } else {
                return false
            }
        case "SaveMealSegue":
            return true
        default:
            return false
        }
    }
    
    //MARK: Actions
    @IBAction func unwindToAddMeal(sender: UIStoryboardSegue){
        if let sourceViewController = sender.source as?
            MealQuantityViewController, let adjustedFoods = sourceViewController.foods {

            print("unwind")
            self.foods = adjustedFoods
            self.tableView.reloadData()
        }
    }


    func getSnippets() { // completion: @escaping ([String]) -> Void) {
        Alamofire.request(
            "http://127.0.0.1:8000/snippets/",
//            parameters: ["content": contentID],
            headers: ["Authorization": "admin:password123"]
            )
            .responseJSON { response in
                guard response.result.isSuccess else {
                    print("Error while fetching tags: \(String(describing: response.result.error))")
//                    completion([String]())
                    return
                }
                
                guard let responseJSON = response.result.value as? [String: Any] else {
                    print("Invalid tag information received from the service")
//                    completion([String]())
                    return
                }
                
                print(responseJSON)
//                completion([String]())
        }
    }
    
    func postSnippets() { // completion: @escaping ([String]) -> Void) {
        let credential = URLCredential(user: "admin", password: "password123", persistence: .none)
        let plainString = "admin:password123" as NSString
        let plainData = plainString.data(using: String.Encoding.utf8.rawValue)
        let base64String = plainData?.base64EncodedString()
        let authHeader = "Basic " + base64String!
        print(authHeader)
        //Alamofire.Manager.sharedInstance.defaultHeaders["Authorization"] = "Basic " + base64String!
        
        Alamofire.request(
            "http://127.0.0.1:8000/snippets/",
            method: .post,
            parameters: ["code": "print \"123\""],
            headers: ["Authorization": authHeader]
            )
//            .authenticate(user: "admin", password: "password123")
//            .authenticate(usingCredential: credential)
            .responseJSON { response in
                guard response.result.isSuccess else {
                    print("Error while fetching tags: \(String(describing: response.result.error))")
//                    completion([String]())
                    return
                }
                
                guard let responseJSON = response.result.value as? [String: Any] else {
                    print("Invalid tag information received from the service")
//                    completion([String]())
                    return
                }
                
                print(responseJSON)
//                completion([String]())
        }
    }
}
