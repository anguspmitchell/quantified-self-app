//
//  NutritionInfo.swift
//  
//
//  Created by Angus Mitchell on 11/29/17.
//

import UIKit

class NutritionInfo {
    var calories: Float?
    var fat: Float?
    var protein: Float?
    var carbohydrates: Float?
    var sugar: Float?
    
    init(){
    }
    
    init(random: Bool){
        self.calories = 200 + Float(arc4random_uniform(200))
        self.fat = 10 + Float(arc4random_uniform(30))
        self.protein = 20 + Float(arc4random_uniform(30))
        self.carbohydrates = 20 + Float(arc4random_uniform(40))
        self.sugar = round(self.carbohydrates! * Float(arc4random()) / Float(UInt32.max))
    }
    
    func add(term: NutritionInfo){
        self.calories = (self.calories ?? 0) + (term.calories ?? 0)
        self.fat = (self.fat ?? 0) + (term.fat ?? 0)
        self.protein = (self.protein ?? 0) + (term.protein ?? 0)
        self.carbohydrates = (self.carbohydrates ?? 0) + (term.carbohydrates ?? 0)
        self.sugar = (self.sugar ?? 0) + (term.sugar ?? 0)
    }
}
