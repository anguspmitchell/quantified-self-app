//
//  Quantity.swift
//  Siri
//
//  Created by Angus Mitchell on 11/9/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class Quantity {
    var number: Float?
    //var measurementType: MeasurementType?
    var measurementType: String?
    var divisor: Int?
    
    init(number: Float?, measurementType: String?){
        self.number = number
        self.measurementType = measurementType
    }
    
    init(){
        
    }
}
