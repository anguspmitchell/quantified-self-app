//
//  CookingMethod.swift
//  Siri
//
//  Created by Angus Mitchell on 11/9/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class CookingMethod {
    var method: String
    
    init?(method: String){
        guard !method.isEmpty else {
            return nil
        }
        self.method = method
    }
}
