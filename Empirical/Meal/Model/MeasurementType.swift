//
//  MeasurementType.swift
//  KitchenMonitor
//
//  Created by Angus Mitchell on 2/5/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

//enum MeasurementType {
//    case unit
//    case teaspoon
//    case tablespoon
//    case fluidOunce
//    case gill
//    case cup
//    case pint
//    case quart
//    case gallon
//    case milliliter
//    case liter
//    case deciliter
//    case pound
//    case ounce
//    case milligram
//    case gram
//    case kilogram
//    case millimeter
//    case centimeter
//    case meter
//    case inch
//    
//    var abbv: String {
//        switch self {
//        case .unit: return "units"
//        case .teaspoon: return "tsp."
//        case .tablespoon: return "tbsp."
//        case .fluidOunce: return "fl oz"
//        case .gill: return "gill"
//        case .cup: return "c"
//        case .pint: return "pt"
//        case .quart: return "qt"
//        case .gallon: return "gal"
//        case .milliliter: return "ml"
//        case .liter: return "l"
//        case .deciliter: return "dl"
//        case .pound: return "lb"
//        case .ounce: return "oz"
//        case .milligram: return "mg"
//        case .gram: return "g"
//        case .kilogram: return "kg"
//        case .millimeter: return "mm"
//        case .centimeter: return "cm"
//        case .meter: return "m"
//        case .inch: return "in"
//        }
//    }
//}

