//
//  MealItem.swift
//  Siri
//
//  Created by Angus Mitchell on 11/6/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class Food: CustomStringConvertible {
    //MARK: Properties
    var name: String
//    var with: [Food]?
    var cookingMethods: [String]?
    var toppedWith: [Food]?
    var children = [Food]()
//    var constituents = [Food]?
    var cookedWith: [Food]?
    //var quantity: String?
    //var quantityNumber: Float?
    var quantity: Quantity?
    var restaurant: String?
    var image: UIImage?
    var nutritionInfo: NutritionInfo?
    var tags = [String: String]()
    
    
    var transcriptions = [(String, Float)]()
    
    init?(name: String) {
        // The name must not be empty
        guard !name.isEmpty else {
            return nil
        }
        
        self.name = name
        self.nutritionInfo = NutritionInfo(random: true)
        
        if name.lowercased() == "steel cut oats" {
            self.image = UIImage(named: "steel cut oats")
        } else if name.lowercased() == "blueberries" {
            self.image = UIImage(named: "blueberries")
        } else if name.lowercased() == "almonds" {
            self.image = UIImage(named: "almonds")
        } else if name.lowercased() == "cranberries" {
            self.image = UIImage(named: "cranberries")
        } else if name.lowercased() == "kale salad" {
            self.image = UIImage(named: "kale salad")
        } else if name.lowercased() == "chicken" {
            self.image = UIImage(named: "chicken")
        } else if name.lowercased() == "green beans" {
            self.image = UIImage(named: "green beans")
        } else if name.lowercased() == "potatoes" {
            self.image = UIImage(named: "potatoes")
        }
    }
    
    public var description: String {
        var returnString = "\nFOOD: \(name)"
        if let quantity = self.quantity {
            returnString += "("
            if let quantityNumber = quantity.number {
                returnString += String(quantityNumber) + " "
            }
            if let quantityDivisor = quantity.divisor {
                returnString += "/ " + String(quantityDivisor) + " "
            }
            if let quantityMeasure = quantity.measurementType {
                returnString += quantityMeasure //.abbv
            } else {
                returnString += "unknown"
            }
            returnString += ")"
        }
        if let cookingMethods = self.cookingMethods {
            if(cookingMethods.count > 0){
                returnString += "("
            }
            for method in cookingMethods {
                returnString += method + "~"
            }
            if(cookingMethods.count > 0){
                returnString += ")"
            }
        }
        if let restaurant = self.restaurant {
            returnString += "R"
        }
        var toppedString = ""
        if let toppedWith = toppedWith {
            for relatedFood in toppedWith {
                toppedString += relatedFood.description
            }
        }
        if toppedString.count > 0 {
            returnString += "\nTOPPED: " + toppedString
        }
        var cookedString = ""
        if let cookedWith = cookedWith {
            for relatedFood in cookedWith {
                cookedString += relatedFood.description
            }
        }
        if cookedString.count > 0 {
            returnString += "\nCOOKED: " + cookedString
        }
        return returnString
    }
    
    func calcCumulativeNutritionalInfo() -> NutritionInfo {
        return calcCumulativeNutritionalInfo(food: self)
    }
    
    func calcCumulativeNutritionalInfo(food: Food?) -> NutritionInfo {
        let cumulativeNutrionalInfo = NutritionInfo()
        if let food = food {
            if let nutritionalInfo = self.nutritionInfo {
                cumulativeNutrionalInfo.add(term: nutritionalInfo)
            }
            if let toppedWith = food.toppedWith {
                for topping in toppedWith {
                    cumulativeNutrionalInfo.add(term: topping.calcCumulativeNutritionalInfo())
                }
            }
        }
        return cumulativeNutrionalInfo
    }
    
    func countChildrenInclusive() -> Int {
        return countChildrenInclusive(food: self)
    }
    
    func countChildrenInclusive(food: Food?) -> Int {
        if let food = food {
            var childrenCount = 0
            if let toppedWith = food.toppedWith {
                for topping in toppedWith {
                    childrenCount += countChildrenInclusive(food: topping)
                }
            }
//            if let cookedWith = food.cookedWith {
//                for cookingIngredient in cookedWith {
//                    childrenCount += countChildrenInclusive(food: cookingIngredient)
//                }
//            }
            return childrenCount + 1
        }
        return 0
    }
    
    func getNthChild(n: Int) -> (Food?, Int) {
        let tuple = getNthIngredient(n: n, runningCount: 0, depth: 0, food: self)
        return (tuple.0, tuple.2)
    }
    // oatmeal       0 call(n = 4, runningCount: 0, food: oatmeal, depth: 0)
    //   blueberries 1   call(n = 4, runningCount: 0 + 0 + 1, food: blueberries, depth: 0 + 1)
    //     butter    2     call(n = 4, runningCount: 1 + 0 + 1, food: butter, depth: 1 + 1)
    //                     return to blueberry (nil, 1, 0)
    //                   blueberry childrenCount = 1
    //                   return to oatmeal (nil, 2, 0)
    //
    //                   oatmeal childrenCount = 2
    //   almonds     3   call(n = 4, runningCount: 0 + 2 + 1, food: almonds, depth: 0 + 1)
    //                   return (nil, 1, 0) to oatmeal
    //                   oatmeal childrenCount = 3
    //   walnuts     4   call(n = 4, runningCount: 0 + 3 + 1, food: walnuts, depth: 0 + 1)
    
    func getNthIngredient(n: Int, runningCount: Int, depth: Int, food: Food?) -> (Food?, Int, Int) {
        if let food = food {
            if n == runningCount {
                return (food, runningCount, depth)
            }
            
            var childrenCount = 0
            if let toppedWith = food.toppedWith {
                for topping in toppedWith {
                    var tuple = getNthIngredient(n: n, runningCount: runningCount + childrenCount + 1, depth: depth + 1, food: topping)
                    if tuple.0 != nil {
                        return tuple
                    }
                    childrenCount += tuple.1
                }
            }
            return (nil, childrenCount + 1, depth)
        }
        return (nil, 0, 0)
    }
    
//    func getNthIngredient(n: Int, count: Int, food: Food?) -> (Food?, Int) {
//        if count > n {
//            return (nil, 0)
//        }
//        if let food = food {
//            if n == count {
//                return (food, 1)
//            }
//
//            if let toppedWith = food.toppedWith {
//                var childrenCount
//                for topping in toppedWith {
//                    return getNthIngredient(n: n, count: count + 1, food: topping)
//                }
//            }
//        }
//        return (nil, 0)
//    }
    
}
