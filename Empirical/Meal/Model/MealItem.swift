//
//  MealItem.swift
//  Siri
//
//  Created by Angus Mitchell on 11/6/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class MealItem {
    //MARK: Properties
    //var name: String
    var transcriptions = [(String, Float)]()
    var image: UIImage?
    var restaurant: String?
    var brand: String?
    var amount: Float?
    var measurementType: MeasurementType?
    var cookingFat: String?
    var cookingMethod: String?
    var acidity: String?
    var saltiness: String?
    var isDrink: Bool?
    
    var food: Food
    
    init?(food: Food) {
        // The name must not be empty
        guard food != nil else {
            return nil
        }
        
        self.food = food
    }
}
