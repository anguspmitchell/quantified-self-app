//
//  ResponseVariable.swift
//  Siri
//
//  Created by Angus Mitchell on 12/12/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class ResponseVariable {
    var name: String
    var inputs: [ResponseVariableInput] = [ResponseVariableInput]()
    
    init(name: String, inputs: [ResponseVariableInput]){
        self.name = name
        self.inputs = inputs
    }
}

