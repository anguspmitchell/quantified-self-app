//
//  ResponsePopupViewController.swift
//  Siri
//
//  Created by Angus Mitchell on 12/12/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class ResponsePopupViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var responseVariableLabel: UILabel!
    
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var inputStackView: UIStackView!
    @IBOutlet weak var containerView: UIView!
    var responseVariable: ResponseVariable?
    var responseVariableIndexPath: IndexPath?
    var inputComponents: [UIView] = [UIView]()
    
//    @IBOutlet weak var pickerViewTest: UIPickerView!
    
//    var pickerViews: [UIPickerView] = [UIPickerView]()
//    var testData: [Int] = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        containerView.layer.cornerRadius = 10.0
//        for i in 150..<250 {
//            testData.append(i)
//        }
        
//        pickerViewTest.dataSource = self
//        pickerViewTest.delegate = self
        
        constructInputUI()
    }
    
    func constructInputUI() {
        if let variable = responseVariable {
            responseVariableLabel.text = variable.name
            
            var allPickers = variable.inputs.map { $0.uiType == InputUIType.pickerView }.reduce(true, { $0 && $1 })
            
            if allPickers {
                if variable.inputs.count > 1 {
                    // Top labels
                    let horizontalLabelStack = UIStackView()
                    horizontalLabelStack.axis = .horizontal
                    horizontalLabelStack.distribution = .fillEqually
                    
                    for input in variable.inputs {
                        let label = UILabel()
                        label.text = input.name
                        label.textAlignment = .center
                        horizontalLabelStack.addArrangedSubview(label)
                    }
                    
                    inputStackView.addArrangedSubview(horizontalLabelStack)
                }
//                    let label1 = UILabel()
//                    label1.text = "Diastolic"
//                    label1.textAlignment = .center
//                    horizontalLabelStack.addArrangedSubview(label1)
//
//                    let label2 = UILabel()
//                    label2.text = "Systolic"
//                    label2.textAlignment = .center
//                    horizontalLabelStack.addArrangedSubview(label2)
//
//                    let label3 = UILabel()
//                    label3.text = "Pulse"
//                    label3.textAlignment = .center
//                    horizontalLabelStack.addArrangedSubview(label3)
                
                
                
                let pickerView = UIPickerView()
                pickerView.dataSource = self
                pickerView.delegate = self
                
                for (i, input) in variable.inputs.enumerated() {
                    pickerView.selectRow(input.getRowOfCoalescedValue(), inComponent: i, animated: false)
                }
                
                inputComponents.append(pickerView)
                
                inputStackView.addArrangedSubview(pickerView)
//                    pickerViews.append(pickerView)
                
//                    for pv in pickerViews {
//                        inputStackView.addArrangedSubview(pv)
//                    }
                
                inputStackView.layoutIfNeeded()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Picker View Delegate
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if let variable = responseVariable {
            return variable.inputs.count
        }
        return 0
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if let variable = responseVariable {
            return variable.inputs[component].valueRange.count
        }
        return 0
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if let variable = responseVariable {
            return String(describing: variable.inputs[component].valueRange[row])
        }
        return "Error"
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        super.prepare(for:segue, sender:sender)
        
        // Configure the destination view controller only when the done button is pressed.
        guard let variable = responseVariable, let button = sender as? UIButton else {
            return
        }
        
        if button === doneButton {
            let allPickers = variable.inputs.map { $0.uiType == InputUIType.pickerView }.reduce(true, { $0 && $1 })
            
            if allPickers {
                if let pv = inputComponents[0] as? UIPickerView {
                    for (i, input) in variable.inputs.enumerated() {
                        let selectedRow = pv.selectedRow(inComponent: i)
                        if let valueString = pickerView(pv, titleForRow: selectedRow, forComponent: i) {
                            input.setValue(valueString: valueString)
                        }
                    }
                }
            }
        } else if button === clearButton {
            for input in variable.inputs {
                input.value = nil
            }
        }
    }
 

}
