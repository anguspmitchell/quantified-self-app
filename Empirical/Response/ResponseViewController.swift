//
//  ResponseViewController.swift
//  Siri
//
//  Created by Angus Mitchell on 12/6/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class ResponseViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    fileprivate let itemsPerRow: CGFloat = 4
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
//    var responseVariables: [String] = ["Weight", "Body Fat", "Blood Pressure", "Hydration", "Mood", "Energy", "Cognitive", "Hunger", "Thirst", "Journal"]
//    var shortcutTitles: [String] = ["I'm Hungry", "Body Tired", "Can't Focus"]
//    var shortcutVariables: [String] = ["Hunger", "Energy", "Cognitive"]
//    var shortcutValues: [String] = ["Starving", "Tired", "Low"]
    
    var responseVariableShortcuts: [(ResponseVariableShortcut, ResponseVariableInput, ResponseVariable)] = [(ResponseVariableShortcut, ResponseVariableInput, ResponseVariable)]()
    var responseVariables: [ResponseVariable] = [ResponseVariable]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.allowsMultipleSelection = true
        
        initializeVariables()
//        view.layoutIfNeeded()
    }
    
    func initializeVariables() {
        let weightRange = Array(10...600)
        let weightInput = ResponseVariableInput(name: "Weight", initialValue: 180 as AnyObject, valueRange: weightRange as [AnyObject], uiType: .pickerView)
        let weight = ResponseVariable(name: "Weight", inputs: [weightInput!])
        responseVariables.append(weight)
        
        let systolicRange = Array(80...220)
        let diastolicRange = Array(50...150)
        let pulseRange = Array(30...250)
        let systolicInput = ResponseVariableInput(name: "Systolic", initialValue: 105 as AnyObject, valueRange: systolicRange as [AnyObject], uiType: .pickerView)
        let diastolicInput = ResponseVariableInput(name: "Diastolic", initialValue: 60 as AnyObject, valueRange: diastolicRange as [AnyObject], uiType: .pickerView)
        let pulseInput = ResponseVariableInput(name: "Pulse", initialValue: 50 as AnyObject, valueRange: pulseRange as [AnyObject], uiType: .pickerView)
        let bloodPressure = ResponseVariable(name: "Blood Pressure", inputs: [systolicInput!, diastolicInput!, pulseInput!])
        responseVariables.append(bloodPressure)
        
        let bodyFat = ResponseVariable(name: "Body Fat", inputs: [])
        responseVariables.append(bodyFat)
        
        let hydration = ResponseVariable(name: "Hydration", inputs: [])
        responseVariables.append(hydration)
        
        let mood = ResponseVariable(name: "Mood", inputs: [])
        responseVariables.append(mood)
        
        let energyRange = ["Exhausted", "Tired", "Normal", "Fresh", "Energetic"]
        let energyInput = ResponseVariableInput(name: "Energy", initialValue: "Normal" as AnyObject, valueRange: energyRange as [AnyObject], uiType: .pickerView)
        let energyShortcut = energyInput!.createShortcut(name: "Body Tired", value: "Tired")
        let energy = ResponseVariable(name: "Energy", inputs: [energyInput!])
        responseVariables.append(energy)
        responseVariableShortcuts.append((energyShortcut!, energyInput!, energy))
        
        let cognitiveRange = ["Muddled", "Sluggish", "Normal", "Alert", "Focused"]
        let cognitiveInput = ResponseVariableInput(name: "Cognitive", initialValue: "Normal" as AnyObject, valueRange: cognitiveRange as [AnyObject], uiType: .pickerView)
        let cognitiveShortcut = cognitiveInput!.createShortcut(name: "Can't Focus", value: "Muddled")
        let cognitive = ResponseVariable(name: "Cognitive", inputs: [cognitiveInput!])
        responseVariables.append(cognitive)
        responseVariableShortcuts.append((cognitiveShortcut!, cognitiveInput!, cognitive))
        
        let hungerRange = ["Starving", "Hungry", "Normal", "Full", "Stuffed"]
        let hungerInput = ResponseVariableInput(name: "Hunger", initialValue: "Normal" as AnyObject, valueRange: hungerRange as [AnyObject], uiType: .pickerView)
        let hungerShortcut = hungerInput!.createShortcut(name: "I'm Hungry", value: "Hungry")
        let fullShortcut = hungerInput!.createShortcut(name: "I'm Full", value: "Full")
        let hunger = ResponseVariable(name: "Hunger", inputs: [hungerInput!])
        responseVariables.append(hunger)
        responseVariableShortcuts.append((hungerShortcut!, hungerInput!, hunger))
        responseVariableShortcuts.append((fullShortcut!, hungerInput!, hunger))
        
        let thirst = ResponseVariable(name: "Thirst", inputs: [])
        responseVariables.append(thirst)
        
        let journal = ResponseVariable(name: "Journal", inputs: [])
        responseVariables.append(journal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return section == 0 ? responseVariableShortcuts.count : responseVariables.count //responseVariables.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResponseShortcutCell",
                                                                for: indexPath) as? ResponseShortcutCollectionViewCell else { fatalError("Dequeued cell is not an instance of ResponseShortcutCollectionViewCell") }
            cell.cellContentView.layer.borderWidth = 1
            cell.cellContentView.layer.borderColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0).cgColor
//            print(cell.customIsSelected)
//            cell.backgroundColor = cell.customIsSelected ? .blue : .clear
            
            //cell.responseNameLabel.text = responseVariables[indexPath.row]
            let shortcut_input_variable = responseVariableShortcuts[indexPath.row]
            let shortcut = shortcut_input_variable.0
            let input = shortcut_input_variable.1
            let variable = shortcut_input_variable.2
            
            cell.responseShortcutTitleTextView.text = shortcut.name //shortcutTitles[indexPath.row]
            cell.responseVariableLabel.text = variable.name //shortcutVariables[indexPath.row]
//            cell.responseValueLabel.text = String(describing: shortcut.value) //shortcutValues[indexPath.row]
            let isOneWord = shortcut.name.split(separator: " ").count == 1
            
            cell.layoutIfNeeded()
            let heightOfSuperview = cell.responseShortcutTitleContainer.frame.size.height
            let widthOfSuperview = cell.responseShortcutTitleContainer.frame.size.width
            cell.responseShortcutTitleHeightConstraint.constant = heightOfSuperview
//            print(heightOfSuperview)
            cell.responseShortcutTitleTextView.layoutIfNeeded()
            
            let minFontSize: CGFloat = 6.0
            var fontSize: CGFloat = cell.responseShortcutTitleTextView.font?.pointSize ?? 17.0
            
            while (fontSize >= minFontSize && (cell.responseShortcutTitleTextView.sizeThatFits(CGSize(width: cell.responseShortcutTitleTextView.frame.size.width, height: CGFloat.greatestFiniteMagnitude)).height >= cell.responseShortcutTitleHeightConstraint.constant || (isOneWord && cell.responseShortcutTitleTextView.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: cell.responseShortcutTitleTextView.frame.size.height)).width >= widthOfSuperview))) {
                
//                print("\(cell.responseShortcutTitleTextView.sizeThatFits(CGSize(width: cell.responseShortcutTitleTextView.frame.size.width, height: CGFloat.greatestFiniteMagnitude)).height) \(cell.responseShortcutTitleHeightConstraint.constant)")
                fontSize = fontSize - CGFloat(1.0)
                cell.responseShortcutTitleTextView.font = cell.responseShortcutTitleTextView.font?.withSize(fontSize)
                cell.responseShortcutTitleTextView.layoutIfNeeded()
            }
            
            let fixedWidth = cell.responseShortcutTitleTextView.frame.size.width
            let newSize = cell.responseShortcutTitleTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            cell.responseShortcutTitleHeightConstraint.constant = newSize.height
            cell.responseShortcutTitleTextView.layoutIfNeeded()
//            print(newSize.height)
//            print("----")
            
            if input.isSetToShortcut(shortcut: shortcut){
                cell.backgroundColor = .blue
            } else {
                cell.backgroundColor = .clear
            }
            
            if indexPath.row % 2 == 0 {
                //            print("requires input \(indexPath.row)")
//                cell.requiresInput = true
            }
            return cell
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResponseCell",
                                                          for: indexPath) as? ResponseCollectionViewCell else { fatalError("Dequeued cell is not an instance of ResponseCollectionViewCell") }
            cell.cellContentView.layer.borderWidth = 1
            cell.cellContentView.layer.borderColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0).cgColor
//            print(cell.customIsSelected)
//            cell.backgroundColor = cell.customIsSelected ? .blue : .clear
            //cell.responseNameLabel.text = responseVariables[indexPath.row]
            
            let responseVariable = responseVariables[indexPath.row]
            cell.responseVariableTextView.text = responseVariable.name //responseVariables[indexPath.row]
            
            let isOneWord = responseVariable.name.split(separator: " ").count == 1
//            if responseVariable.name == "Cognitive" {
//                var bp = true
//            }

            cell.layoutIfNeeded()
            let heightOfSuperview = cell.responseVariableContainer.frame.size.height
            let widthOfSuperview = cell.responseVariableContainer.frame.size.width
            cell.responseVariableTextViewHeightConstraint.constant = heightOfSuperview
            cell.responseVariableTextView.layoutIfNeeded()
            
            let minFontSize: CGFloat = 6.0
            var fontSize: CGFloat = cell.responseVariableTextView.font?.pointSize ?? 17.0
            
            while (fontSize >= minFontSize && (cell.responseVariableTextView.sizeThatFits(CGSize(width: cell.responseVariableTextView.frame.size.width, height: CGFloat.greatestFiniteMagnitude)).height >= cell.responseVariableTextViewHeightConstraint.constant || (isOneWord && cell.responseVariableTextView.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: cell.responseVariableTextView.frame.size.height)).width >= widthOfSuperview))) {
                
                //                print("\(cell.responseShortcutTitleTextView.sizeThatFits(CGSize(width: cell.responseShortcutTitleTextView.frame.size.width, height: CGFloat.greatestFiniteMagnitude)).height) \(cell.responseShortcutTitleHeightConstraint.constant)")
                fontSize = fontSize - CGFloat(1.0)
                cell.responseVariableTextView.font = cell.responseVariableTextView.font?.withSize(fontSize)
                cell.responseVariableTextView.layoutIfNeeded()
            }
            
            let fixedWidth = cell.responseVariableTextView.frame.size.width
            let newSize = cell.responseVariableTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            cell.responseVariableTextViewHeightConstraint.constant = newSize.height
            cell.responseVariableTextView.layoutIfNeeded()
//            cell.responseVariableTextView.layoutIfNeeded()
////            cell.responseVariableTextView.setNeedsDisplay()
////            cell.responseVariableTextView.setNeedsLayout()
//
//            let fixedWidth = cell.responseVariableTextView.frame.size.width
//            let newSize = cell.responseVariableTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
//            cell.responseVariableTextViewHeightConstraint.constant = newSize.height
            
//            print("cellForItemAt --> _")
//            if(cell.isSelected) {
//                print("***VARIABLE SELECTED*** cellForItemAt")
//            }
            
            let inputsValued = responseVariable.inputs.map{ $0.value != nil }.reduce(true, { $0 && $1 }) && responseVariable.inputs.count > 0
            if inputsValued {
                cell.backgroundColor = .blue
                
                var inputValueString = ""
                let inputValues = responseVariable.inputs.map({ $0.value })
                for (i, inputValue) in inputValues.enumerated() {
                    if let value = inputValue {
                        inputValueString += String(describing: value)
                        if i < inputValues.count - 1 {
                            inputValueString += " "
                        }
                    }
                }
//                if responseVariable.inputs.count == 1 {
//                    if let value = responseVariable.inputs[0].value
//                    cell.responseValueLabel.text = String(describing: value)
//                } else if responseVariable.inputs.count > 1 {
//                    cell.responseValueLabel.text = "Multiple Values"
//                }
                if inputValueString.count > 19 {
                    inputValueString = "Multiple Values"
                }
                cell.responseValueLabel.text = inputValueString
            } else {
                cell.responseValueLabel.text = ""
                cell.backgroundColor = .clear
            }
            
            
            if indexPath.row % 2 == 0 {
    //            print("requires input \(indexPath.row)")
//                cell.requiresInput = true
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        shouldSelectItemAt indexPath: IndexPath) -> Bool {
//        print("Should select item at")
//        return true // indexPath.section == 0
//        //largePhotoIndexPath = largePhotoIndexPath == indexPath ? nil : indexPath
//        //return false
//        guard let cell = collectionView.cellForItem(at: indexPath) as? ResponseCollectionViewCell else { fatalError("Dequeued cell is not an instance of ResponseCollectionViewCell") }
////        print("RI \(cell.requiresInput) \(indexPath.row)")
//        return !cell.requiresInput
        
        if indexPath.section == 0 {
            return true
        } else {
            guard let cell = collectionView.cellForItem(at: indexPath) as? ResponseCollectionViewCell else { fatalError("Selected cell is not an instance of ResponseCollectionViewCell") }
//            print("Should Select --> _")
//            if(cell.isSelected) {
//                print("***VARIABLE SELECTED*** shouldSelectItem")
//            }
            return true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            guard let cell = collectionView.cellForItem(at: indexPath) as? ResponseShortcutCollectionViewCell else { fatalError("Selected cell is not an instance of ResponseShortcutCollectionViewCell") }
            
            let shortcut_input_variable = responseVariableShortcuts[indexPath.row]
            let input = shortcut_input_variable.1
            let shortcut = shortcut_input_variable.0
            
            // Selection
            if cell.backgroundColor == .clear {
                input.setValue(shortcut: shortcut)
            } else {
                input.clearValue()
            }
           
            
//            collectionView.reloadSections(IndexSet(1...1))
            collectionView.reloadData()
//            print("Shortcut Select --> Blue")
//            cell.backgroundColor = .blue
//            cell.isSelected
//            print("SELECT")
        } else {
            guard let cell = collectionView.cellForItem(at: indexPath) as? ResponseCollectionViewCell else { fatalError("Selected cell is not an instance of ResponseCollectionViewCell") }
            // Segue to modal popover
//            if(cell.isSelected) {
//                print("***VARIABLE SELECTED*** didSelectItemAt")
//            }
//            print("Variable Select --> _")
//            cell.isSelected = false
//            print("cell.isSelected = false : didDeselectItemAt")
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        didDeselectItemAt indexPath: IndexPath) {
//        if let cell = collectionView.cellForItem(at: indexPath as IndexPath) {
////            cell.isSelected = false
////            print("Deselect!")
////            print(cell.isSelected)
////            cell.backgroundColor = .clear
//        }
        
        if indexPath.section == 0 {
            guard let cell = collectionView.cellForItem(at: indexPath) as? ResponseShortcutCollectionViewCell else { fatalError("Selected cell is not an instance of ResponseShortcutCollectionViewCell") }
            
            let shortcut_input_variable = responseVariableShortcuts[indexPath.row]
            let input = shortcut_input_variable.1
            input.clearValue()
            
            collectionView.reloadData()
//            collectionView.reloadSections(IndexSet(1...1))
//            print("Shortcut Deselect --> Clear")
//            cell.backgroundColor = .clear
            //            cell.isSelected
//            print("DESELECT")
        } else {
            guard let cell = collectionView.cellForItem(at: indexPath) as? ResponseCollectionViewCell else { fatalError("Selected cell is not an instance of ResponseCollectionViewCell") }
            // Segue to modal popover
            print("Variable Deselect --> _")
//            if(cell.isSelected) {
//                print("***VARIABLE SELECTED*** didDeselectItem")
//            }
//            cell.isSelected = false
//            print("cell.isSelected = false : didDeselectItemAt")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                                 viewForSupplementaryElementOfKind kind: String,
                                 at indexPath: IndexPath) -> UICollectionReusableView {
        //1
        switch kind {
        //2
        case UICollectionElementKindSectionHeader:
            //3
            guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "ResponseCollectionHeaderView",
                                                                             for: indexPath) as? ResponseCollectionHeaderView else { fatalError("Dequeued supplementary view is not ResponseCollectionHeaderView")
                                                                                
            }
            if indexPath.section == 0 {
                headerView.headerLabel.text = "Shortcuts"
            } else if indexPath.section == 1 {
                headerView.headerLabel.text = "Response Variables"
            }
            headerView.layoutIfNeeded()
            headerView.containerView.layer.addBorder(edge: .bottom, color: .black, thickness: 1.0)
//            print("ADD BORDER")
            return headerView
        default:
            //4
            assert(false, "Unexpected element kind")
        }
    }
    
    // MARK: - Collection View Flow
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow // floor(availableWidth / itemsPerRow)
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        super.prepare(for: segue, sender: sender)
//        print("segue")
        switch(segue.identifier ?? ""){
        case "ResponsePopupSegue":
            guard let responsePopupViewController = segue.destination as? ResponsePopupViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedVariableCell = sender as? ResponseCollectionViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            guard let indexPath = collectionView.indexPath(for: selectedVariableCell) else{
                fatalError("The selected cell is not being displayed by the collection view")
            }
            
            responsePopupViewController.responseVariable = responseVariables[indexPath.row]
            responsePopupViewController.responseVariableIndexPath = indexPath
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
        
    }
 
    // Background color always goes to blue
    @IBAction func unwindToEditedResponseVariable(sender: UIStoryboardSegue){
        if let sourceViewController = sender.source as?
            ResponsePopupViewController {
            
            if let indexPath = sourceViewController.responseVariableIndexPath, let responseVariable = sourceViewController.responseVariable {
                
                if let cell = collectionView.cellForItem(at: indexPath) as? ResponseCollectionViewCell {
//                    print("Done --> Unwind --> Blue")
//                    if(cell.isSelected) {
//                        print("***VARIABLE SELECTED*** Done")
//                    }
//                    cell.backgroundColor = .blue
//                    cell.layoutIfNeeded()
//                    cell.customIsSelected = true
                    
                    responseVariables[indexPath.row] = responseVariable
//                    collectionView.reloadItems(at: [indexPath])
                    collectionView.reloadData()
                    
//                    cell.customIsSelected = true
                }
            }
            
        }
    }
    
    // Background color unchanged
    @IBAction func unwindToCanceledResponseVariable(sender: UIStoryboardSegue){
//        print("Cancel --> Unwind --> _")
        
        if let sourceViewController = sender.source as?
            ResponsePopupViewController {
            
            if let indexPath = sourceViewController.responseVariableIndexPath, let responseVariable = sourceViewController.responseVariable {
                
                if let cell = collectionView.cellForItem(at: indexPath) {
//                    collectionView.reloadItems(at: [indexPath])
                    collectionView.reloadData()
                }
            }
            
        }

        
//        return
    }
    
    // Background color always goes to clear
    @IBAction func unwindToClearedResponseVariable(sender: UIStoryboardSegue){
        if let sourceViewController = sender.source as?
            ResponsePopupViewController {
            
            if let indexPath = sourceViewController.responseVariableIndexPath, let responseVariable = sourceViewController.responseVariable {
                
                if let cell = collectionView.cellForItem(at: indexPath) as? ResponseCollectionViewCell {
//                    print("Clear --> Unwind --> Clear")
//                    if(cell.isSelected) {
//                        print("***VARIABLE SELECTED*** Cleared")
//                    }
//                    cell.backgroundColor = .clear
//                    cell.customIsSelected = false
//                    cell.layoutIfNeeded()
                    
                    responseVariables[indexPath.row] = responseVariable
//                    collectionView.reloadItems(at: [indexPath])
                    collectionView.reloadData()
                    
//                    cell.customIsSelected = false
                }
            }
            
        }
    }
}
