//
//  ResponseCollectionViewCell.swift
//  Siri
//
//  Created by Angus Mitchell on 12/6/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class ResponseCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var responseVariableTextViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var responseVariableTextView: UITextViewFixed!
    @IBOutlet weak var responseValueLabel: UILabel!
//    @IBOutlet weak var responseNameLabel: UILabel!
    
    @IBOutlet weak var responseVariableContainer: UIView!
    
    @IBOutlet weak var cellContentView: UIView!
//    var requiresInput: Bool = false
    
//    var customIsSelected: Bool = false {
//        didSet {
//            if customIsSelected {
//                self.backgroundColor = .blue
//            } else {
//                self.backgroundColor = .clear
//            }
////            self.layoutIfNeeded()
//            self.setNeedsLayout()
//            self.setNeedsDisplay()
//            print("CHANGE: \(customIsSelected)")
//        }
//    }
}
