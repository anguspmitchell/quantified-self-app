//
//  ResponseShortcutCollectionViewCell.swift
//  Siri
//
//  Created by Angus Mitchell on 12/12/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class ResponseShortcutCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellContentView: UIView!
    //    @IBOutlet weak var responseShortcutTitleTextField: UITextField!
//    
//    @IBOutlet weak var responseShortcutTitleLabel: UILabel!
    @IBOutlet weak var responseValueLabel: UILabel!
    
    @IBOutlet weak var responseShortcutTitleContainer: UIView!
    @IBOutlet weak var responseShortcutTitleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var responseVariableLabel: UILabel!
    @IBOutlet weak var responseShortcutTitleTextView: UITextViewFixed!
    
    
//    override var isSelected: Bool {
//        didSet {
//            if isSelected {
//                self.backgroundColor = .blue
//            } else {
//                self.backgroundColor = .clear
//            }
//            self.layoutIfNeeded()
//        }
////        didSet {
////            self.customIsSelected = isSelected
////        }
//    }
    
//    var customIsSelected: Bool = false {
//        didSet {
//            if customIsSelected {
//                self.backgroundColor = .blue
//            } else {
//                self.backgroundColor = .clear
//            }
//            self.layoutIfNeeded()
//        }
//
//    }
//    @IBOutlet weak var responseShortcutTitleTextView: UILabel!
    
}
