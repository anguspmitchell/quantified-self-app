//
//  ResponseVariableInput.swift
//  Siri
//
//  Created by Angus Mitchell on 12/12/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class ResponseVariableInput {
    var name: String
    var initialValue: Any
    var value: Any?
    var valueRange: [Any]
    var valueType: InputValueType
    var uiType: InputUIType
    var shortcuts: [ResponseVariableShortcut] = [ResponseVariableShortcut]()
    
    init?(name: String, initialValue: Any, valueRange: [Any], uiType: InputUIType) {
        if initialValue is Int {
            self.valueType = .input_int
        } else if initialValue is Float {
            self.valueType = .input_float
        } else if initialValue is Double {
            self.valueType = .input_double
        } else if initialValue is String {
            self.valueType = .input_string
        } else {
            return nil
        }
        
        self.name = name
        self.initialValue = initialValue
        self.valueRange = valueRange
        self.uiType = uiType
    }
    
//    func getRowOfValue(isInitialValue: Bool) -> Int {
//        var getValue: Any? = isInitialValue ? initialValue : value
//        if let getValue = getValue {
//            switch valueType {
//            case .input_int:
//                return (valueRange as! [Int]).index(of: getValue as! Int) ?? -1
//            case .input_float:
//                return (valueRange as! [Float]).index(of: getValue as! Float) ?? -1
//            case .input_double:
//                return (valueRange as! [Double]).index(of: getValue as! Double) ?? -1
//            case .input_string:
//                return (valueRange as! [String]).index(of: getValue as! String) ?? -1
//            }
//        }
//        return -1
//    }
    
    func getRowOfCoalescedValue() -> Int {
        var getValue: Any
        if let value = self.value {
            getValue = value
        } else {
            getValue = initialValue
        }
        
        switch valueType {
        case .input_int:
            return (valueRange as! [Int]).index(of: getValue as! Int) ?? -1
        case .input_float:
            return (valueRange as! [Float]).index(of: getValue as! Float) ?? -1
        case .input_double:
            return (valueRange as! [Double]).index(of: getValue as! Double) ?? -1
        case .input_string:
            return (valueRange as! [String]).index(of: getValue as! String) ?? -1
        }
    }
    
    func setValue(valueString: String) {
        switch valueType {
        case .input_int:
            self.value = Int(valueString)
        case .input_float:
            self.value = Float(valueString)
        case .input_double:
            self.value = Double(valueString)
        case .input_string:
            self.value = valueString
        }
    }
    
    func clearValue() {
        self.value = nil
    }
    func setValue(shortcut: ResponseVariableShortcut) {
        self.value = shortcut.value
//        switch valueType {
//        case .input_int:
//            self.value = shortcut.value
//        case .input_float:
//            self.value = Float(valueString)
//        case .input_double:
//            self.value = Double(valueString)
//        case .input_string:
//            self.value = valueString
//        }
    }
    
    func isSetToShortcut(shortcut: ResponseVariableShortcut) -> Bool {
        return hasValue(value: shortcut.value)
    }
    
    func hasValue(value: Any) -> Bool {
        if self.value != nil {
            switch valueType {
            case .input_int:
                return self.value as! Int == value as! Int
            case .input_float:
                return self.value as! Float == value as! Float
            case .input_double:
                return self.value as! Double == value as! Double
            case .input_string:
                return self.value as! String == value as! String
            }
        }
        return false
    }
    
    func rangeContainsValue (value: Any) -> Bool {
        switch valueType {
        case .input_int:
            return (valueRange as! [Int]).index(of: value as! Int) != nil
        case .input_float:
            return (valueRange as! [Float]).index(of: value as! Float) != nil
        case .input_double:
            return (valueRange as! [Double]).index(of: value as! Double) != nil
        case .input_string:
            return (valueRange as! [String]).index(of: value as! String) != nil
        }
    }
    
    func createShortcut(name: String, value: Any) -> ResponseVariableShortcut? {
        var existingShortcut: [ResponseVariableShortcut]
        switch valueType {
        case .input_int:
            existingShortcut = shortcuts.filter( { $0.value as! Int == value as! Int })
        case .input_float:
            existingShortcut = shortcuts.filter( { $0.value as! Float == value as! Float })
        case .input_double:
            existingShortcut = shortcuts.filter( { $0.value as! Double == value as! Double })
        case .input_string:
            existingShortcut = shortcuts.filter( { $0.value as! String == value as! String })
        }
        
        if existingShortcut.count > 0 {
            existingShortcut[0].name = name
            return existingShortcut[0]
        } else {
            if rangeContainsValue(value: value) {
                let shortcut = ResponseVariableShortcut(name: name, value: value)
                shortcuts.append(shortcut)
                return shortcut
            }
        }
        
        return nil
    }
}

class ResponseVariableShortcut {
    var value: Any
    var name: String
    
    init(name: String, value: Any) {
        self.name = name
        self.value = value
    }
}

enum InputValueType {
    case input_int
    case input_float
    case input_string
    case input_double
}

enum InputUIType {
    case pickerView
}
