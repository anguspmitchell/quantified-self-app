//
//  ResponseCollectionHeaderView.swift
//  Siri
//
//  Created by Angus Mitchell on 12/12/17.
//  Copyright © 2017 Angus Mitchell. All rights reserved.
//

import UIKit

class ResponseCollectionHeaderView: UICollectionReusableView {
    @IBOutlet weak var headerLabel: UILabel!
    
    @IBOutlet weak var containerView: UIView!
}
